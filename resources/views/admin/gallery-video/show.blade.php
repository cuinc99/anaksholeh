@extends('layouts.admin')
@section('content')

<div class="card bg-blueGray-100">
    <div class="card-header">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('global.view') }}
                {{ trans('cruds.galleryVideo.title_singular') }}
            </h6>
        </div>
    </div>

    <div class="card-body">
        <div class="pt-3">
            <table class="table table-view">
                <tbody class="bg-white">
                    <tr>
                        <th>
                            {{ trans('cruds.galleryVideo.fields.title') }}
                        </th>
                        <td>
                            {{ $galleryVideo->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.galleryVideo.fields.desc') }}
                        </th>
                        <td>
                            {{ $galleryVideo->desc }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.galleryVideo.fields.link') }}
                        </th>
                        <td>
                            {{ $galleryVideo->link }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.galleryVideo.fields.school_unit') }}
                        </th>
                        <td>
                            @if($galleryVideo->SchoolUnit)
                                <span class="badge badge-relationship">{{ $galleryVideo->SchoolUnit->name ?? '' }}</span>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <a href="{{ route('admin.gallery-videos.index') }}" class="btn btn-secondary">
                {{ trans('global.back') }}
            </a>
        </div>
    </div>
</div>
@endsection
