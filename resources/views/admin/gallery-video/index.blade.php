@extends('layouts.admin')
@section('content')
<div class="card bg-white">
    <div class="card-header border-b border-blueGray-200">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('cruds.galleryVideo.title_singular') }}
                {{ trans('global.list') }}
            </h6>

            @can('gallery_video_create')
                <a class="btn btn-indigo" href="{{ route('admin.gallery-videos.create') }}">
                    {{ trans('global.add') }} {{ trans('cruds.galleryVideo.title_singular') }}
                </a>
            @endcan
        </div>
    </div>
    @livewire('gallery-video.index')

</div>
@endsection