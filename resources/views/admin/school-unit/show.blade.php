@extends('layouts.admin')
@section('content')

<div class="card bg-blueGray-100">
    <div class="card-header">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('global.view') }}
                {{ trans('cruds.schoolUnit.title_singular') }}
            </h6>
        </div>
    </div>

    <div class="card-body">
        <div class="pt-3">
            <table class="table table-view">
                <tbody class="bg-white">
                    <tr>
                        <th>
                            {{ trans('cruds.schoolUnit.fields.name') }}
                        </th>
                        <td>
                            {{ $schoolUnit->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.schoolUnit.fields.logo') }}
                        </th>
                        <td>
                            @foreach($schoolUnit->logo as $key => $entry)
                                <a class="link-photo" href="{{ $entry['url'] }}">
                                    <img src="{{ $entry['preview_thumbnail'] }}" alt="{{ $entry['name'] }}" title="{{ $entry['name'] }}">
                                </a>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.schoolUnit.fields.profile') }}
                        </th>
                        <td>
                            {!! $schoolUnit->profile !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.schoolUnit.fields.vision_mission') }}
                        </th>
                        <td>
                            {!! $schoolUnit->vision_mission !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.schoolUnit.fields.location') }}
                        </th>
                        <td>
                            {{ $schoolUnit->location }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.schoolUnit.fields.fb') }}
                        </th>
                        <td>
                            {{ $schoolUnit->fb }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.schoolUnit.fields.ig') }}
                        </th>
                        <td>
                            {{ $schoolUnit->ig }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.schoolUnit.fields.tw') }}
                        </th>
                        <td>
                            {{ $schoolUnit->tw }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.schoolUnit.fields.yt') }}
                        </th>
                        <td>
                            {{ $schoolUnit->yt }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.schoolUnit.fields.wa') }}
                        </th>
                        <td>
                            {{ $schoolUnit->wa }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <a href="{{ route('admin.school-units.index') }}" class="btn btn-secondary">
                {{ trans('global.back') }}
            </a>
        </div>
    </div>
</div>
@endsection
