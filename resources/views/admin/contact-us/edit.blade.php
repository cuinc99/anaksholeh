@extends('layouts.admin')
@section('content')

<div class="card bg-blueGray-100">
    <div class="card-header">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('global.edit') }}
                {{ trans('cruds.contactUs.title_singular') }}:
                {{ trans('cruds.contactUs.fields.id') }}
                {{ $contactus->id }}
            </h6>
        </div>
    </div>

    <div class="card-body">
        @livewire('contact-us.edit', [$contactus])
    </div>
</div>
@endsection
