@extends('layouts.admin')
@section('content')

<div class="card bg-blueGray-100">
    <div class="card-header">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('global.view') }}
                {{ trans('cruds.contactUs.title_singular') }}:
                {{ trans('cruds.contactUs.fields.id') }}
                {{ $contactus->id }}
            </h6>
        </div>
    </div>

    <div class="card-body">
        <div class="pt-3">
            <table class="table table-view">
                <tbody class="bg-white">
                    <tr>
                        <th>
                            {{ trans('cruds.contactUs.fields.id') }}
                        </th>
                        <td>
                            {{ $contactus->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contactUs.fields.name') }}
                        </th>
                        <td>
                            {{ $contactus->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contactUs.fields.number') }}
                        </th>
                        <td>
                            {{ $contactus->number }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contactUs.fields.message') }}
                        </th>
                        <td>
                            {{ $contactus->message }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contactUs.fields.school_unit') }}
                        </th>
                        <td>
                            @if($contactus->SchoolUnit)
                                <span class="badge badge-relationship">{{ $contactus->SchoolUnit->name ?? '' }}</span>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <a href="{{ route('admin.contactuses.index') }}" class="btn btn-secondary">
                {{ trans('global.back') }}
            </a>
        </div>
    </div>
</div>
@endsection
