@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="card bg-blueGray-100">
        <div class="card-header">
            <div class="card-header-container">
                <h6 class="card-title">
                    {{ trans('global.view') }}
                    {{ trans('cruds.pendaftaran.title_singular') }}:
                    {{ trans('cruds.pendaftaran.fields.id') }}
                    {{ $pendaftaran->id }}
                </h6>
            </div>
        </div>

        <div class="card-body">
            <div class="pt-3">
                <table class="table table-view">
                    <tbody class="bg-white">
                        @if ($pendaftaran->nama_lengkap)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.nama_lengkap') }}
                            </th>
                            <td>
                                {{ $pendaftaran->nama_lengkap }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->nama_pangilan)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.nama_pangilan') }}
                            </th>
                            <td>
                                {{ $pendaftaran->nama_pangilan }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->jenis_kelamin_label )
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.jenis_kelamin') }}
                            </th>
                            <td>
                                {{ $pendaftaran->jenis_kelamin_label }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->nisn)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.nisn') }}
                            </th>
                            <td>
                                {{ $pendaftaran->nisn }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->golongan_darah_label )
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.golongan_darah') }}
                            </th>
                            <td>
                                {{ $pendaftaran->golongan_darah_label }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->tempat_lahir )
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.tempat_lahir') }}
                            </th>
                            <td>
                                {{ $pendaftaran->tempat_lahir }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->tanggal_lahir)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.tanggal_lahir') }}
                            </th>
                            <td>
                                {{ $pendaftaran->tanggal_lahir }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->nik)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.nik') }}
                            </th>
                            <td>
                                {{ $pendaftaran->nik }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->kewarganegaraan)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.kewarganegaraan') }}
                            </th>
                            <td>
                                {{ $pendaftaran->kewarganegaraan }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->anak_ke)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.anak_ke') }}
                            </th>
                            <td>
                                {{ $pendaftaran->anak_ke }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->jumlah_saudara)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.jumlah_saudara') }}
                            </th>
                            <td>
                                {{ $pendaftaran->jumlah_saudara }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->bahasa)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.bahasa') }}
                            </th>
                            <td>
                                {{ $pendaftaran->bahasa }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->asal_sekolah)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.asal_sekolah') }}
                            </th>
                            <td>
                                {{ $pendaftaran->asal_sekolah }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->alamat_siswa)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.alamat_siswa') }}
                            </th>
                            <td>
                                {{ $pendaftaran->alamat_siswa }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->kelas)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.kelas') }}
                            </th>
                            <td>
                                {{ $pendaftaran->kelas }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->jalur_masuk)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.jalur_masuk') }}
                            </th>
                            <td>
                                {{ $pendaftaran->jalur_masuk }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->ket_jalur_masuk)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.ket_jalur_masuk') }}
                            </th>
                            <td>
                                {{ $pendaftaran->ket_jalur_masuk }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->nama_ayah)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.nama_ayah') }}
                            </th>
                            <td>
                                {{ $pendaftaran->nama_ayah }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->tahun_lahir_ayah)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.tahun_lahir_ayah') }}
                            </th>
                            <td>
                                {{ $pendaftaran->tahun_lahir_ayah }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->pendidikan_ayah)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.pendidikan_ayah') }}
                            </th>
                            <td>
                                {{ $pendaftaran->pendidikan_ayah }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->telepon_ayah)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.telepon_ayah') }}
                            </th>
                            <td>
                                {{ $pendaftaran->telepon_ayah }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->pekerjaan_ayah)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.pekerjaan_ayah') }}
                            </th>
                            <td>
                                {{ $pendaftaran->pekerjaan_ayah }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->penghasilan_ayah_label)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.penghasilan_ayah') }}
                            </th>
                            <td>
                                {{ $pendaftaran->penghasilan_ayah_label }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->nama_ibu)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.nama_ibu') }}
                            </th>
                            <td>
                                {{ $pendaftaran->nama_ibu }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->tahun_lahir_ibu)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.tahun_lahir_ibu') }}
                            </th>
                            <td>
                                {{ $pendaftaran->tahun_lahir_ibu }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->pendidikan_ibu)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.pendidikan_ibu') }}
                            </th>
                            <td>
                                {{ $pendaftaran->pendidikan_ibu }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->telepon_ibu)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.telepon_ibu') }}
                            </th>
                            <td>
                                {{ $pendaftaran->telepon_ibu }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->pekerjaan_ibu)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.pekerjaan_ibu') }}
                            </th>
                            <td>
                                {{ $pendaftaran->pekerjaan_ibu }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->penghasilan_ibu_label)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.penghasilan_ibu') }}
                            </th>
                            <td>
                                {{ $pendaftaran->penghasilan_ibu_label }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->alamat_ortu)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.alamat_ortu') }}
                            </th>
                            <td>
                                {{ $pendaftaran->alamat_ortu }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->pilihan_kelas_label)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.pilihan_kelas') }}
                            </th>
                            <td>
                                {{ $pendaftaran->pilihan_kelas_label }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->nama_wali)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.nama_wali') }}
                            </th>
                            <td>
                                {{ $pendaftaran->nama_wali }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->pekerjaan_wali)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.pekerjaan_wali') }}
                            </th>
                            <td>
                                {{ $pendaftaran->pekerjaan_wali }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->telepon_wali)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.telepon_wali') }}
                            </th>
                            <td>
                                {{ $pendaftaran->telepon_wali }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->jarak_rumah)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.jarak_rumah') }}
                            </th>
                            <td>
                                {{ $pendaftaran->jarak_rumah }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->waktu_tempuh)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.waktu_tempuh') }}
                            </th>
                            <td>
                                {{ $pendaftaran->waktu_tempuh }}
                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->biaya_administrasi)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.biaya_administrasi') }}
                            </th>
                            <td>
                                <a class="btn btn-primary" href="/storage/{{ $pendaftaran->biaya_administrasi }}">Lihat File</a>

                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->kartu_keluarga)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.kartu_keluarga') }}
                            </th>
                            <td>
                                <a class="btn btn-primary" href="/storage/{{ $pendaftaran->kartu_keluarga }}">Lihat File</a>

                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->akta_kelahiran)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.akta_kelahiran') }}
                            </th>
                            <td>
                                <a class="btn btn-primary" href="/storage/{{ $pendaftaran->akta_kelahiran }}">Lihat File</a>

                            </td>
                        </tr>
                        @endif
                        @if ($pendaftaran->pas_foto)
                        <tr>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.pas_foto') }}
                            </th>
                            <td>
                                <a class="btn btn-primary" href="/storage/{{ $pendaftaran->pas_foto }}">Lihat File</a>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <a href="{{ url()->previous() }}" class="btn btn-secondary">
                    {{ trans('global.back') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
