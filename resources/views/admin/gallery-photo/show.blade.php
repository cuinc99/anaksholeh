@extends('layouts.admin')
@section('content')

<div class="card bg-blueGray-100">
    <div class="card-header">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('global.view') }}
                {{ trans('cruds.galleryPhoto.title_singular') }}
            </h6>
        </div>
    </div>

    <div class="card-body">
        <div class="pt-3">
            <table class="table table-view">
                <tbody class="bg-white">
                    <tr>
                        <th>
                            {{ trans('cruds.galleryPhoto.fields.title') }}
                        </th>
                        <td>
                            {{ $galleryPhoto->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.galleryPhoto.fields.desc') }}
                        </th>
                        <td>
                            {{ $galleryPhoto->desc }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.galleryPhoto.fields.photo') }}
                        </th>
                        <td>
                            @foreach($galleryPhoto->photo as $key => $entry)
                                <a class="link-photo" href="{{ $entry['url'] }}">
                                    <img src="{{ $entry['preview_thumbnail'] }}" alt="{{ $entry['name'] }}" title="{{ $entry['name'] }}">
                                </a>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.galleryPhoto.fields.school_unit') }}
                        </th>
                        <td>
                            @if($galleryPhoto->SchoolUnit)
                                <span class="badge badge-relationship">{{ $galleryPhoto->SchoolUnit->name ?? '' }}</span>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <a href="{{ route('admin.gallery-photos.index') }}" class="btn btn-secondary">
                {{ trans('global.back') }}
            </a>
        </div>
    </div>
</div>
@endsection
