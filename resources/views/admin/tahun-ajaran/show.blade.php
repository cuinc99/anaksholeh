@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="card bg-blueGray-100">
        <div class="card-header">
            <div class="card-header-container">
                <h6 class="card-title">
                    {{ trans('global.view') }}
                    {{ trans('cruds.tahunAjaran.title_singular') }}:
                    {{ trans('cruds.tahunAjaran.fields.id') }}
                    {{ $tahunAjaran->id }}
                </h6>
            </div>
        </div>

        <div class="card-body">
            <div class="pt-3">
                <table class="table table-view">
                    <tbody class="bg-white">
                        <tr>
                            <th>
                                {{ trans('cruds.tahunAjaran.fields.name') }}
                            </th>
                            <td>
                                {{ $tahunAjaran->name }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ trans('cruds.tahunAjaran.fields.description') }}
                            </th>
                            <td>
                                {{ $tahunAjaran->description }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ trans('cruds.tahunAjaran.fields.school_unit') }}
                            </th>
                            <td>
                                @if($tahunAjaran->schoolUnit)
                                    <span class="badge badge-relationship">{{ $tahunAjaran->schoolUnit->name ?? '' }}</span>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="mt-4 overflow-hidden">
                    <div class="overflow-x-auto">
                <table class="table w-full table-view">
                    <thead>
                        <tr>
                            <th class="w-28">
                                No
                            </th>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.nama_lengkap') }}
                            </th>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.jenis_kelamin') }}
                            </th>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.tempat_lahir') }}
                            </th>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.tanggal_lahir') }}
                            </th>
                            <th>
                                {{ trans('cruds.pendaftaran.fields.alamat_siswa') }}
                            </th>
                            <th>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white">
                        @forelse($tahunAjaran->pendaftarans as $pendaftaran)
                        <tr>
                            <td>
                                {{ $loop->iteration }}
                            </td>
                            <td>
                                {{ $pendaftaran->nama_lengkap }}
                            </td>
                            <td>
                                {{ $pendaftaran->jenis_kelamin_label }}
                            </td>
                            <td>
                                {{ $pendaftaran->tempat_lahir }}
                            </td>
                            <td>
                                {{ $pendaftaran->tanggal_lahir }}
                            </td>
                            <td>
                                {{ $pendaftaran->alamat_siswa }}
                            </td>
                            <td>
                                <div class="flex justify-end">
                                    @can('pendaftaran_show')
                                        <a class="mr-2 btn btn-sm btn-info" href="{{ route('admin.pendaftarans.show', $pendaftaran) }}">
                                            {{ trans('global.view') }} DETAIL
                                        </a>
                                    @endcan
                                    {{-- @can('pendaftaran_edit')
                                        <a class="mr-2 btn btn-sm btn-success" href="{{ route('admin.pendaftarans.edit', $pendaftaran) }}">
                                            {{ trans('global.edit') }}
                                        </a>
                                    @endcan --}}
                                    {{-- @can('pendaftaran_delete')
                                        <button class="mr-2 btn btn-sm btn-rose" type="button" wire:click="confirm('delete', {{ $pendaftaran->id }})" wire:loading.attr="disabled">
                                            {{ trans('global.delete') }}
                                        </button>
                                    @endcan --}}
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="10">No entries found.</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{-- @can('tahun_ajaran_edit')
                    <a href="{{ route('admin.tahun-ajarans.edit', $tahunAjaran) }}" class="mr-2 btn btn-indigo">
                        {{ trans('global.edit') }}
                    </a>
                @endcan --}}
                <a href="{{ route('admin.tahun-ajarans.index') }}" class="btn btn-secondary">
                    {{ trans('global.back') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
