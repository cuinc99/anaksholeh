@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="bg-white card">
        <div class="border-b card-header border-blueGray-200">
            <div class="card-header-container">
                <h6 class="card-title">
                    {{ trans('cruds.tahunAjaran.title_singular') }}
                    {{ trans('global.list') }}
                </h6>

                @can('tahun_ajaran_create')
                    <a class="btn btn-indigo" href="{{ route('admin.tahun-ajarans.create') }}">
                        {{ trans('global.add') }} {{ trans('cruds.tahunAjaran.title_singular') }}
                    </a>
                @endcan
            </div>
        </div>
        @livewire('tahun-ajaran.index')

    </div>
</div>
@endsection
