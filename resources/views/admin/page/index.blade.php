@extends('layouts.admin')
@section('content')
<div class="card bg-white">
    <div class="card-header border-b border-blueGray-200">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('cruds.page.title_singular') }}
                {{ trans('global.list') }}
            </h6>

            @can('page_create')
                <a class="btn btn-indigo" href="{{ route('admin.pages.create') }}">
                    {{ trans('global.add') }} {{ trans('cruds.page.title_singular') }}
                </a>
            @endcan
        </div>
    </div>
    @livewire('page.index')

</div>
@endsection