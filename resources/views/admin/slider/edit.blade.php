@extends('layouts.admin')
@section('content')

<div class="card bg-blueGray-100">
    <div class="card-header">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('global.edit') }}
                {{ trans('cruds.slider.title_singular') }}
        </div>
    </div>

    <div class="card-body">
        @livewire('slider.edit', [$slider])
    </div>
</div>
@endsection
