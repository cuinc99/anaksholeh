@extends('layouts.admin')
@section('content')

<div class="card bg-blueGray-100">
    <div class="card-header">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('global.view') }}
                {{ trans('cruds.statistic.title_singular') }}:
                {{ trans('cruds.statistic.fields.id') }}
                {{ $statistic->id }}
            </h6>
        </div>
    </div>

    <div class="card-body">
        <div class="pt-3">
            <table class="table table-view">
                <tbody class="bg-white">
                    <tr>
                        <th>
                            {{ trans('cruds.statistic.fields.id') }}
                        </th>
                        <td>
                            {{ $statistic->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.statistic.fields.school_unit') }}
                        </th>
                        <td>
                            {{ $statistic->school_unit }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.statistic.fields.teacher') }}
                        </th>
                        <td>
                            {{ $statistic->teacher }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.statistic.fields.employee') }}
                        </th>
                        <td>
                            {{ $statistic->employee }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.statistic.fields.student') }}
                        </th>
                        <td>
                            {{ $statistic->student }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <a href="{{ route('admin.statistics.index') }}" class="btn btn-secondary">
                {{ trans('global.back') }}
            </a>
        </div>
    </div>
</div>
@endsection