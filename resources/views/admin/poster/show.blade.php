@extends('layouts.admin')
@section('content')

<div class="card bg-blueGray-100">
    <div class="card-header">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('global.view') }}
                {{ trans('cruds.poster.title_singular') }}:
                {{ trans('cruds.poster.fields.id') }}
                {{ $poster->id }}
            </h6>
        </div>
    </div>

    <div class="card-body">
        <div class="pt-3">
            <table class="table table-view">
                <tbody class="bg-white">
                    <tr>
                        <th>
                            {{ trans('cruds.poster.fields.title') }}
                        </th>
                        <td>
                            {{ $poster->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.poster.fields.desc') }}
                        </th>
                        <td>
                            {{ $poster->desc }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.poster.fields.image') }}
                        </th>
                        <td>
                            @foreach($poster->image as $key => $entry)
                                <a class="link-photo" href="{{ $entry['url'] }}">
                                    <img src="{{ $entry['preview_thumbnail'] }}" alt="{{ $entry['name'] }}" title="{{ $entry['name'] }}">
                                </a>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.poster.fields.school_unit') }}
                        </th>
                        <td>
                            @if($poster->SchoolUnit)
                                <span class="badge badge-relationship">{{ $poster->SchoolUnit->name ?? '' }}</span>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <a href="{{ route('admin.posters.index') }}" class="btn btn-secondary">
                {{ trans('global.back') }}
            </a>
        </div>
    </div>
</div>
@endsection
