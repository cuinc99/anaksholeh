@extends('layouts.admin')
@section('content')

<div class="card bg-blueGray-100">
    <div class="card-header">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('global.edit') }}
                {{ trans('cruds.poster.title_singular') }}:
                {{ trans('cruds.poster.fields.id') }}
                {{ $poster->id }}
            </h6>
        </div>
    </div>

    <div class="card-body">
        @livewire('poster.edit', [$poster])
    </div>
</div>
@endsection