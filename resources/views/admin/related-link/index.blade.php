@extends('layouts.admin')
@section('content')
<div class="card bg-white">
    <div class="card-header border-b border-blueGray-200">
        <div class="card-header-container">
            <h6 class="card-title">
                {{ trans('cruds.relatedLink.title_singular') }}
                {{ trans('global.list') }}
            </h6>

            @can('related_link_create')
                <a class="btn btn-indigo" href="{{ route('admin.related-links.create') }}">
                    {{ trans('global.add') }} {{ trans('cruds.relatedLink.title_singular') }}
                </a>
            @endcan
        </div>
    </div>
    @livewire('related-link.index')

</div>
@endsection