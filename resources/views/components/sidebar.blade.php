<nav class="relative z-10 flex flex-wrap items-center justify-between px-6 py-4 bg-white shadow-xl md:left-0 md:block md:fixed md:top-0 md:bottom-0 md:overflow-y-auto md:flex-row md:flex-nowrap md:overflow-hidden md:w-64">
    <div class="flex flex-wrap items-center justify-between w-full px-0 mx-auto md:flex-col md:items-stretch md:min-h-full md:flex-nowrap">
        <button class="px-3 py-1 text-xl leading-none text-black bg-transparent border border-transparent border-solid rounded opacity-50 cursor-pointer md:hidden" type="button" onclick="toggleNavbar('example-collapse-sidebar')">
            <i class="fas fa-bars"></i>
        </button>
        <a class="inline-block p-4 px-0 mr-0 text-sm font-bold text-left uppercase md:block md:pb-2 text-blueGray-700 whitespace-nowrap" href="{{ route('admin.home') }}">
            {{ trans('panel.site_title') }}
        </a>
        <div class="absolute top-0 left-0 right-0 z-40 items-center flex-1 hidden h-auto overflow-x-hidden overflow-y-auto rounded shadow md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none" id="example-collapse-sidebar">
            <div class="block pb-4 mb-4 border-b border-solid md:min-w-full md:hidden border-blueGray-300">
                <div class="flex flex-wrap">
                    <div class="w-6/12">
                        <a class="inline-block p-4 px-0 mr-0 text-sm font-bold text-left uppercase md:block md:pb-2 text-blueGray-700 whitespace-nowrap" href="{{ route('admin.home') }}">
                            {{ trans('panel.site_title') }}
                        </a>
                    </div>
                    <div class="flex justify-end w-6/12">
                        <button type="button" class="px-3 py-1 text-xl leading-none text-black bg-transparent border border-transparent border-solid rounded opacity-50 cursor-pointer md:hidden" onclick="toggleNavbar('example-collapse-sidebar')">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Divider -->
            <hr class="mb-6 md:min-w-full" />
            <!-- Heading -->

            <ul class="flex flex-col list-none md:flex-col md:min-w-full">
                <li class="items-center">
                    <a href="{{ route("admin.home") }}" class="{{ request()->is("admin") ? "sidebar-nav-active" : "sidebar-nav" }}">
                        <i class="fas fa-tv"></i>
                        {{ trans('global.dashboard') }}
                    </a>
                </li>

                @if (auth()->user()->school_unit_id == 0)
                @can('user_management_access')
                    <li class="items-center">
                        <a class="has-sub {{ request()->is("admin/permissions*")||request()->is("admin/roles*")||request()->is("admin/users*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="#" onclick="window.openSubNav(this)">
                            <i class="fa-fw fas fa-users c-sidebar-nav-icon">
                            </i>
                            {{ trans('cruds.userManagement.title') }}
                        </a>
                        <ul class="hidden ml-4 subnav">
                            {{-- @can('permission_access')
                                <li class="items-center">
                                    <a class="{{ request()->is("admin/permissions*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.permissions.index") }}">
                                        <i class="fa-fw c-sidebar-nav-icon fas fa-unlock-alt">
                                        </i>
                                        {{ trans('cruds.permission.title') }}
                                    </a>
                                </li>
                            @endcan
                            @can('role_access')
                                <li class="items-center">
                                    <a class="{{ request()->is("admin/roles*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.roles.index") }}">
                                        <i class="fa-fw c-sidebar-nav-icon fas fa-briefcase">
                                        </i>
                                        {{ trans('cruds.role.title') }}
                                    </a>
                                </li>
                            @endcan --}}
                            @can('user_access')
                                <li class="items-center">
                                    <a class="{{ request()->is("admin/users*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.users.index") }}">
                                        <i class="fa-fw c-sidebar-nav-icon fas fa-user">
                                        </i>
                                        {{ trans('cruds.user.title') }}
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @endif
                @can('school_unit_access')
                    <li class="items-center">
                        <a class="{{ request()->is("admin/school-units*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.school-units.index") }}">
                            <i class="fa-fw c-sidebar-nav-icon fas fa-building">
                            </i>
                            {{ trans('cruds.schoolUnit.title') }}
                        </a>
                    </li>
                @endcan

                {{-- @can('category_access')
                    <li class="items-center">
                        <a class="{{ request()->is("admin/categories*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.categories.index") }}">
                            <i class="fa-fw c-sidebar-nav-icon fas fa-bars">
                            </i>
                            {{ trans('cruds.category.title') }}
                        </a>
                    </li>
                @endcan --}}
                @can('article_access')
                    <li class="items-center">
                        <a class="{{ request()->is("admin/articles*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.articles.index") }}">
                            <i class="fa-fw c-sidebar-nav-icon fas fa-newspaper">
                            </i>
                            {{ trans('cruds.article.title') }}
                        </a>
                    </li>
                @endcan
                @can('page_access')
                    <li class="items-center">
                        <a class="{{ request()->is("admin/pages*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.pages.index") }}">
                            <i class="fa-fw c-sidebar-nav-icon fas fa-pager">
                            </i>
                            {{ trans('cruds.page.title') }}
                        </a>
                    </li>
                @endcan
                @can('event_access')
                    <li class="items-center">
                        <a class="{{ request()->is("admin/events*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.events.index") }}">
                            <i class="fa-fw c-sidebar-nav-icon fas fa-calendar-week">
                            </i>
                            {{ trans('cruds.event.title') }}
                        </a>
                    </li>
                @endcan
                @can('gallery_photo_access')
                    <li class="items-center">
                        <a class="has-sub {{ request()->is("admin/gallery-photo*")||request()->is("admin/gallery-videos*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="#" onclick="window.openSubNav(this)">
                            <i class="fa-fw fas fa-photo-video c-sidebar-nav-icon">
                            </i>
                            Galeri
                        </a>
                        <ul class="hidden ml-4 subnav">
                            @can('gallery_photo_access')
                                <li class="items-center">
                                    <a class="{{ request()->is("admin/gallery-photos*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.gallery-photos.index") }}">
                                        <i class="fa-fw c-sidebar-nav-icon fas fa-bars">
                                        </i>
                                        {{ trans('cruds.galleryPhoto.title') }}
                                    </a>
                                </li>
                            @endcan
                            @can('gallery_video_access')
                                <li class="items-center">
                                    <a class="{{ request()->is("admin/gallery-videos*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.gallery-videos.index") }}">
                                        <i class="fa-fw c-sidebar-nav-icon fas fa-bars">
                                        </i>
                                        {{ trans('cruds.galleryVideo.title') }}
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('poster_access')
                    <li class="items-center">
                        <a class="{{ request()->is("admin/posters*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.posters.index") }}">
                            <i class="fa-fw c-sidebar-nav-icon fas fa-image">
                            </i>
                            {{ trans('cruds.poster.title') }}
                        </a>
                    </li>
                @endcan
                @can('contact_us_access')
                    <li class="items-center">
                        <a class="{{ request()->is("admin/contactuses*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.contactuses.index") }}">
                            <i class="fa-fw c-sidebar-nav-icon fas fa-file-signature">
                            </i>
                            {{ trans('cruds.contactUs.title') }}
                        </a>
                    </li>
                @endcan
                {{-- @can('pendaftaran_access')
                    <li class="items-center">
                        <a class="{{ request()->is("admin/pendaftarans*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.pendaftarans.index") }}">
                            <i class="fa-fw c-sidebar-nav-icon fas fa-cogs">
                            </i>
                            {{ trans('cruds.pendaftaran.title') }}
                        </a>
                    </li>
                @endcan --}}
                @can('tahun_ajaran_access')
                    <li class="items-center">
                        <a class="{{ request()->is("admin/tahun-ajarans*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.tahun-ajarans.index") }}">
                            <i class="fa-fw c-sidebar-nav-icon fas fa-calendar-alt">
                            </i>
                            {{ trans('cruds.pendaftaran.title') }} (TA) <span class="badge badge-primary">Baru</span>
                        </a>
                    </li>
                @endcan
                @can('setting_access')
                    <li class="items-center">
                        <a class="has-sub {{ request()->is("admin/sliders*")||request()->is("admin/statistics*")||request()->is("admin/related-links*")||request()->is("admin/testimonials*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="#" onclick="window.openSubNav(this)">
                            <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">
                            </i>
                            {{ trans('cruds.setting.title') }}
                        </a>
                        <ul class="hidden ml-4 subnav">
                            @can('slider_access')
                                <li class="items-center">
                                    <a class="{{ request()->is("admin/sliders*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.sliders.index") }}">
                                        <i class="fa-fw c-sidebar-nav-icon fas fa-bars">
                                        </i>
                                        {{ trans('cruds.slider.title') }}
                                    </a>
                                </li>
                            @endcan
                            @can('statistic_access')
                                <li class="items-center">
                                    <a class="{{ request()->is("admin/statistics*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.statistics.index") }}">
                                        <i class="fa-fw c-sidebar-nav-icon fas fa-bars">
                                        </i>
                                        {{ trans('cruds.statistic.title') }}
                                    </a>
                                </li>
                            @endcan
                            @can('related_link_access')
                                <li class="items-center">
                                    <a class="{{ request()->is("admin/related-links*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.related-links.index") }}">
                                        <i class="fa-fw c-sidebar-nav-icon fas fa-bars">
                                        </i>
                                        {{ trans('cruds.relatedLink.title') }}
                                    </a>
                                </li>
                            @endcan
                            @can('testimonial_access')
                                <li class="items-center">
                                    <a class="{{ request()->is("admin/testimonials*") ? "sidebar-nav-active" : "sidebar-nav" }}" href="{{ route("admin.testimonials.index") }}">
                                        <i class="fa-fw c-sidebar-nav-icon fas fa-bars">
                                        </i>
                                        {{ trans('cruds.testimonial.title') }}
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
                    @can('profile_password_edit')
                        <li class="items-center">
                            <a href="{{ route("profile.password.edit") }}" class="{{ request()->is("profile/password") || request()->is("profile/password/*") ? "sidebar-nav-active" : "sidebar-nav" }}">
                                <i class="fas fa-cogs"></i>
                                {{ trans('global.change_password') }}
                            </a>
                        </li>
                    @endcan
                @endif

                <li class="items-center">
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();" class="sidebar-nav">
                        <i class="fa-fw fas fa-sign-out-alt"></i>
                        {{ trans('global.logout') }}
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
