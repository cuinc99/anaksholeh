<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('pendaftaran.nama_lengkap') ? 'invalid' : '' }}">
        <label class="form-label required" for="nama_lengkap">{{ trans('cruds.pendaftaran.fields.nama_lengkap') }}</label>
        <input class="form-control" type="text" name="nama_lengkap" id="nama_lengkap" required wire:model.defer="pendaftaran.nama_lengkap">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.nama_lengkap') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.nama_lengkap_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.nama_pangilan') ? 'invalid' : '' }}">
        <label class="form-label" for="nama_pangilan">{{ trans('cruds.pendaftaran.fields.nama_pangilan') }}</label>
        <input class="form-control" type="text" name="nama_pangilan" id="nama_pangilan" wire:model.defer="pendaftaran.nama_pangilan">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.nama_pangilan') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.nama_pangilan_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.jenis_kelamin') ? 'invalid' : '' }}">
        <label class="form-label">{{ trans('cruds.pendaftaran.fields.jenis_kelamin') }}</label>
        @foreach($this->listsForFields['jenis_kelamin'] as $key => $value)
            <label class="radio-label"><input type="radio" name="jenis_kelamin" wire:model="pendaftaran.jenis_kelamin" value="{{ $key }}">{{ $value }}</label>
        @endforeach
        <div class="validation-message">
            {{ $errors->first('pendaftaran.jenis_kelamin') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.jenis_kelamin_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.nisn') ? 'invalid' : '' }}">
        <label class="form-label" for="nisn">{{ trans('cruds.pendaftaran.fields.nisn') }}</label>
        <input class="form-control" type="text" name="nisn" id="nisn" wire:model.defer="pendaftaran.nisn">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.nisn') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.nisn_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.golongan_darah') ? 'invalid' : '' }}">
        <label class="form-label">{{ trans('cruds.pendaftaran.fields.golongan_darah') }}</label>
        @foreach($this->listsForFields['golongan_darah'] as $key => $value)
            <label class="radio-label"><input type="radio" name="golongan_darah" wire:model="pendaftaran.golongan_darah" value="{{ $key }}">{{ $value }}</label>
        @endforeach
        <div class="validation-message">
            {{ $errors->first('pendaftaran.golongan_darah') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.golongan_darah_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.tempat_lahir') ? 'invalid' : '' }}">
        <label class="form-label required" for="tempat_lahir">{{ trans('cruds.pendaftaran.fields.tempat_lahir') }}</label>
        <input class="form-control" type="text" name="tempat_lahir" id="tempat_lahir" required wire:model.defer="pendaftaran.tempat_lahir">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.tempat_lahir') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.tempat_lahir_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.tanggal_lahir') ? 'invalid' : '' }}">
        <label class="form-label" for="tanggal_lahir">{{ trans('cruds.pendaftaran.fields.tanggal_lahir') }}</label>
        <x-date-picker class="form-control" wire:model="pendaftaran.tanggal_lahir" id="tanggal_lahir" name="tanggal_lahir" picker="date" />
        <div class="validation-message">
            {{ $errors->first('pendaftaran.tanggal_lahir') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.tanggal_lahir_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.nik') ? 'invalid' : '' }}">
        <label class="form-label" for="nik">{{ trans('cruds.pendaftaran.fields.nik') }}</label>
        <input class="form-control" type="text" name="nik" id="nik" wire:model.defer="pendaftaran.nik">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.nik') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.nik_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.kewarganegaraan') ? 'invalid' : '' }}">
        <label class="form-label" for="kewarganegaraan">{{ trans('cruds.pendaftaran.fields.kewarganegaraan') }}</label>
        <input class="form-control" type="text" name="kewarganegaraan" id="kewarganegaraan" wire:model.defer="pendaftaran.kewarganegaraan">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.kewarganegaraan') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.kewarganegaraan_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.anak_ke') ? 'invalid' : '' }}">
        <label class="form-label" for="anak_ke">{{ trans('cruds.pendaftaran.fields.anak_ke') }}</label>
        <input class="form-control" type="number" name="anak_ke" id="anak_ke" wire:model.defer="pendaftaran.anak_ke" step="1">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.anak_ke') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.anak_ke_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.jumlah_saudara') ? 'invalid' : '' }}">
        <label class="form-label" for="jumlah_saudara">{{ trans('cruds.pendaftaran.fields.jumlah_saudara') }}</label>
        <input class="form-control" type="number" name="jumlah_saudara" id="jumlah_saudara" wire:model.defer="pendaftaran.jumlah_saudara" step="1">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.jumlah_saudara') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.jumlah_saudara_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.bahasa') ? 'invalid' : '' }}">
        <label class="form-label" for="bahasa">{{ trans('cruds.pendaftaran.fields.bahasa') }}</label>
        <input class="form-control" type="text" name="bahasa" id="bahasa" wire:model.defer="pendaftaran.bahasa">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.bahasa') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.bahasa_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.asal_sekolah') ? 'invalid' : '' }}">
        <label class="form-label" for="asal_sekolah">{{ trans('cruds.pendaftaran.fields.asal_sekolah') }}</label>
        <input class="form-control" type="text" name="asal_sekolah" id="asal_sekolah" wire:model.defer="pendaftaran.asal_sekolah">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.asal_sekolah') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.asal_sekolah_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.alamat_siswa') ? 'invalid' : '' }}">
        <label class="form-label required" for="alamat_siswa">{{ trans('cruds.pendaftaran.fields.alamat_siswa') }}</label>
        <textarea class="form-control" name="alamat_siswa" id="alamat_siswa" required wire:model.defer="pendaftaran.alamat_siswa" rows="4"></textarea>
        <div class="validation-message">
            {{ $errors->first('pendaftaran.alamat_siswa') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.alamat_siswa_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.nama_ayah') ? 'invalid' : '' }}">
        <label class="form-label required" for="nama_ayah">{{ trans('cruds.pendaftaran.fields.nama_ayah') }}</label>
        <input class="form-control" type="text" name="nama_ayah" id="nama_ayah" required wire:model.defer="pendaftaran.nama_ayah">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.nama_ayah') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.nama_ayah_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.tahun_lahir_ayah') ? 'invalid' : '' }}">
        <label class="form-label" for="tahun_lahir_ayah">{{ trans('cruds.pendaftaran.fields.tahun_lahir_ayah') }}</label>
        <input class="form-control" type="number" name="tahun_lahir_ayah" id="tahun_lahir_ayah" wire:model.defer="pendaftaran.tahun_lahir_ayah" step="1">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.tahun_lahir_ayah') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.tahun_lahir_ayah_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.pendidikan_ayah') ? 'invalid' : '' }}">
        <label class="form-label" for="pendidikan_ayah">{{ trans('cruds.pendaftaran.fields.pendidikan_ayah') }}</label>
        <input class="form-control" type="text" name="pendidikan_ayah" id="pendidikan_ayah" wire:model.defer="pendaftaran.pendidikan_ayah">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.pendidikan_ayah') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.pendidikan_ayah_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.telepon_ayah') ? 'invalid' : '' }}">
        <label class="form-label" for="telepon_ayah">{{ trans('cruds.pendaftaran.fields.telepon_ayah') }}</label>
        <input class="form-control" type="text" name="telepon_ayah" id="telepon_ayah" wire:model.defer="pendaftaran.telepon_ayah">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.telepon_ayah') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.telepon_ayah_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.pekerjaan_ayah') ? 'invalid' : '' }}">
        <label class="form-label required" for="pekerjaan_ayah">{{ trans('cruds.pendaftaran.fields.pekerjaan_ayah') }}</label>
        <input class="form-control" type="text" name="pekerjaan_ayah" id="pekerjaan_ayah" required wire:model.defer="pendaftaran.pekerjaan_ayah">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.pekerjaan_ayah') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.pekerjaan_ayah_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.penghasilan_ayah') ? 'invalid' : '' }}">
        <label class="form-label">{{ trans('cruds.pendaftaran.fields.penghasilan_ayah') }}</label>
        @foreach($this->listsForFields['penghasilan_ayah'] as $key => $value)
            <label class="radio-label"><input type="radio" name="penghasilan_ayah" wire:model="pendaftaran.penghasilan_ayah" value="{{ $key }}">{{ $value }}</label>
        @endforeach
        <div class="validation-message">
            {{ $errors->first('pendaftaran.penghasilan_ayah') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.penghasilan_ayah_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.nama_ibu') ? 'invalid' : '' }}">
        <label class="form-label required" for="nama_ibu">{{ trans('cruds.pendaftaran.fields.nama_ibu') }}</label>
        <input class="form-control" type="text" name="nama_ibu" id="nama_ibu" required wire:model.defer="pendaftaran.nama_ibu">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.nama_ibu') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.nama_ibu_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.tahun_lahir_ibu') ? 'invalid' : '' }}">
        <label class="form-label" for="tahun_lahir_ibu">{{ trans('cruds.pendaftaran.fields.tahun_lahir_ibu') }}</label>
        <input class="form-control" type="number" name="tahun_lahir_ibu" id="tahun_lahir_ibu" wire:model.defer="pendaftaran.tahun_lahir_ibu" step="1">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.tahun_lahir_ibu') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.tahun_lahir_ibu_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.pendidikan_ibu') ? 'invalid' : '' }}">
        <label class="form-label" for="pendidikan_ibu">{{ trans('cruds.pendaftaran.fields.pendidikan_ibu') }}</label>
        <input class="form-control" type="text" name="pendidikan_ibu" id="pendidikan_ibu" wire:model.defer="pendaftaran.pendidikan_ibu">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.pendidikan_ibu') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.pendidikan_ibu_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.telepon_ibu') ? 'invalid' : '' }}">
        <label class="form-label" for="telepon_ibu">{{ trans('cruds.pendaftaran.fields.telepon_ibu') }}</label>
        <input class="form-control" type="text" name="telepon_ibu" id="telepon_ibu" wire:model.defer="pendaftaran.telepon_ibu">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.telepon_ibu') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.telepon_ibu_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.pekerjaan_ibu') ? 'invalid' : '' }}">
        <label class="form-label required" for="pekerjaan_ibu">{{ trans('cruds.pendaftaran.fields.pekerjaan_ibu') }}</label>
        <input class="form-control" type="text" name="pekerjaan_ibu" id="pekerjaan_ibu" required wire:model.defer="pendaftaran.pekerjaan_ibu">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.pekerjaan_ibu') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.pekerjaan_ibu_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.penghasilan_ibu') ? 'invalid' : '' }}">
        <label class="form-label">{{ trans('cruds.pendaftaran.fields.penghasilan_ibu') }}</label>
        @foreach($this->listsForFields['penghasilan_ibu'] as $key => $value)
            <label class="radio-label"><input type="radio" name="penghasilan_ibu" wire:model="pendaftaran.penghasilan_ibu" value="{{ $key }}">{{ $value }}</label>
        @endforeach
        <div class="validation-message">
            {{ $errors->first('pendaftaran.penghasilan_ibu') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.penghasilan_ibu_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.alamat_ortu') ? 'invalid' : '' }}">
        <label class="form-label required" for="alamat_ortu">{{ trans('cruds.pendaftaran.fields.alamat_ortu') }}</label>
        <textarea class="form-control" name="alamat_ortu" id="alamat_ortu" required wire:model.defer="pendaftaran.alamat_ortu" rows="4"></textarea>
        <div class="validation-message">
            {{ $errors->first('pendaftaran.alamat_ortu') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.alamat_ortu_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.pilihan_kelas') ? 'invalid' : '' }}">
        <label class="form-label">{{ trans('cruds.pendaftaran.fields.pilihan_kelas') }}</label>
        <select class="form-control" wire:model="pendaftaran.pilihan_kelas">
            <option value="null" disabled>{{ trans('global.pleaseSelect') }}...</option>
            @foreach($this->listsForFields['pilihan_kelas'] as $key => $value)
                <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
        <div class="validation-message">
            {{ $errors->first('pendaftaran.pilihan_kelas') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.pilihan_kelas_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.nama_wali') ? 'invalid' : '' }}">
        <label class="form-label" for="nama_wali">{{ trans('cruds.pendaftaran.fields.nama_wali') }}</label>
        <input class="form-control" type="text" name="nama_wali" id="nama_wali" wire:model.defer="pendaftaran.nama_wali">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.nama_wali') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.nama_wali_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.pekerjaan_wali') ? 'invalid' : '' }}">
        <label class="form-label" for="pekerjaan_wali">{{ trans('cruds.pendaftaran.fields.pekerjaan_wali') }}</label>
        <input class="form-control" type="text" name="pekerjaan_wali" id="pekerjaan_wali" wire:model.defer="pendaftaran.pekerjaan_wali">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.pekerjaan_wali') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.pekerjaan_wali_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.telepon_wali') ? 'invalid' : '' }}">
        <label class="form-label" for="telepon_wali">{{ trans('cruds.pendaftaran.fields.telepon_wali') }}</label>
        <input class="form-control" type="text" name="telepon_wali" id="telepon_wali" wire:model.defer="pendaftaran.telepon_wali">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.telepon_wali') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.telepon_wali_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.jarak_rumah') ? 'invalid' : '' }}">
        <label class="form-label" for="jarak_rumah">{{ trans('cruds.pendaftaran.fields.jarak_rumah') }}</label>
        <input class="form-control" type="number" name="jarak_rumah" id="jarak_rumah" wire:model.defer="pendaftaran.jarak_rumah" step="1">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.jarak_rumah') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.jarak_rumah_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.waktu_tempuh') ? 'invalid' : '' }}">
        <label class="form-label" for="waktu_tempuh">{{ trans('cruds.pendaftaran.fields.waktu_tempuh') }}</label>
        <input class="form-control" type="number" name="waktu_tempuh" id="waktu_tempuh" wire:model.defer="pendaftaran.waktu_tempuh" step="1">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.waktu_tempuh') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.waktu_tempuh_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.biaya_administrasi') ? 'invalid' : '' }}">
        <label class="form-label" for="biaya_administrasi">{{ trans('cruds.pendaftaran.fields.biaya_administrasi') }}</label>
        <input class="form-control" type="text" name="biaya_administrasi" id="biaya_administrasi" wire:model.defer="pendaftaran.biaya_administrasi">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.biaya_administrasi') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.biaya_administrasi_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.kartu_keluarga') ? 'invalid' : '' }}">
        <label class="form-label" for="kartu_keluarga">{{ trans('cruds.pendaftaran.fields.kartu_keluarga') }}</label>
        <input class="form-control" type="text" name="kartu_keluarga" id="kartu_keluarga" wire:model.defer="pendaftaran.kartu_keluarga">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.kartu_keluarga') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.kartu_keluarga_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.akta_kelahiran') ? 'invalid' : '' }}">
        <label class="form-label" for="akta_kelahiran">{{ trans('cruds.pendaftaran.fields.akta_kelahiran') }}</label>
        <input class="form-control" type="text" name="akta_kelahiran" id="akta_kelahiran" wire:model.defer="pendaftaran.akta_kelahiran">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.akta_kelahiran') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.akta_kelahiran_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('pendaftaran.pas_foto') ? 'invalid' : '' }}">
        <label class="form-label" for="pas_foto">{{ trans('cruds.pendaftaran.fields.pas_foto') }}</label>
        <input class="form-control" type="text" name="pas_foto" id="pas_foto" wire:model.defer="pendaftaran.pas_foto">
        <div class="validation-message">
            {{ $errors->first('pendaftaran.pas_foto') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.pendaftaran.fields.pas_foto_helper') }}
        </div>
    </div>

    <div class="form-group">
        <button class="mr-2 btn btn-indigo" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.pendaftarans.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>
