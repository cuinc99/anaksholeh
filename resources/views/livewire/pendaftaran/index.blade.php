<div>
    <div class="card-controls sm:flex">
        <div class="w-full sm:w-1/2">
            Per page:
            <select wire:model="perPage" class="w-full form-select sm:w-1/6">
                @foreach($paginationOptions as $value)
                    <option value="{{ $value }}">{{ $value }}</option>
                @endforeach
            </select>

            @can('pendaftaran_delete')
                <button class="ml-3 btn btn-rose disabled:opacity-50 disabled:cursor-not-allowed" type="button" wire:click="confirm('deleteSelected')" wire:loading.attr="disabled" {{ $this->selectedCount ? '' : 'disabled' }}>
                    {{ __('Delete Selected') }}
                </button>
            @endcan

            @if(file_exists(app_path('Http/Livewire/ExcelExport.php')))
                <livewire:excel-export model="Pendaftaran" format="csv" />
                <livewire:excel-export model="Pendaftaran" format="xlsx" />
                <livewire:excel-export model="Pendaftaran" format="pdf" />
            @endif




        </div>
        <div class="w-full sm:w-1/2 sm:text-right">
            Search:
            <input type="text" wire:model.debounce.300ms="search" class="inline-block w-full sm:w-1/3" />
        </div>
    </div>
    <div wire:loading.delay>
        Loading...
    </div>

    <div class="overflow-hidden">
        <div class="overflow-x-auto">
            <table class="table w-full table-index">
                <thead>
                    <tr>
                        <th class="w-9">
                        </th>
                        <th class="w-28">
                            {{ trans('cruds.pendaftaran.fields.id') }}
                            @include('components.table.sort', ['field' => 'id'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.nama_lengkap') }}
                            @include('components.table.sort', ['field' => 'nama_lengkap'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.nama_pangilan') }}
                            @include('components.table.sort', ['field' => 'nama_pangilan'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.jenis_kelamin') }}
                            @include('components.table.sort', ['field' => 'jenis_kelamin'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.nisn') }}
                            @include('components.table.sort', ['field' => 'nisn'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.golongan_darah') }}
                            @include('components.table.sort', ['field' => 'golongan_darah'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.tempat_lahir') }}
                            @include('components.table.sort', ['field' => 'tempat_lahir'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.tanggal_lahir') }}
                            @include('components.table.sort', ['field' => 'tanggal_lahir'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.nik') }}
                            @include('components.table.sort', ['field' => 'nik'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.kewarganegaraan') }}
                            @include('components.table.sort', ['field' => 'kewarganegaraan'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.anak_ke') }}
                            @include('components.table.sort', ['field' => 'anak_ke'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.jumlah_saudara') }}
                            @include('components.table.sort', ['field' => 'jumlah_saudara'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.bahasa') }}
                            @include('components.table.sort', ['field' => 'bahasa'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.asal_sekolah') }}
                            @include('components.table.sort', ['field' => 'asal_sekolah'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.alamat_siswa') }}
                            @include('components.table.sort', ['field' => 'alamat_siswa'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.nama_ayah') }}
                            @include('components.table.sort', ['field' => 'nama_ayah'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.tahun_lahir_ayah') }}
                            @include('components.table.sort', ['field' => 'tahun_lahir_ayah'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.pendidikan_ayah') }}
                            @include('components.table.sort', ['field' => 'pendidikan_ayah'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.telepon_ayah') }}
                            @include('components.table.sort', ['field' => 'telepon_ayah'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.pekerjaan_ayah') }}
                            @include('components.table.sort', ['field' => 'pekerjaan_ayah'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.penghasilan_ayah') }}
                            @include('components.table.sort', ['field' => 'penghasilan_ayah'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.nama_ibu') }}
                            @include('components.table.sort', ['field' => 'nama_ibu'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.tahun_lahir_ibu') }}
                            @include('components.table.sort', ['field' => 'tahun_lahir_ibu'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.pendidikan_ibu') }}
                            @include('components.table.sort', ['field' => 'pendidikan_ibu'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.telepon_ibu') }}
                            @include('components.table.sort', ['field' => 'telepon_ibu'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.pekerjaan_ibu') }}
                            @include('components.table.sort', ['field' => 'pekerjaan_ibu'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.penghasilan_ibu') }}
                            @include('components.table.sort', ['field' => 'penghasilan_ibu'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.alamat_ortu') }}
                            @include('components.table.sort', ['field' => 'alamat_ortu'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.pilihan_kelas') }}
                            @include('components.table.sort', ['field' => 'pilihan_kelas'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.nama_wali') }}
                            @include('components.table.sort', ['field' => 'nama_wali'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.pekerjaan_wali') }}
                            @include('components.table.sort', ['field' => 'pekerjaan_wali'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.telepon_wali') }}
                            @include('components.table.sort', ['field' => 'telepon_wali'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.jarak_rumah') }}
                            @include('components.table.sort', ['field' => 'jarak_rumah'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.waktu_tempuh') }}
                            @include('components.table.sort', ['field' => 'waktu_tempuh'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.biaya_administrasi') }}
                            @include('components.table.sort', ['field' => 'biaya_administrasi'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.kartu_keluarga') }}
                            @include('components.table.sort', ['field' => 'kartu_keluarga'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.akta_kelahiran') }}
                            @include('components.table.sort', ['field' => 'akta_kelahiran'])
                        </th>
                        <th>
                            {{ trans('cruds.pendaftaran.fields.pas_foto') }}
                            @include('components.table.sort', ['field' => 'pas_foto'])
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($pendaftarans as $pendaftaran)
                        <tr>
                            <td>
                                <input type="checkbox" value="{{ $pendaftaran->id }}" wire:model="selected">
                            </td>
                            <td>
                                {{ $pendaftaran->id }}
                            </td>
                            <td>
                                {{ $pendaftaran->nama_lengkap }}
                            </td>
                            <td>
                                {{ $pendaftaran->nama_pangilan }}
                            </td>
                            <td>
                                {{ $pendaftaran->jenis_kelamin_label }}
                            </td>
                            <td>
                                {{ $pendaftaran->nisn }}
                            </td>
                            <td>
                                {{ $pendaftaran->golongan_darah_label }}
                            </td>
                            <td>
                                {{ $pendaftaran->tempat_lahir }}
                            </td>
                            <td>
                                {{ $pendaftaran->tanggal_lahir }}
                            </td>
                            <td>
                                {{ $pendaftaran->nik }}
                            </td>
                            <td>
                                {{ $pendaftaran->kewarganegaraan }}
                            </td>
                            <td>
                                {{ $pendaftaran->anak_ke }}
                            </td>
                            <td>
                                {{ $pendaftaran->jumlah_saudara }}
                            </td>
                            <td>
                                {{ $pendaftaran->bahasa }}
                            </td>
                            <td>
                                {{ $pendaftaran->asal_sekolah }}
                            </td>
                            <td>
                                {{ $pendaftaran->alamat_siswa }}
                            </td>
                            <td>
                                {{ $pendaftaran->nama_ayah }}
                            </td>
                            <td>
                                {{ $pendaftaran->tahun_lahir_ayah }}
                            </td>
                            <td>
                                {{ $pendaftaran->pendidikan_ayah }}
                            </td>
                            <td>
                                {{ $pendaftaran->telepon_ayah }}
                            </td>
                            <td>
                                {{ $pendaftaran->pekerjaan_ayah }}
                            </td>
                            <td>
                                {{ $pendaftaran->penghasilan_ayah_label }}
                            </td>
                            <td>
                                {{ $pendaftaran->nama_ibu }}
                            </td>
                            <td>
                                {{ $pendaftaran->tahun_lahir_ibu }}
                            </td>
                            <td>
                                {{ $pendaftaran->pendidikan_ibu }}
                            </td>
                            <td>
                                {{ $pendaftaran->telepon_ibu }}
                            </td>
                            <td>
                                {{ $pendaftaran->pekerjaan_ibu }}
                            </td>
                            <td>
                                {{ $pendaftaran->penghasilan_ibu_label }}
                            </td>
                            <td>
                                {{ $pendaftaran->alamat_ortu }}
                            </td>
                            <td>
                                {{ $pendaftaran->pilihan_kelas_label }}
                            </td>
                            <td>
                                {{ $pendaftaran->nama_wali }}
                            </td>
                            <td>
                                {{ $pendaftaran->pekerjaan_wali }}
                            </td>
                            <td>
                                {{ $pendaftaran->jarak_rumah }}
                            </td>
                            <td>
                                {{ $pendaftaran->waktu_tempuh }}
                            </td>
                            <td>
                                {{ $pendaftaran->biaya_administrasi }}
                            </td>
                            <td>
                                {{ $pendaftaran->kartu_keluarga }}
                            </td>
                            <td>
                                {{ $pendaftaran->akta_kelahiran }}
                            </td>
                            <td>
                                {{ $pendaftaran->pas_foto }}
                            </td>
                            <td>
                                <div class="flex justify-end">
                                    @can('pendaftaran_show')
                                        <a class="mr-2 btn btn-sm btn-info" href="{{ route('admin.pendaftarans.show', $pendaftaran) }}">
                                            {{ trans('global.view') }}
                                        </a>
                                    @endcan
                                    @can('pendaftaran_edit')
                                        <a class="mr-2 btn btn-sm btn-success" href="{{ route('admin.pendaftarans.edit', $pendaftaran) }}">
                                            {{ trans('global.edit') }}
                                        </a>
                                    @endcan
                                    @can('pendaftaran_delete')
                                        <button class="mr-2 btn btn-sm btn-rose" type="button" wire:click="confirm('delete', {{ $pendaftaran->id }})" wire:loading.attr="disabled">
                                            {{ trans('global.delete') }}
                                        </button>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="10">No entries found.</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

    <div class="card-body">
        <div class="pt-3">
            @if($this->selectedCount)
                <p class="text-sm leading-5">
                    <span class="font-medium">
                        {{ $this->selectedCount }}
                    </span>
                    {{ __('Entries selected') }}
                </p>
            @endif
            {{ $pendaftarans->links() }}
        </div>
    </div>
</div>

@push('scripts')
    <script>
        Livewire.on('confirm', e => {
    if (!confirm("{{ trans('global.areYouSure') }}")) {
        return
    }
@this[e.callback](...e.argv)
})
    </script>
@endpush
