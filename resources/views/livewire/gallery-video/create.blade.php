<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('galleryVideo.title') ? 'invalid' : '' }}">
        <label class="form-label" for="title">{{ trans('cruds.galleryVideo.fields.title') }}</label>
        <input class="form-control" type="text" name="title" id="title" wire:model.defer="galleryVideo.title">
        <div class="validation-message">
            {{ $errors->first('galleryVideo.title') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.galleryVideo.fields.title_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('galleryVideo.desc') ? 'invalid' : '' }}">
        <label class="form-label" for="desc">{{ trans('cruds.galleryVideo.fields.desc') }}</label>
        <textarea class="form-control" name="desc" id="desc" wire:model.defer="galleryVideo.desc" rows="4"></textarea>
        <div class="validation-message">
            {{ $errors->first('galleryVideo.desc') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.galleryVideo.fields.desc_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('galleryVideo.link') ? 'invalid' : '' }}">
        <label class="form-label" for="link">{{ trans('cruds.galleryVideo.fields.link') }}</label>
        <input class="form-control" type="text" name="link" id="link" wire:model.defer="galleryVideo.link">
        <div class="validation-message">
            {{ $errors->first('galleryVideo.link') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.galleryVideo.fields.link_helper') }}
        </div>
    </div>
    @if (auth()->user()->school_unit_id == 0)
    <div class="form-group {{ $errors->has('galleryVideo.school_unit_id') ? 'invalid' : '' }}">
        <label class="form-label" for="school_unit">{{ trans('cruds.galleryVideo.fields.school_unit') }}</label>
        <x-select-list class="form-control" id="school_unit" name="school_unit" :options="$this->listsForFields['school_unit']" wire:model="galleryVideo.school_unit_id" />
        <div class="validation-message">
            {{ $errors->first('galleryVideo.school_unit_id') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.galleryVideo.fields.school_unit_helper') }}
        </div>
    </div>
    @endif

    <div class="form-group">
        <button class="btn btn-indigo mr-2" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.gallery-videos.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>
