<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('event.title') ? 'invalid' : '' }}">
        <label class="form-label" for="title">{{ trans('cruds.event.fields.title') }}</label>
        <input class="form-control" type="text" name="title" id="title" wire:model.defer="event.title">
        <div class="validation-message">
            {{ $errors->first('event.title') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.event.fields.title_helper') }}
        </div>
    </div>
    {{-- <div class="form-group {{ $errors->has('mediaCollections.event_cover') ? 'invalid' : '' }}">
        <label class="form-label" for="cover">{{ trans('cruds.event.fields.cover') }}</label>
        <x-dropzone id="cover" name="cover" action="{{ route('admin.events.storeMedia') }}" collection-name="event_cover" max-file-size="1" max-width="4096" max-height="4096" max-files="2" />
        <div class="validation-message">
            {{ $errors->first('mediaCollections.event_cover') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.event.fields.cover_helper') }}
        </div>
    </div> --}}
    <div class="form-group {{ $errors->has('event.from_date') ? 'invalid' : '' }}">
        <label class="form-label" for="from_date">{{ trans('cruds.event.fields.from_date') }}</label>
        <x-date-picker class="form-control" wire:model="event.from_date" id="from_date" name="from_date" picker="date" />
        <div class="validation-message">
            {{ $errors->first('event.from_date') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.event.fields.from_date_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('event.to_date') ? 'invalid' : '' }}">
        <label class="form-label" for="to_date">{{ trans('cruds.event.fields.to_date') }}</label>
        <x-date-picker class="form-control" wire:model="event.to_date" id="to_date" name="to_date" picker="date" />
        <div class="validation-message">
            {{ $errors->first('event.to_date') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.event.fields.to_date_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('event.location') ? 'invalid' : '' }}">
        <label class="form-label" for="location">{{ trans('cruds.event.fields.location') }}</label>
        <input class="form-control" type="text" name="location" id="location" wire:model.defer="event.location">
        <div class="validation-message">
            {{ $errors->first('event.location') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.event.fields.location_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('event.desc') ? 'invalid' : '' }}" wire:ignore>
        <label class="form-label" for="desc">{{ trans('cruds.event.fields.desc') }}</label>
        <textarea class="form-control" name="desc" id="desc" wire:model.defer="event.desc" rows="4"
            wire:key="ckeditor-1"
            x-data
            x-init="
            CKEDITOR.replace('desc', {
                filebrowserImageBrowseUrl: '/filemanager?type=Images',
                filebrowserBrowseUrl: '/filemanager?type=Files',
            });
            CKEDITOR.instances.desc.on('change', function() {
                $dispatch('input', this.getData());
            });"
            >
        </textarea>
        <div class="validation-message">
            {{ $errors->first('event.desc') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.event.fields.desc_helper') }}
        </div>
    </div>

    @if (auth()->user()->school_unit_id == 0)
    <div class="form-group {{ $errors->has('event.school_unit_id') ? 'invalid' : '' }}">
        <label class="form-label" for="school_unit">{{ trans('cruds.event.fields.school_unit') }}</label>
        <x-select-list class="form-control" id="school_unit" name="school_unit" :options="$this->listsForFields['school_unit']" wire:model="event.school_unit_id" />
        <div class="validation-message">
            {{ $errors->first('event.school_unit_id') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.event.fields.school_unit_helper') }}
        </div>
    </div>
    @endif

    <div class="form-group">
        <button class="mr-2 btn btn-indigo" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.events.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>
