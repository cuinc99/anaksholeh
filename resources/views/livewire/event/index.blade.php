<div>
    <div class="card-controls sm:flex">
        <div class="w-full sm:w-1/2">
            Per page:
            <select wire:model="perPage" class="form-select w-full sm:w-1/6">
                @foreach($paginationOptions as $value)
                    <option value="{{ $value }}">{{ $value }}</option>
                @endforeach
            </select>

            <button class="btn btn-rose ml-3 disabled:opacity-50 disabled:cursor-not-allowed" type="button" wire:click="confirm('deleteSelected')" wire:loading.attr="disabled" {{ $this->selectedCount ? '' : 'disabled' }}>
                {{ __('Delete Selected') }}
            </button>

        </div>
        <div class="w-full sm:w-1/2 sm:text-right">
            Search:
            <input type="text" wire:model.debounce.300ms="search" class="w-full sm:w-1/3 inline-block" />
        </div>
    </div>
    <div wire:loading.delay class="col-12 alert alert-info">
        Loading...
    </div>
    <table class="table table-index w-full">
        <thead>
            <tr>
                <th class="w-9">
                </th>
                <th class="w-10">
                    {{ trans('cruds.event.fields.id') }}
                    @include('components.table.sort', ['field' => 'id'])
                </th>
                <th>
                    {{ trans('cruds.event.fields.title') }}
                    @include('components.table.sort', ['field' => 'title'])
                </th>
                <th>
                    {{ trans('cruds.event.fields.from_date') }}
                    @include('components.table.sort', ['field' => 'from_date'])
                </th>
                <th>
                    {{ trans('cruds.event.fields.to_date') }}
                    @include('components.table.sort', ['field' => 'to_date'])
                </th>
                <th>
                    {{ trans('cruds.event.fields.location') }}
                    @include('components.table.sort', ['field' => 'location'])
                </th>
                <th>
                    {{ trans('cruds.event.fields.school_unit') }}
                    @include('components.table.sort', ['field' => 'school_unit.name'])
                </th>
                <th>
                </th>
            </tr>
        </thead>
        <tbody>
            @forelse($events as $event)
                <tr>
                    <td>
                        <input type="checkbox" value="{{ $event->id }}" wire:model="selected">
                    </td>
                    <td>
                        {{ $loop->iteration }}
                    </td>
                    <td>
                        {{ Str::limit($event->title, 50)}}
                    </td>
                    <td>
                        {{ $event->from_date }}
                    </td>
                    <td>
                        {{ $event->to_date }}
                    </td>
                    <td>
                        {{ $event->location }}
                    </td>
                    <td>
                        @if($event->SchoolUnit)
                            <span class="badge badge-relationship">{{ $event->SchoolUnit->name ?? '' }}</span>
                        @endif
                    </td>
                    <td>
                        <div class="flex justify-end">
                            @can('event_show')
                                <a class="btn btn-sm btn-info mr-2" href="{{ route('admin.events.show', $event) }}">
                                    {{ trans('global.view') }}
                                </a>
                            @endcan
                            @can('event_edit')
                                <a class="btn btn-sm btn-success mr-2" href="{{ route('admin.events.edit', $event) }}">
                                    {{ trans('global.edit') }}
                                </a>
                            @endcan
                            @can('event_delete')
                                <button class="btn btn-sm btn-rose mr-2" type="button" wire:click="confirm('delete', {{ $event->id }})" wire:loading.attr="disabled">
                                    {{ trans('global.delete') }}
                                </button>
                            @endcan
                        </div>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="10">No entries found.</td>
                </tr>
            @endforelse
        </tbody>
    </table>

    <div class="card-body">
        <div class="pt-3">
            @if($this->selectedCount)
                <p class="text-sm leading-5">
                    <span class="font-medium">
                        {{ $this->selectedCount }}
                    </span>
                    {{ __('Entries selected') }}
                </p>
            @endif
            {{ $events->links() }}
        </div>
    </div>
</div>

@push('scripts')
    <script>
        Livewire.on('confirm', e => {
    if (!confirm("{{ trans('global.areYouSure') }}")) {
        return
    }
@this[e.callback](...e.argv)
})
    </script>
@endpush
