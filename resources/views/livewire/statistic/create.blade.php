<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('statistic.school_unit') ? 'invalid' : '' }}">
        <label class="form-label" for="school_unit">{{ trans('cruds.statistic.fields.school_unit') }}</label>
        <input class="form-control" type="number" name="school_unit" id="school_unit" wire:model.defer="statistic.school_unit" step="1">
        <div class="validation-message">
            {{ $errors->first('statistic.school_unit') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.statistic.fields.school_unit_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('statistic.teacher') ? 'invalid' : '' }}">
        <label class="form-label" for="teacher">{{ trans('cruds.statistic.fields.teacher') }}</label>
        <input class="form-control" type="number" name="teacher" id="teacher" wire:model.defer="statistic.teacher" step="1">
        <div class="validation-message">
            {{ $errors->first('statistic.teacher') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.statistic.fields.teacher_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('statistic.employee') ? 'invalid' : '' }}">
        <label class="form-label" for="employee">{{ trans('cruds.statistic.fields.employee') }}</label>
        <input class="form-control" type="number" name="employee" id="employee" wire:model.defer="statistic.employee" step="1">
        <div class="validation-message">
            {{ $errors->first('statistic.employee') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.statistic.fields.employee_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('statistic.student') ? 'invalid' : '' }}">
        <label class="form-label" for="student">{{ trans('cruds.statistic.fields.student') }}</label>
        <input class="form-control" type="number" name="student" id="student" wire:model.defer="statistic.student" step="1">
        <div class="validation-message">
            {{ $errors->first('statistic.student') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.statistic.fields.student_helper') }}
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-indigo mr-2" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.statistics.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>