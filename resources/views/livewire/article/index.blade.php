<div>
    <div class="card-controls sm:flex">
        <div class="w-full sm:w-1/2">
            Per page:
            <select wire:model="perPage" class="form-select w-full sm:w-1/6">
                @foreach($paginationOptions as $value)
                    <option value="{{ $value }}">{{ $value }}</option>
                @endforeach
            </select>

            <button class="btn btn-rose ml-3 disabled:opacity-50 disabled:cursor-not-allowed" type="button" wire:click="confirm('deleteSelected')" wire:loading.attr="disabled" {{ $this->selectedCount ? '' : 'disabled' }}>
                {{ __('Delete Selected') }}
            </button>

        </div>
        <div class="w-full sm:w-1/2 sm:text-right">
            Search:
            <input type="text" wire:model.debounce.300ms="search" class="w-full sm:w-1/3 inline-block" />
        </div>
    </div>
    <div wire:loading.delay class="col-12 alert alert-info">
        Loading...
    </div>
    <table class="table table-index w-full">
        <thead>
            <tr>
                <th class="w-9">
                </th>
                <th class="w-28">
                    NO
                </th>
                <th>
                    {{ trans('cruds.article.fields.title') }}
                    @include('components.table.sort', ['field' => 'title'])
                </th>
                <th>
                    {{ trans('cruds.article.fields.school_unit') }}
                    @include('components.table.sort', ['field' => 'school_unit.name'])
                </th>
                <th>
                    {{ trans('cruds.article.fields.category') }}
                    @include('components.table.sort', ['field' => 'category.name'])
                </th>
                <th>
                    {{ trans('cruds.article.fields.published_at') }}
                    @include('components.table.sort', ['field' => 'published_at'])
                </th>
                <th>
                    visit
                    @include('components.table.sort', ['field' => 'visit'])
                </th>
                <th>
                </th>
            </tr>
        </thead>
        <tbody>
            @forelse($articles as $article)
                <tr>
                    <td>
                        <input type="checkbox" value="{{ $article->id }}" wire:model="selected">
                    </td>
                    <td width="1%">
                        {{ $loop->iteration }}
                    </td>
                    <td>
                        {{ Str::limit($article->title, 50) }}
                    </td>
                    <td>
                        @if($article->schoolUnit)
                            <span class="badge badge-relationship">{{ $article->schoolUnit->name ?? '' }}</span>
                        @endif
                    </td>
                    <td>
                        @if($article->category)
                        @if ($article->category->name == 'Berita')
                        <span class="badge badge-info">{{ $article->category->name ?? '' }}</span>
                        @elseif ($article->category->name == 'Pengumuman')
                        <span class="badge badge-success">{{ $article->category->name ?? '' }}</span>
                        @elseif ($article->category->name == 'Artikel')
                        <span class="badge badge-primary">{{ $article->category->name ?? '' }}</span>
                        @else
                        <span class="badge badge-danger">No Kategori</span>
                        @endif
                        @endif
                    </td>
                    <td>
                        {{ Date::parse($article->published_at)->diffForHumans() }}
                    </td>
                    <td>
                        {{ $article->visit ?? '0' }} View
                    </td>
                    <td>
                        <div class="flex justify-end">
                            @can('article_show')
                                <a class="btn btn-sm btn-info mr-2" href="{{ route('admin.articles.show', $article) }}">
                                    {{ trans('global.view') }}
                                </a>
                            @endcan
                            @can('article_edit')
                                <a class="btn btn-sm btn-success mr-2" href="{{ route('admin.articles.edit', $article) }}">
                                    {{ trans('global.edit') }}
                                </a>
                            @endcan
                            @can('article_delete')
                                <button class="btn btn-sm btn-rose mr-2" type="button" wire:click="confirm('delete', {{ $article->id }})" wire:loading.attr="disabled">
                                    {{ trans('global.delete') }}
                                </button>
                            @endcan
                        </div>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="10">No entries found.</td>
                </tr>
            @endforelse
        </tbody>
    </table>

    <div class="card-body">
        <div class="pt-3">
            @if($this->selectedCount)
                <p class="text-sm leading-5">
                    <span class="font-medium">
                        {{ $this->selectedCount }}
                    </span>
                    {{ __('Entries selected') }}
                </p>
            @endif
            {{ $articles->links() }}
        </div>
    </div>
</div>

@push('scripts')
    <script>
        Livewire.on('confirm', e => {
    if (!confirm("{{ trans('global.areYouSure') }}")) {
        return
    }
@this[e.callback](...e.argv)
})
    </script>
@endpush
