<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('article.title') ? 'invalid' : '' }}">
        <label class="form-label" for="title">{{ trans('cruds.article.fields.title') }}</label>
        <input class="form-control" type="text" name="title" id="title" wire:model.defer="article.title">
        <div class="validation-message">
            {{ $errors->first('article.title') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.article.fields.title_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('article.content') ? 'invalid' : '' }}" wire:ignore>
        <label class="form-label" for="content">{{ trans('cruds.article.fields.content') }}</label>
        <textarea
            class="form-control"
            id="content"
            wire:model.defer="article.content"
            wire:key="ckeditor-1"
            x-data
            x-init="
            CKEDITOR.replace('content', {
                filebrowserImageBrowseUrl: '/filemanager?type=Images',
                filebrowserBrowseUrl: '/filemanager?type=Files',
            });
            CKEDITOR.instances.content.on('change', function() {
                $dispatch('input', this.getData());
            });"
            >
        </textarea>
        <div class="validation-message">
            {{ $errors->first('article.content') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.article.fields.content_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('mediaCollections.article_cover') ? 'invalid' : '' }}">
        <label class="form-label" for="cover">{{ trans('cruds.article.fields.cover') }}</label>
        <x-dropzone id="cover" name="cover" action="{{ route('admin.articles.storeMedia') }}" collection-name="article_cover" max-file-size="1" max-width="4096" max-height="4096" max-files="2" />
        <div class="validation-message">
            {{ $errors->first('mediaCollections.article_cover') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.article.fields.cover_helper') }}
        </div>
    </div>
    @if (auth()->user()->school_unit_id == 0)
    <div class="form-group {{ $errors->has('article.school_unit_id') ? 'invalid' : '' }}">
        <label class="form-label" for="school_unit">{{ trans('cruds.article.fields.school_unit') }}</label>
        <x-select-list class="form-control" id="school_unit" name="school_unit" :options="$this->listsForFields['school_unit']" wire:model="article.school_unit_id" />
        <div class="validation-message">
            {{ $errors->first('article.school_unit_id') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.article.fields.school_unit_helper') }}
        </div>
    </div>
    @endif
    <div class="form-group {{ $errors->has('article.category_id') ? 'invalid' : '' }}">
        <label class="form-label" for="category">{{ trans('cruds.article.fields.category') }}</label>
        <x-select-list class="form-control" id="category" name="category" :options="$this->listsForFields['category']" wire:model="article.category_id" />
        <div class="validation-message">
            {{ $errors->first('article.category_id') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.article.fields.category_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('article.published_at') ? 'invalid' : '' }}">
        <label class="form-label" for="published_at">{{ trans('cruds.article.fields.published_at') }}</label>
        <x-date-picker class="form-control" wire:model="article.published_at" id="published_at" name="published_at" />
        <div class="validation-message">
            {{ $errors->first('article.published_at') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.article.fields.published_at_helper') }}
        </div>
    </div>
    {{-- <div class="form-group {{ $errors->has('article.is_slider') ? 'invalid' : '' }}">
        <label class="form-label">{{ trans('cruds.article.fields.is_slider') }}</label>
        @foreach($this->listsForFields['is_slider'] as $key => $value)
            <label class="radio-label"><input type="radio" name="is_slider" wire:model="article.is_slider" value="{{ $key }}">{{ $value }}</label>
        @endforeach
        <div class="validation-message">
            {{ $errors->first('article.is_slider') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.article.fields.is_slider_helper') }}
        </div>
    </div> --}}

    <div class="form-group">
        <button class="btn btn-indigo mr-2" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.articles.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>
