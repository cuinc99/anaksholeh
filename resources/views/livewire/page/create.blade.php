<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('page.title') ? 'invalid' : '' }}">
        <label class="form-label" for="title">{{ trans('cruds.page.fields.title') }}</label>
        <input class="form-control" type="text" name="title" id="title" wire:model.defer="page.title">
        <div class="validation-message">
            {{ $errors->first('page.title') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.page.fields.title_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('page.content') ? 'invalid' : '' }}" wire:ignore>
        <label class="form-label" for="content">{{ trans('cruds.page.fields.content') }}</label>
        <textarea
            class="form-control"
            id="content"
            wire:model.defer="page.content"
            wire:key="ckeditor-1"
            x-data
            x-init="
            CKEDITOR.replace('content', {
                filebrowserImageBrowseUrl: '/filemanager?type=Images',
                filebrowserBrowseUrl: '/filemanager?type=Files',
            });
            CKEDITOR.instances.content.on('change', function() {
                $dispatch('input', this.getData());
            });"
            >
        </textarea>
        <div class="validation-message">
            {{ $errors->first('page.content') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.page.fields.content_helper') }}
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-indigo mr-2" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.pages.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>
