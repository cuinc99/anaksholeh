<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('tahunAjaran.name') ? 'invalid' : '' }}">
        <label class="form-label required" for="name">{{ trans('cruds.tahunAjaran.fields.name') }}</label>
        <input class="form-control" type="text" name="name" id="name" required wire:model.defer="tahunAjaran.name">
        <div class="validation-message">
            {{ $errors->first('tahunAjaran.name') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.tahunAjaran.fields.name_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('tahunAjaran.description') ? 'invalid' : '' }}">
        <label class="form-label" for="description">{{ trans('cruds.tahunAjaran.fields.description') }}</label>
        <textarea class="form-control" name="description" id="description" wire:model.defer="tahunAjaran.description" rows="4"></textarea>
        <div class="validation-message">
            {{ $errors->first('tahunAjaran.description') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.tahunAjaran.fields.description_helper') }}
        </div>
    </div>

    @if (auth()->user()->school_unit_id == 0)
    <div class="form-group {{ $errors->has('tahunAjaran.school_unit_id') ? 'invalid' : '' }}">
        <label class="form-label" for="school_unit">{{ trans('cruds.tahunAjaran.fields.school_unit') }}</label>
        <x-select-list class="form-control" id="school_unit" name="school_unit" :options="$this->listsForFields['school_unit']" wire:model="tahunAjaran.school_unit_id" />
        <div class="validation-message">
            {{ $errors->first('tahunAjaran.school_unit_id') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.tahunAjaran.fields.school_unit_helper') }}
        </div>
    </div>
    @else
    <input type="hidden" name="school_unit" value="{{ auth()->user()->school_unit_id }}">
    @endif

    <div class="form-group">
        <button class="mr-2 btn btn-indigo" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.tahun-ajarans.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>
