<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('poster.title') ? 'invalid' : '' }}">
        <label class="form-label" for="title">{{ trans('cruds.poster.fields.title') }}</label>
        <input class="form-control" type="text" name="title" id="title" wire:model.defer="poster.title">
        <div class="validation-message">
            {{ $errors->first('poster.title') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.poster.fields.title_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('poster.desc') ? 'invalid' : '' }}">
        <label class="form-label" for="desc">{{ trans('cruds.poster.fields.desc') }}</label>
        <textarea class="form-control" name="desc" id="desc" rows="4" wire:model.defer="poster.desc"></textarea>
        <div class="validation-message">
            {{ $errors->first('poster.desc') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.poster.fields.desc_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('mediaCollections.poster_image') ? 'invalid' : '' }}">
        <label class="form-label" for="image">{{ trans('cruds.poster.fields.image') }}</label>
        <x-dropzone id="image" name="image" action="{{ route('admin.posters.storeMedia') }}" collection-name="poster_image" max-file-size="1" max-width="4096" max-height="4096" max-files="1" />
        <div class="validation-message">
            {{ $errors->first('mediaCollections.poster_image') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.poster.fields.image_helper') }}
        </div>
    </div>


    @if (auth()->user()->school_unit_id == 0)
    <div class="form-group {{ $errors->has('poster.school_unit_id') ? 'invalid' : '' }}">
        <label class="form-label" for="school_unit">{{ trans('cruds.poster.fields.school_unit') }}</label>
        <x-select-list class="form-control" id="school_unit" name="school_unit" :options="$this->listsForFields['school_unit']" wire:model="poster.school_unit_id" />
        <div class="validation-message">
            {{ $errors->first('poster.school_unit_id') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.poster.fields.school_unit_helper') }}
        </div>
    </div>
    @endif

    <div class="form-group">
        <button class="btn btn-indigo mr-2" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.posters.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>
