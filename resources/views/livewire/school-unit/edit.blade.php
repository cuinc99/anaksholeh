<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('schoolUnit.name') ? 'invalid' : '' }}">
        <label class="form-label" for="name">{{ trans('cruds.schoolUnit.fields.name') }}</label>
        <input class="form-control" type="text" name="name" id="name" wire:model.defer="schoolUnit.name">
        <div class="validation-message">
            {{ $errors->first('schoolUnit.name') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.schoolUnit.fields.name_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('mediaCollections.school_unit_logo') ? 'invalid' : '' }}">
        <label class="form-label" for="logo">{{ trans('cruds.schoolUnit.fields.logo') }}</label>
        <x-dropzone id="logo" name="logo" action="{{ route('admin.school-units.storeMedia') }}" collection-name="school_unit_logo" max-file-size="1" max-width="4096" max-height="4096" max-files="2" />
        <div class="validation-message">
            {{ $errors->first('mediaCollections.school_unit_logo') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.schoolUnit.fields.logo_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('schoolUnit.profile') ? 'invalid' : '' }}" wire:ignore>
        <label class="form-label" for="profile">{{ trans('cruds.schoolUnit.fields.profile') }}</label>
        <textarea
            class="form-control"
            id="profile"
            wire:model.defer="schoolUnit.profile"
            wire:key="ckeditor-1"
            x-data
            x-init="
            CKEDITOR.replace('profile', {
                filebrowserImageBrowseUrl: '/filemanager?type=Images',
                filebrowserBrowseUrl: '/filemanager?type=Files',
            });
            CKEDITOR.instances.profile.on('change', function() {
                $dispatch('input', this.getData());
            });"
            >
        </textarea>
        <div class="validation-message">
            {{ $errors->first('schoolUnit.profile') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.schoolUnit.fields.profile_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('schoolUnit.vision_mission') ? 'invalid' : '' }}" wire:ignore>
        <label class="form-label" for="vision_mission">{{ trans('cruds.schoolUnit.fields.vision_mission') }}</label>
        <textarea
            class="form-control"
            id="vision_mission"
            wire:model.defer="schoolUnit.vision_mission"
            wire:key="ckeditor-2"
            x-data
            x-init="
            CKEDITOR.replace('vision_mission', {
                filebrowserImageBrowseUrl: '/filemanager?type=Images',
                filebrowserBrowseUrl: '/filemanager?type=Files',
            });
            CKEDITOR.instances.vision_mission.on('change', function() {
                $dispatch('input', this.getData());
            });"
            >
        </textarea>
        <div class="validation-message">
            {{ $errors->first('schoolUnit.vision_mission') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.schoolUnit.fields.vision_mission_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('schoolUnit.location') ? 'invalid' : '' }}">
        <label class="form-label" for="location">{{ trans('cruds.schoolUnit.fields.location') }}</label>
        <input class="form-control" type="text" name="location" id="location" wire:model.defer="schoolUnit.location">
        <div class="validation-message">
            {{ $errors->first('schoolUnit.location') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.schoolUnit.fields.location_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('schoolUnit.fb') ? 'invalid' : '' }}">
        <label class="form-label" for="fb">{{ trans('cruds.schoolUnit.fields.fb') }}</label>
        <input class="form-control" type="text" name="fb" id="fb" wire:model.defer="schoolUnit.fb" placeholder="Username Facebook">
        <div class="validation-message">
            {{ $errors->first('schoolUnit.fb') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.schoolUnit.fields.fb_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('schoolUnit.ig') ? 'invalid' : '' }}">
        <label class="form-label" for="ig">{{ trans('cruds.schoolUnit.fields.ig') }}</label>
        <input class="form-control" type="text" name="ig" id="ig" wire:model.defer="schoolUnit.ig" placeholder="Username Instagram">
        <div class="validation-message">
            {{ $errors->first('schoolUnit.ig') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.schoolUnit.fields.ig_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('schoolUnit.tw') ? 'invalid' : '' }}">
        <label class="form-label" for="tw">{{ trans('cruds.schoolUnit.fields.tw') }}</label>
        <input class="form-control" type="text" name="tw" id="tw" wire:model.defer="schoolUnit.tw" placeholder="Username Twitter">
        <div class="validation-message">
            {{ $errors->first('schoolUnit.tw') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.schoolUnit.fields.tw_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('schoolUnit.yt') ? 'invalid' : '' }}">
        <label class="form-label" for="yt">{{ trans('cruds.schoolUnit.fields.yt') }}</label>
        <input class="form-control" type="text" name="yt" id="yt" wire:model.defer="schoolUnit.yt" placeholder="Link Channel Youtube">
        <div class="validation-message">
            {{ $errors->first('schoolUnit.yt') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.schoolUnit.fields.yt_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('schoolUnit.wa') ? 'invalid' : '' }}">
        <label class="form-label" for="wa">{{ trans('cruds.schoolUnit.fields.wa') }}</label>
        <input class="form-control" type="text" name="wa" id="wa" wire:model.defer="schoolUnit.wa" placeholder="Nomor Whatsapp">
        <div class="validation-message">
            {{ $errors->first('schoolUnit.wa') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.schoolUnit.fields.wa_helper') }}
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-indigo mr-2" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.school-units.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>
