<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('testimonial.name') ? 'invalid' : '' }}">
        <label class="form-label" for="name">{{ trans('cruds.testimonial.fields.name') }}</label>
        <input class="form-control" type="text" name="name" id="name" wire:model.defer="testimonial.name">
        <div class="validation-message">
            {{ $errors->first('testimonial.name') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.testimonial.fields.name_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('testimonial.message') ? 'invalid' : '' }}">
        <label class="form-label" for="message">{{ trans('cruds.testimonial.fields.message') }}</label>
        <textarea class="form-control" name="message" id="message" wire:model.defer="testimonial.message" rows="4"></textarea>
        <div class="validation-message">
            {{ $errors->first('testimonial.message') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.testimonial.fields.message_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('mediaCollections.testimonial_cover') ? 'invalid' : '' }}">
        <label class="form-label" for="cover">{{ trans('cruds.testimonial.fields.cover') }}</label>
        <x-dropzone id="cover" name="cover" action="{{ route('admin.testimonials.storeMedia') }}" collection-name="testimonial_cover" max-file-size="1" max-width="4096" max-height="4096" max-files="2" />
        <div class="validation-message">
            {{ $errors->first('mediaCollections.testimonial_cover') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.testimonial.fields.cover_helper') }}
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-indigo mr-2" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.testimonials.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>
