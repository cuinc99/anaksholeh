<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('relatedLink.name') ? 'invalid' : '' }}">
        <label class="form-label" for="name">{{ trans('cruds.relatedLink.fields.name') }}</label>
        <input class="form-control" type="text" name="name" id="name" wire:model.defer="relatedLink.name">
        <div class="validation-message">
            {{ $errors->first('relatedLink.name') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.relatedLink.fields.name_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('relatedLink.url') ? 'invalid' : '' }}">
        <label class="form-label" for="url">{{ trans('cruds.relatedLink.fields.url') }}</label>
        <input class="form-control" type="text" name="url" id="url" wire:model.defer="relatedLink.url">
        <div class="validation-message">
            {{ $errors->first('relatedLink.url') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.relatedLink.fields.url_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('mediaCollections.related_link_logo') ? 'invalid' : '' }}">
        <label class="form-label" for="logo">{{ trans('cruds.relatedLink.fields.logo') }}</label>
        <x-dropzone id="logo" name="logo" action="{{ route('admin.related-links.storeMedia') }}" collection-name="related_link_logo" max-file-size="1" max-width="4096" max-height="4096" max-files="2" />
        <div class="validation-message">
            {{ $errors->first('mediaCollections.related_link_logo') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.relatedLink.fields.logo_helper') }}
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-indigo mr-2" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.related-links.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>
