<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('galleryPhoto.title') ? 'invalid' : '' }}">
        <label class="form-label" for="title">{{ trans('cruds.galleryPhoto.fields.title') }}</label>
        <input class="form-control" type="text" name="title" id="title" wire:model.defer="galleryPhoto.title">
        <div class="validation-message">
            {{ $errors->first('galleryPhoto.title') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.galleryPhoto.fields.title_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('galleryPhoto.desc') ? 'invalid' : '' }}">
        <label class="form-label" for="desc">{{ trans('cruds.galleryPhoto.fields.desc') }}</label>
        <textarea class="form-control" name="desc" id="desc" wire:model.defer="galleryPhoto.desc" rows="4"></textarea>
        <div class="validation-message">
            {{ $errors->first('galleryPhoto.desc') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.galleryPhoto.fields.desc_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('mediaCollections.gallery_photo_photo') ? 'invalid' : '' }}">
        <label class="form-label" for="photo">{{ trans('cruds.galleryPhoto.fields.photo') }}</label>
        <x-dropzone id="photo" name="photo" action="{{ route('admin.gallery-photos.storeMedia') }}" collection-name="gallery_photo_photo" max-file-size="1" max-width="4096" max-height="4096" />
        <div class="validation-message">
            {{ $errors->first('mediaCollections.gallery_photo_photo') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.galleryPhoto.fields.photo_helper') }}
        </div>
    </div>


    @if (auth()->user()->school_unit_id == 0)
    <div class="form-group {{ $errors->has('galleryPhoto.school_unit_id') ? 'invalid' : '' }}">
        <label class="form-label" for="school_unit">{{ trans('cruds.galleryPhoto.fields.school_unit') }}</label>
        <x-select-list class="form-control" id="school_unit" name="school_unit" :options="$this->listsForFields['school_unit']" wire:model="galleryPhoto.school_unit_id" />
        <div class="validation-message">
            {{ $errors->first('galleryPhoto.school_unit_id') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.galleryPhoto.fields.school_unit_helper') }}
        </div>
    </div>
    @else
    <input type="hidden" name="school_unit" value="{{ auth()->user()->school_unit_id }}">
    @endif

    <div class="form-group">
        <button class="btn btn-indigo mr-2" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.gallery-photos.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>
