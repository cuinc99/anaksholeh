<form wire:submit.prevent="submit" class="pt-3">

    <div class="form-group {{ $errors->has('contactUs.name') ? 'invalid' : '' }}">
        <label class="form-label required" for="name">{{ trans('cruds.contactUs.fields.name') }}</label>
        <input class="form-control" type="text" name="name" id="name" required wire:model.defer="contactUs.name">
        <div class="validation-message">
            {{ $errors->first('contactUs.name') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.contactUs.fields.name_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('contactUs.number') ? 'invalid' : '' }}">
        <label class="form-label required" for="number">{{ trans('cruds.contactUs.fields.number') }}</label>
        <input class="form-control" type="text" name="number" id="number" required wire:model.defer="contactUs.number">
        <div class="validation-message">
            {{ $errors->first('contactUs.number') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.contactUs.fields.number_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('contactUs.message') ? 'invalid' : '' }}">
        <label class="form-label required" for="message">{{ trans('cruds.contactUs.fields.message') }}</label>
        <textarea class="form-control" name="message" id="message" required wire:model.defer="contactUs.message" rows="4"></textarea>
        <div class="validation-message">
            {{ $errors->first('contactUs.message') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.contactUs.fields.message_helper') }}
        </div>
    </div>
    <div class="form-group {{ $errors->has('contactUs.school_unit_id') ? 'invalid' : '' }}">
        <label class="form-label required" for="school_unit">{{ trans('cruds.contactUs.fields.school_unit') }}</label>
        <x-select-list class="form-control" required id="school_unit" name="school_unit" :options="$this->listsForFields['school_unit']" wire:model="contactUs.school_unit_id" />
        <div class="validation-message">
            {{ $errors->first('contactUs.school_unit_id') }}
        </div>
        <div class="help-block">
            {{ trans('cruds.contactUs.fields.school_unit_helper') }}
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-indigo mr-2" type="submit">
            {{ trans('global.save') }}
        </button>
        <a href="{{ route('admin.contactuses.index') }}" class="btn btn-secondary">
            {{ trans('global.cancel') }}
        </a>
    </div>
</form>