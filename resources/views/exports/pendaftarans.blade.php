<table>
    <thead>
        <tr>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                No
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.nama_lengkap') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.nama_pangilan') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.jenis_kelamin') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.nisn') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.golongan_darah') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.tempat_lahir') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.tanggal_lahir') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.nik') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.kewarganegaraan') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.anak_ke') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.jumlah_saudara') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.bahasa') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.asal_sekolah') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.alamat_siswa') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.kelas') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.jalur_masuk') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.ket_jalur_masuk') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.nama_ayah') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.tahun_lahir_ayah') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.pendidikan_ayah') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.telepon_ayah') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.pekerjaan_ayah') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.penghasilan_ayah') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.nama_ibu') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.tahun_lahir_ibu') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.pendidikan_ibu') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.telepon_ibu') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.pekerjaan_ibu') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.penghasilan_ibu') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.alamat_ortu') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.pilihan_kelas') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.nama_wali') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.pekerjaan_wali') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.telepon_wali') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.jarak_rumah') }}
            </th>
            <th style="font-weight: bold; background-color: #FDFF00; text-transform: uppercase; text-align: center;">
                {{ trans('cruds.pendaftaran.fields.waktu_tempuh') }}
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($pendaftarans as $pendaftaran)
            <tr>
                <td>
                    {{ $loop->iteration }}
                </td>
                <td>
                    {{ $pendaftaran->nama_lengkap }}
                </td>
                <td>
                    {{ $pendaftaran->nama_pangilan }}
                </td>
                <td>
                    {{ $pendaftaran->jenis_kelamin_label }}
                </td>
                <td>
                    {{ $pendaftaran->nisn }}
                </td>
                <td>
                    {{ $pendaftaran->golongan_darah_label }}
                </td>
                <td>
                    {{ $pendaftaran->tempat_lahir }}
                </td>
                <td>
                    {{ $pendaftaran->tanggal_lahir }}
                </td>
                <td>
                    {{ $pendaftaran->nik }}
                </td>
                <td>
                    {{ $pendaftaran->kewarganegaraan }}
                </td>
                <td>
                    {{ $pendaftaran->anak_ke }}
                </td>
                <td>
                    {{ $pendaftaran->jumlah_saudara }}
                </td>
                <td>
                    {{ $pendaftaran->bahasa }}
                </td>
                <td>
                    {{ $pendaftaran->asal_sekolah }}
                </td>
                <td>
                    {{ $pendaftaran->alamat_siswa }}
                </td>
                <td>
                    {{ $pendaftaran->kelas }}
                </td>
                <td>
                    {{ $pendaftaran->jalur_masuk }}
                </td>
                <td>
                    {{ $pendaftaran->ket_jalur_masuk }}
                </td>
                <td>
                    {{ $pendaftaran->nama_ayah }}
                </td>
                <td>
                    {{ $pendaftaran->tahun_lahir_ayah }}
                </td>
                <td>
                    {{ $pendaftaran->pendidikan_ayah }}
                </td>
                <td>
                    {{ $pendaftaran->telepon_ayah }}
                </td>
                <td>
                    {{ $pendaftaran->pekerjaan_ayah }}
                </td>
                <td>
                    {{ $pendaftaran->penghasilan_ayah_label }}
                </td>
                <td>
                    {{ $pendaftaran->nama_ibu }}
                </td>
                <td>
                    {{ $pendaftaran->tahun_lahir_ibu }}
                </td>
                <td>
                    {{ $pendaftaran->pendidikan_ibu }}
                </td>
                <td>
                    {{ $pendaftaran->telepon_ibu }}
                </td>
                <td>
                    {{ $pendaftaran->pekerjaan_ibu }}
                </td>
                <td>
                    {{ $pendaftaran->penghasilan_ibu_label }}
                </td>
                <td>
                    {{ $pendaftaran->alamat_ortu }}
                </td>
                <td>
                    {{ $pendaftaran->pilihan_kelas_label }}
                </td>
                <td>
                    {{ $pendaftaran->nama_wali }}
                </td>
                <td>
                    {{ $pendaftaran->pekerjaan_wali }}
                </td>
                <td>
                    {{ $pendaftaran->jarak_rumah }}
                </td>
                <td>
                    {{ $pendaftaran->waktu_tempuh }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
