@extends('frontend.layouts.base')

@section('title', $schoolUnit->name)

@section('content')
@if ($schoolUnit->logo->count())
@foreach($schoolUnit->logo as $key => $entry)
@php
    $background = $entry['background'];
@endphp
@endforeach
@else
@php
    $background = '/assets/images/software/bg.png'
@endphp
@endif
<!-- Hero Start -->
<section class="bg-half d-table w-100" style="background: url({{ $background }}) center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title text-white title-dark"> {{ Str::ucfirst(Request::segment(1)) }} {{ $schoolUnit->name ?? '' }} </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('schoolUnit', $schoolUnit->slug) }}">{{ $schoolUnit->name }}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Profil</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->

<section class="section">
    <div class="container">
        <div class="row justify-content-center" id="counter">
            <div class="col-lg-10">
                <div class="row">

                    <div class="col-md-12">
                        <h1>Profil</h1>
                        {!! $schoolUnit->profile !!}
                        <br>
                        <br>
                        <br>
                        <h1>Visi & Misi</h1>
                        {!! $schoolUnit->vision_mission !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
