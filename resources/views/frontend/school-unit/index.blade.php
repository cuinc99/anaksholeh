@extends('frontend.layouts.base')

@section('title', $schoolUnit->name)

@section('content')
    {{-- visi misi --}}
    @if ($schoolUnit->logo->count())
    @foreach($schoolUnit->logo as $key => $entry)
    @php
        $background = $entry['background'];
    @endphp
    @endforeach
    @else
    @php
        $background = '/assets/images/software/bg.png'
    @endphp
    @endif
     <!-- Hero Start -->
     <section class="section bg-primary d-table w-100" style="background: url({{ $background }}) center center; height: 100vh;">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row align-items-center position-relative mt-100" style="z-index: 1;">
                <div class="col-12">
                    <div class="mt-4 text-center title-heading text-lg-left">
                        <h1 class="mb-3 text-white heading title-dark">
                            <span class="element" data-elements="Yayasan Pendidikan Islam Terpadu ( IBNU Abbas ), Anak Sholeh Mataram"></span>
                            <br>
                            Lembaga {{ $schoolUnit->name ?? '' }}
                        </h1>
                        <p class="para-desc text-white-50">{!! Str::limit($schoolUnit->profile, 200) ?? '' !!}</p>
                        <div class="mt-4">
                            <a href="{{ route('profile', $schoolUnit->slug) }}" class="btn btn-light"><i class="mdi mdi-book"></i> Profil Lengkap</a>
                        </div>
                    </div>
                </div><!--end col-->


            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- Partners start -->
    <section class="py-4 bg-light border-bottom d-none d-sm-block">
        {{-- <div class="container"> --}}
        <marquee behavior="scroll" loop="infinite" direction="horizontal" scrolldelay="20" onmouseover="this.stop();"
            onmouseout="this.start();" style="width: 1900px; overflow: hidden;">
            <div class="row justify-content-center">
                @foreach ($announcements as $announcement)
                <div class="py-2 col-lg-4 col-4">
                    <a href="{{ route('articleDetail', [$schoolUnit->slug, 'pengumuman', $announcement->slug]) }}">
                        <div class="p-3 rounded shadow media key-feature align-items-center">
                            <img src="/assets/images/illustrator/announcement.svg" class="avatar avatar-ex-sm" alt="" style="max-height: 40px;">
                            <div class="ml-3 media-body">
                                <h4 class="mb-0 title text-dark">
                                    {{ Str::limit($announcement->title, 50) }}
                                </h4>
                                <p class="mb-0 text-muted">{{ Date::parse($announcement->published_at)->diffForHumans() }}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
            <!--end row-->
        </marquee>
        {{-- </div> --}}
        <!--end container-->
    </section>
    <!--end section-->

     <!-- Partners start -->
     <section class="py-4 bg-light border-bottom d-block d-sm-none">
        <div class="container">
            <div class="row justify-content-center">
                @foreach ($announcements as $announcement)
                <div class="py-2 col-12">
                    <a href="{{ route('articleDetail', [$schoolUnit->slug, 'pengumuman', $announcement->slug]) }}">
                        <div class="p-3 rounded shadow media key-feature align-items-center">
                            <img src="/assets/images/illustrator/announcement.svg" class="avatar avatar-ex-sm" alt="" style="max-height: 40px;">
                            <div class="ml-3 media-body">
                                <h4 class="mb-0 title text-dark">
                                    {{ Str::limit($announcement->title, 50) }}
                                </h4>
                                <p class="mb-0 text-muted">{{ Date::parse($announcement->published_at)->diffForHumans() }}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <!--end container-->
    </section>
    <!--end section-->

    {{-- social --}}
    <section class="section">
        <div class="container">
            <div class="row justify-content-center" id="counter">
                <div class="col-lg-8 col-md-10">
                    <div class="pb-2 mb-4 text-center">
                        <h5 class="mb-0 font-weight-normal text-muted">Ingin tahu info tentang lembaga pendidikan <span
                                class="text-success font-weight-bold">{{ $schoolUnit->name }}</span> Anak Sholeh? Ayok ikuti <span
                                class="font-weight-bold text-primary">Social Media</span> kami.</h5>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->

            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8 pb-md-4">
                    <ul class="p-0 mb-0 text-center">
                        <li class="mx-2 mt-3 list-inline-item"><a href="https://www.facebook.com/{{ $schoolUnit->fb }}"
                                class="btn btn-icon btn-pills btn-lg btn-blue" data-toggle="tooltip" data-placement="top"
                                title="Facebook"><i data-feather="facebook" class="icons"></i></a></li>
                        <li class="mx-2 mt-3 list-inline-item"><a href="https://www.instagram.com/{{ $schoolUnit->ig }}"
                                class="btn btn-icon btn-pills btn-lg btn-danger" data-toggle="tooltip" data-placement="top"
                                title="Instagram"><i data-feather="instagram" class="icons"></i></a></li>
                        <li class="mx-2 mt-3 list-inline-item"><a href="https://twitter.com/{{ $schoolUnit->tw }}"
                                class="btn btn-icon btn-pills btn-lg btn-info" data-toggle="tooltip" data-placement="top"
                                title="Twitter"><i data-feather="twitter" class="icons"></i></a></li>
                        <li class="mx-2 mt-3 list-inline-item"><a href="{{ $schoolUnit->yt }}"
                                class="btn btn-icon btn-pills btn-lg btn-danger" data-toggle="tooltip" data-placement="top"
                                title="Youtube"><i data-feather="youtube" class="icons"></i></a></li>
                        <li class="mx-2 mt-3 list-inline-item"><a href="https://wa.me/{{ $schoolUnit->wa }}"
                                class="btn btn-icon btn-pills btn-lg btn-success" data-toggle="tooltip" data-placement="top"
                                title="Whatsapp"><i class="mdi mdi-whatsapp icons"></i></a></li>
                    </ul>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>

    {{-- berita --}}
    <section class="section bg-light">
        <div class="container">
            <div class="pb-4 mb-4 row align-items-center">
                <div class="col-md-8">
                    <div class="text-center section-title text-md-left">
                        <h4 class="mb-4">Berita dan Artikel Terbaru</h4>
                        <p class="mb-0 text-muted para-desc">Berikut daftar berita dan artikel terbaru lembaga <span
                                class="text-primary font-weight-bold">{{ $schoolUnit->name }}</span>.</p>
                    </div>
                </div>
                <!--end col-->

                <div class="mt-4 col-md-4 mt-sm-0">
                    <div class="text-center text-md-right">
                        <a href="{{ route('blog', [$schoolUnit->slug]) }}" class="btn btn-soft-primary">Lihat Semua <i data-feather="arrow-right"
                                class="fea icon-sm"></i></a>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->

            <div class="row">
                @foreach ($theNews as $news)
                <div class="pt-2 mt-4 col-lg-4 col-md-6">
                    <div class="overflow-hidden border-0 rounded shadow card blog">
                        <div class="position-relative">
                            @if ($news->cover->count())
                            @foreach($news->cover as $key => $entry)
                            <img src="{{ $entry['article_thumbnail'] }}" class="card-img-top" alt="{{ $entry['name'] }}" title="{{ $entry['name'] }}">
                            @endforeach
                            @else
                            <img src="/assets/images/blog/04.jpg" class="card-img-top" alt="...">
                            @endif
                            <div class="overlay rounded-top bg-dark"></div>
                        </div>
                        <div class="card-body content">
                            <h5><a href="{{ route('articleDetail', [$schoolUnit->slug, $news->category->slug, $news->slug]) }}" class="card-title title text-dark">{{ Str::limit($news->title, 50) }}</a></h5>
                            <div class="mt-3 post-meta d-flex justify-content-between">
                                <ul class="mb-0 list-unstyled">
                                    {{-- <li class="mb-0 mr-2 list-inline-item"><a href="javascript:void(0)"
                                            class="text-muted like"><i class="mr-1 mdi mdi-heart-outline"></i>33</a></li> --}}
                                    <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i
                                                class="mr-1 mdi mdi-eye-outline"></i>{{ $news->visit ?? '0' }}</a></li>
                                </ul>
                                <a href="{{ route('articleDetail', [$schoolUnit->slug, $news->category->slug, $news->slug]) }}" class="text-muted readmore">Selengkapnya <i
                                        class="mdi mdi-chevron-right"></i></a>
                            </div>
                        </div>
                        <div class="author">
                            <small class="text-light user d-block"><i class="mdi mdi-account"></i> {{ $schoolUnit->name ?? '' }}</small>
                            <small class="text-light date"><i class="mdi mdi-calendar-check"></i> {{ Date::parse($news->published_at)->diffForHumans() }} &bull; {{ $news->category->name ?? '' }} </small>
                        </div>
                    </div>
                </div>
                <!--end col-->
                @endforeach
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->

    {{-- agenda --}}
    <!-- Schedule Start -->
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="text-center col-12">
                    <div class="pb-2 mb-4 section-title">
                        <h4 class="mb-4 title">Agenda Kegiatan</h4>
                        <p class="mx-auto mb-0 text-muted para-desc">Berikut daftar agenda kegiatan terbaru lembaga <span
                                class="text-primary font-weight-bold">{{ $schoolUnit->name }}</span>.</p>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->

            <div class="row">
                <div class="col-12">
                    <div class="row">
                        @foreach ($events as $event)
                        <div class="pt-2 mt-4 col-lg-6">
                            <div class="border rounded card event-schedule">
                                <div class="card-body">
                                    <div class="media">
                                        <ul class="mb-0 mr-3 text-center date text-primary list-unstyled">
                                            <li class="mb-2 day font-weight-bold">{{ Date::parse($event->from_date)->format('d') }}</li>
                                            <li class="month font-weight-bold">{{ strtoupper(Date::parse($event->from_date)->format('F')) }}</li>
                                        </ul>
                                        <div class="media-body content">
                                            <h4><a href="{{ route('eventDetail', [$schoolUnit->slug, $event->slug]) }}" class="text-dark title">{{ Str::limit($event->title, 37) }}</a></h4>
                                            <p class="text-muted location-time"><span class="text-dark h6">Lokasi:</span> {{ Str::limit($event->location, 40) }} <br> <span
                                                    class="text-dark h6">Selesai:</span> {{ $event->from_date != $event->to_date ? Date::parse($event->to_date)->format('l, d F Y') : 'Hari ini' }}</p>
                                            <a href="{{ route('eventDetail', [$schoolUnit->slug, $event->slug]) }}"
                                                class="btn btn-sm btn-outline-primary mouse-down">Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                        @endforeach
                    </div>
                    <!--end row-->
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->
    <!-- Schedule End -->

    {{-- galeri foto --}}
    <section class="section bg-light" style="background: url('/assets/images/digital/home-bg.png') center center;">
        <div class="container" id="portfolio">
            <div class="row">
                <div class="col-12">
                    <div class="pb-2 mb-4 section-title">
                        <h4 class="mb-4 title">Galeri Foto</h4>
                        <p class="mb-0 text-muted para-desc">Berikut galeri foto kegiatan belajar mengajar, ekstrakurikuler,
                            foto fasilitas yang diadakan oleh <span
                                class="text-primary font-weight-bold">{{ $schoolUnit->name }}</span></p>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->

            <div class="row">
                @foreach ($photos as $photo)
                <div class="pt-2 mt-4 col-lg-4 col-md-6 col-12">
                    <div class="overflow-hidden border-0 rounded card work-container work-modern position-relative d-block">
                        <div class="p-0 card-body">
                            @if ($photo->photo->count())
                            @foreach($photo->photo as $key => $entry)
                            @if ($loop->first)
                            <img src="{{ $entry['photo'] }}" class="img-fluid" alt="{{ $entry['name'] }}" title="{{ $entry['name'] }}">
                            @endif
                            @endforeach
                            @else
                            <img src="/assets/images/personal/1.jpg" class="img-fluid" alt="...">
                            @endif
                            <div class="overlay-work bg-dark"></div>
                            <div class="content">
                                <h5 class="mb-0"><a href="{{ route('galleryPhotoDetail', [$schoolUnit->slug, $photo->slug]) }}" class="text-white title">{{ Str::limit($photo->title, 50) }}</a>
                                </h5>
                            </div>
                            <div class="text-center icons">
                                @if ($photo->photo->count())
                                    @foreach ($photo->photo as $key => $entry)
                                        @if ($loop->first)
                                            <a href="{{ $entry['url'] }}"
                                                class="bg-white text-primary work-icon d-inline-block rounded-pill mfp-image"><i
                                                    data-feather="camera" class="fea icon-sm"></i></a>
                                        @endif
                                    @endforeach
                                @else
                                    <a href="/assets/images/personal/1.jpg"
                                        class="bg-white text-primary work-icon d-inline-block rounded-pill mfp-image"><i
                                            data-feather="camera" class="fea icon-sm"></i></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--end col-->
                @endforeach

                <div class="pt-2 mt-4 col-12">
                    <a href="{{ route('galleryPhotos', [$schoolUnit->slug]) }}" class="btn btn-soft-primary">Lihat Semua <i
                            class="mdi mdi-chevron-right"></i></a>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->

        {{-- galeri video --}}
        <div class="container mt-100 mt-60" id="portfolio">
            <div class="row">
                <div class="col-12">
                    <div class="pb-2 mb-4 section-title">
                        <h4 class="mb-4 title">Galeri Video</h4>
                        <p class="mb-0 text-muted para-desc">Berikut galeri video kegiatan belajar mengajar,
                            ekstrakurikuler, foto fasilitas yang diadakan oleh <span
                                class="text-primary font-weight-bold">{{ $schoolUnit->name }}</span></p>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->


            <div class="row projects-wrapper">
                @foreach ($videos as $video)
                <div class="pt-2 mt-4 col-lg-4 col-md-6 col-12">
                    <div class="overflow-hidden border-0 card work-container work-grid position-relative d-block rounded-0">
                        <div class="p-0 card-body">
                            <iframe width="100%" height="195px" src="https://www.youtube.com/embed/{{ substr($video->link, -11) }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                            <div class="p-3 bg-white content">
                                <h5 class="mb-0"><a href="{{ route('galleryVideoDetail', [$schoolUnit->slug, $video->slug]) }}" class="text-dark title">{{ Str::limit($video->title, 50) }}</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end col-->
                @endforeach

                <div class="pt-2 mt-2 col-12">
                    <a href="{{ route('galleryVideos', $schoolUnit->slug) }}" class="btn btn-soft-primary">Lihat Semua <i
                            class="mdi mdi-chevron-right"></i></a>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <div class="position-relative">
        <div class="overflow-hidden text-white shape">
            <svg viewBox="0 0 2880 250" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M720 125L2160 0H2880V250H0V125H720Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>

    <section class="section">
        {{-- poster --}}
        <div class="container">
            <div class="pb-2 mb-4 row align-items-end">
                <div class="col-md-8">
                    <div class="text-center section-title text-md-left">
                        <h4 class="mb-4 title">Poster</h4>
                        <p class="mb-0 text-muted para-desc">Berikut daftar poster <span
                                class="text-primary font-weight-bold">{{ $schoolUnit->name }}</span></p>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
        <div class="container-fluid">
            <div class="row">
                <div class="pt-2 mt-4 col-md-12">
                    <div id="six-slide" class="owl-carousel owl-theme">
                        @foreach ($posters as $poster)
                        <div class="mx-3 overflow-hidden rounded-md popular-tour position-relative">
                            @if ($poster->image->count())
                            @foreach($poster->image as $key => $entry)
                            <img src="{{ $entry['poster_thumbnail'] }}" class="img-fluid" alt="{{ $entry['name'] }}" title="{{ $entry['name'] }}">
                            @endforeach
                            @else
                            <img src="/assets/images/travel/dubai.jpg" class="img-fluid" alt="">
                            @endif
                            <div class="overlay-work bg-dark"></div>
                            <div class="content">
                                <a href="{{ route('posterDetail', [$schoolUnit->slug, $poster->slug]) }}" class="text-white title h4 title-dark">{{ Str::limit($poster->title, 50) }}</a>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
    </section>
@endsection

@section('scripts')
@endsection
