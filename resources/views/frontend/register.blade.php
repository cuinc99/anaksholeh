@extends('frontend.layouts.base')

@section('title', 'Pendaftaran')

@section('content')

<section class="bg-half-170 agency-wrapper d-table w-100" id="home">
    <div class="container">
        <div class="row align-items-center">
            <div class="order-1 col-lg-7 col-md-7 order-md-2">
                <div class="mt-4 title-heading ml-lg-5">
                    <h1 class="mb-3 heading">Pendaftaran <span class="text-primary">Online</span> <br> YPIT Ibnu Abbas Mataram</h1>

                </div>
            </div><!--end col-->

            <div class="order-2 pt-2 mt-4 col-lg-5 col-md-5 order-md-1 mt-sm-0 pt-sm-0">
                <img src="/assets/images/illustrator/working_with_computer.svg" class="img-fluid" alt="">
            </div>
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- Hero End -->
    <section class="section">
        <div class="container mt-lg-3">
            <div class="row">
                <div class="col-12 d-lg-block d-none">
                    <div class="p-4 rounded shadow sidebar sticky-bar">

                        <div class="widget">
                            <div class="row">

                                @foreach ($schoolUnits as $schoolUnit)
                                <div class="pt-2 mt-4 col-6">
                                    <a href="{{ route('registerForm', $schoolUnit->slug) }}"
                                        class="py-3 text-center rounded shadow accounts d-block">
                                        <span class="pro-icons h3 text-muted"><i class="uil uil-edit"></i></span>
                                        <h6 class="my-0 title text-dark h6">Daftar {{ $schoolUnit->name ?? '-' }}</h6>
                                    </a>
                                </div>
                                @endforeach

                                <!--end col-->
                            </div>
                            <!--end row-->
                        </div>


                    </div>
                </div>
                <!--end col-->


            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->
    <!-- Profile End -->
    <!-- Hero End -->
    @include('frontend.layouts.footer')
@endsection
