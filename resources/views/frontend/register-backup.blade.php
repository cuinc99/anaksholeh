@extends('frontend.layouts.base')

@section('title', 'Pendaftaran')

@section('content')

    <section class="section">
        <div class="container">
            <h1>{{ $tahunAjaran->name }}</h1>
            <h4>{{ $tahunAjaran->description }}</h4>
            <div class=" justify-content-center login_page" id="counter">
                <form action="{{ route('registerStore') }}" method="POST" class="mt-4 login-form row" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="school_unit_id" value="{{ $schoolUnit->id }}">
                    <input type="hidden" name="tahun_ajaran_id" value="{{ $tahunAjaran->id }}">
                    <div class="col-md-6">
                        <div class="border-0 rounded shadow card">
                            <div class="card-body">
                                <h4 class="text-center card-title">Data Siswa</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Nama Lengkap <span class="text-danger">*</span></label>
                                            <i data-feather="user" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Nama Lengkap" name="nama_lengkap"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Nama Panggilan <span class="text-danger">*</span></label>
                                            <i data-feather="user" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Nama Pangilan" name="nama_pangilan"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Jenis Kelamin <span class="text-danger">*</span></label>
                                            <i data-feather="airplay" class="fea icon-sm icons"></i>
                                            <select name="jenis_kelamin" class="pl-5 form-control" required>
                                                <option value="">=== Pilih Jenis Kelamin ===</option>
                                                @foreach (\App\Models\Pendaftaran::JENIS_KELAMIN_RADIO as $jk)
                                                    <option value="{{ $jk }}">{{ $jk }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>NISN <span class="text-danger">*</span></label>
                                            <i data-feather="hash" class="fea icon-sm icons"></i>
                                            <input type="number" class="pl-5 form-control" placeholder="NISN" name="nisn"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Golongan Darah <span class="text-danger">*</span></label>
                                            <i data-feather="link" class="fea icon-sm icons"></i>
                                            <select name="golongan_darah" class="pl-5 form-control" required>
                                                <option value="">=== Pilih Golongan Darah ===</option>
                                                @foreach (\App\Models\Pendaftaran::GOLONGAN_DARAH_RADIO as $gol)
                                                    <option value="{{ $gol }}">{{ $gol }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Tempat Lahir <span class="text-danger">*</span></label>
                                            <i data-feather="home" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Tempat Lahir" name="tempat_lahir"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Tanggal Lahir <span class="text-danger">*</span></label>
                                            <i data-feather="calendar" class="fea icon-sm icons"></i>
                                            <input type="date" class="pl-5 form-control" placeholder="Tanggal Lahir" name="tanggal_lahir"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>NIK (Nomor Induk Keluarga) <span class="text-danger">*</span></label>
                                            <i data-feather="hash" class="fea icon-sm icons"></i>
                                            <input type="number" class="pl-5 form-control" placeholder="Nomor Induk Keluarga" name="nik"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Kewarganegaraan <span class="text-danger">*</span></label>
                                            <i data-feather="globe" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Kewarganegaraan" name="kewarganegaraan"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Anak Ke <span class="text-danger">*</span></label>
                                            <i data-feather="user" class="fea icon-sm icons"></i>
                                            <input type="number" class="pl-5 form-control" placeholder="Anak Ke" name="anak_ke"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Jumlah Saudara <span class="text-danger">*</span></label>
                                            <i data-feather="users" class="fea icon-sm icons"></i>
                                            <input type="number" class="pl-5 form-control" placeholder="Jumlah Saudara" name="jumlah_saudara"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Bahasa sehari-hari dirumah <span class="text-danger">*</span></label>
                                            <i data-feather="globe" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Bahasa sehari-hari dirumah" name="bahasa"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Asal Sekolah <span class="text-danger">*</span></label>
                                            <i data-feather="home" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Asal Sekolah" name="asal_sekolah"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Alamat Lengkap <span class="text-danger">*</span></label>
                                            <i data-feather="map-pin" class="fea icon-sm icons"></i>
                                            <textarea name="alamat_siswa" class="pl-5 form-control" placeholder="Alamat Lengkap" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Pilih Kelas<span class="text-danger">*</span></label>
                                            <i data-feather="share-2" class="fea icon-sm icons"></i>
                                            <select name="pilihan_kelas" class="pl-5 form-control" required>
                                                <option value="">=== Pilih Kelas ===</option>
                                                @foreach (\App\Models\Pendaftaran::PILIHAN_KELAS_SELECT as $kelas)
                                                    <option value="{{ $kelas }}">{{ $kelas }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-4 border-0 rounded shadow card">
                            <div class="card-body">
                                <h4 class="text-center card-title">Informasi Tambahan</h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Jarak Rumah ke Sekolah (km) <span class="text-danger">*</span></label>
                                            <i data-feather="trending-up" class="fea icon-sm icons"></i>
                                            <input type="number" class="pl-5 form-control" placeholder="Jarak Rumah ke Sekolah (km)" name="jarak_rumah"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Waktu Tempuh ke Sekolah (menit) <span class="text-danger">*</span></label>
                                            <i data-feather="clock" class="fea icon-sm icons"></i>
                                            <input type="number" class="pl-5 form-control" placeholder="Waktu Tempuh ke Sekolah (menit)" name="waktu_tempuh"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Transportasi yang Digunakan <span class="text-danger">*</span></label>
                                            <i data-feather="truck" class="fea icon-sm icons"></i>
                                            <select name="transportasi" class="pl-5 form-control" required>
                                                <option value="">=== Pilih Transportasi ===</option>
                                                @foreach (\App\Models\Pendaftaran::TRANSPORTASI_RADIO as $transportasi)
                                                    <option value="{{ $transportasi }}">{{ $transportasi }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="col-md-6">
                        <div class="border-0 rounded shadow card">
                            <div class="card-body">
                                <h4 class="text-center card-title">Data Orang Tua</h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Data Ayah</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Nama Lengkap <span class="text-danger">*</span></label>
                                            <i data-feather="user" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Nama Lengkap" name="nama_ayah"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Tahun Lahir<span class="text-danger">*</span></label>
                                            <i data-feather="calendar" class="fea icon-sm icons"></i>
                                            <input type="number" class="pl-5 form-control" placeholder="Tahun Lahir" name="tahun_lahir_ayah"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Pendidikan Terakhir <span class="text-danger">*</span></label>
                                            <i data-feather="award" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Pendidikan Terakhir" name="pendidikan_ayah"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Telepon (Nomor HP) <span class="text-danger">*</span></label>
                                            <i data-feather="phone" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Telepon (Nomor HP)" name="telepon_ayah"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Pekerjaan <span class="text-danger">*</span></label>
                                            <i data-feather="trello" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Pekerjaan" name="pekerjaan_ayah"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Penghasilan <span class="text-danger">*</span></label>
                                            <i data-feather="dollar-sign" class="fea icon-sm icons"></i>
                                            <select name="penghasilan_ayah" class="pl-5 form-control" required>
                                                <option value="">=== Pilih Penghasilan ===</option>
                                                @foreach (\App\Models\Pendaftaran::PENGHASILAN_AYAH_RADIO as $penghasilan)
                                                    <option value="{{ $penghasilan }}">{{ $penghasilan }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Alamat <span class="text-danger">*</span></label>
                                            <i data-feather="map-pin" class="fea icon-sm icons"></i>
                                            <textarea name="alamat_ayah" class="pl-5 form-control" placeholder="Alamat" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h4>Data Ibu</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Nama Lengkap <span class="text-danger">*</span></label>
                                            <i data-feather="user" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Nama Lengkap" name="nama_ibu"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Tahun Lahir<span class="text-danger">*</span></label>
                                            <i data-feather="calendar" class="fea icon-sm icons"></i>
                                            <input type="number" class="pl-5 form-control" placeholder="Tahun Lahir" name="tahun_lahir_ibu"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Pendidikan Terakhir <span class="text-danger">*</span></label>
                                            <i data-feather="award" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Pendidikan Terakhir" name="pendidikan_ibu"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Telepon (Nomor HP) <span class="text-danger">*</span></label>
                                            <i data-feather="phone" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Telepon (Nomor HP)" name="telepon_ibu"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Pekerjaan <span class="text-danger">*</span></label>
                                            <i data-feather="trello" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Pekerjaan" name="pekerjaan_ibu"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Penghasilan <span class="text-danger">*</span></label>
                                            <i data-feather="dollar-sign" class="fea icon-sm icons"></i>
                                            <select name="penghasilan_ibu" class="pl-5 form-control" required>
                                                <option value="">=== Pilih Penghasilan ===</option>
                                                @foreach (\App\Models\Pendaftaran::PENGHASILAN_IBU_RADIO as $penghasilan)
                                                    <option value="{{ $penghasilan }}">{{ $penghasilan }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Alamat <span class="text-danger">*</span></label>
                                            <i data-feather="map-pin" class="fea icon-sm icons"></i>
                                            <textarea name="alamat_ibu" class="pl-5 form-control" placeholder="Alamat" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h4>Data Wali (Opsional)</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Nama Lengkap <span class="text-danger">*</span></label>
                                            <i data-feather="user" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Nama Lengkap" name="nama_wali"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Pekerjaan<span class="text-danger">*</span></label>
                                            <i data-feather="trello" class="fea icon-sm icons"></i>
                                            <input type="text" class="pl-5 form-control" placeholder="Pekerjaan" name="pekerjaan_wali"
                                                required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-4 border-0 rounded shadow card">
                            <div class="card-body">
                                <h4 class="text-center card-title">Upload File</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Biaya Administrasi <span class="text-danger">*</span></label>
                                            <i data-feather="file" class="fea icon-sm icons"></i>
                                            <input type="file" class="pl-5 form-control" name="biaya_administrasi"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Kartu Keluarga <span class="text-danger">*</span></label>
                                            <i data-feather="file" class="fea icon-sm icons"></i>
                                            <input type="file" class="pl-5 form-control" name="kartu_keluarga"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Akta Kelahiran <span class="text-danger">*</span></label>
                                            <i data-feather="file" class="fea icon-sm icons"></i>
                                            <input type="file" class="pl-5 form-control" name="akta_kelahiran"
                                                required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Pas Foto <span class="text-danger">*</span></label>
                                            <i data-feather="file" class="fea icon-sm icons"></i>
                                            <input type="file" class="pl-5 form-control" name="pas_foto"
                                                required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4 col-md-12">
                        <button type="submit" class="btn btn-primary btn-block">Daftar</button>
                    </div>

                </form>
            </div>
        </div>
    </section>

    <!-- Hero End -->
    @include('frontend.layouts.footer')
@endsection
