@extends('frontend.layouts.base')

@section('title', 'Hubungi Kami')

@section('content')
<section class="section">
    <div class="container">
        <div class="row" id="counter">
            <div class="col-12">
                @if (session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"> × </span>
                    </button>
                </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Error!</strong> <br>
                    @foreach ($errors->all() as $error)
                    <ul>
                        <li>{{ $error }}</li>
                    </ul>
                    @endforeach
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"> × </span>
                    </button>
                </div>
                @endif
            </div>
            <div class="col-lg-4 col-md-6  mt-4 pt-2">
                <div class="card shadow rounded border-0">
                    <div class="card-body py-5">
                        <h4 class="card-title">Hubungi Kami !</h4>
                        <div class="custom-form mt-4">
                            <div id="message"></div>
                            <form method="post" action="{{ route('contactUs.store') }}" id="contact-form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Nama Lengkap <span class="text-danger">*</span></label>
                                            <i data-feather="user" class="fea icon-sm icons"></i>
                                            <input name="name" id="name" type="text" class="form-control pl-5" placeholder="Nama Lengkap :">
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-md-6">
                                        <div class="form-group position-relative">
                                            <label>Nomor HP <span class="text-danger">*</span></label>
                                            <i data-feather="mail" class="fea icon-sm icons"></i>
                                            <input name="number" id="number" type="number" class="form-control pl-5" placeholder="Nomor HP :">
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Lembaga Pendidikan</label>
                                            <i data-feather="book" class="fea icon-sm icons"></i>
                                            <select class="form-control custom-select pl-5" name="school_unit_id">
                                                <option selected="">Pilih Lembaga Pendidikan</option>
                                                @foreach ($schoolUnits as $id => $schoolUnit)
                                                <option value="{{ $id }}">{{ $schoolUnit }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                            <label>Pesan</label>
                                            <i data-feather="message-circle" class="fea icon-sm icons"></i>
                                            <textarea name="message" id="message" rows="4" class="form-control pl-5" placeholder="Pesan :"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group position-relative">
                                        <div class="row">
                                            <div class="col-6">
                                                {!! captcha_img('flat') !!}
                                            </div>
                                            <div class="col-6">
                                                <input name="captcha" id="captcha" type="text" class="form-control" placeholder="Captcha :">
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div><!--end row-->
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <input type="submit" id="submit" class="submitBnt btn btn-primary btn-block" value="Send Message">
                                        <div id="simple-msg"></div>
                                    </div><!--end col-->
                                </div><!--end row-->
                            </form><!--end form-->
                        </div><!--end custom-form-->
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-8 col-md-6 pl-md-3 pr-md-3 mt-4 pt-2">
                <div class="card map map-height-two rounded map-gray border-0">
                    <div class="card-body p-0">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30982.441573055934!2d116.07734262943272!3d-8.62342726557704!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dcdbf03e3a2b35b%3A0x52d89f9894a432ae!2sSMPIT%20%26%20SDIT%20Anak%20Sholeh%202%20Mataram!5e1!3m2!1sid!2sid!4v1622944861565!5m2!1sid!2sid" style="border:0" class="rounded" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>
@include('frontend.layouts.footer')
@endsection
