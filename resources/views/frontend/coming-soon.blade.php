<!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Coming Soon</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <!-- favicon -->
        <link rel="shortcut icon" href="/img/favicon.png">
        <!-- Bootstrap -->
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="/assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v2.1.9/css/unicons.css">
        <!-- Main Css -->
        <link href="/assets/css/style.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <link href="/assets/css/colors/default.css" rel="stylesheet" id="color-opt">

    </head>

    <body>
        <!-- Loader -->
        <!-- <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div> -->
        <!-- Loader -->

        {{-- <div class="rounded back-to-home d-none d-sm-block">
            <a href="/" class="btn btn-icon btn-soft-primary"><i data-feather="home" class="icons"></i></a>
        </div> --}}

        <!-- COMING SOON PAGE -->
        <section class="bg-home d-flex align-items-center" data-jarallax='{"speed": 0.5}' style="background-image: url('https://wallpaperaccess.com/full/3656621.png');">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="text-center col-lg-8 col-md-12">
                        <a href="javascript:void(0)" class="logo h5"><img src="/img/logo-anaksholeh3.png" height="100" alt=""></a>
                        <div class="mt-2 mb-4 text-white text-uppercase title-dark coming-soon">Pendaftaran belum dibuka</div>
                        {{-- <p class="mx-auto text-light para-desc para-dark">Start working with <span class="text-primary font-weight-bold">Landrick</span> that can provide everything you need to generate awareness, drive traffic, connect.</p> --}}
                    </div>
                </div>

                <div class="row">
                    <div class="text-center col-md-12">
                        <div id="countdown"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="text-center col-md-12">
                        <a href="/" class="mt-4 btn btn-primary"><i class="mdi mdi-backup-restore"></i> Kembali ke Beranda</a>
                    </div>
                </div>
            </div> <!-- end container -->
        </section><!--section end-->
        <!-- COMING SOON PAGE -->

        <!-- javascript -->
        <script src="/assets/js/jquery-3.5.1.min.js"></script>
        <script src="/assets/js/bootstrap.bundle.min.js"></script>
        <script src="/assets/js/jquery.easing.min.js"></script>
        <script src="/assets/js/scrollspy.min.js"></script>
        <!-- Comingsoon -->
        <script src="/assets/js/jquery.countdown.min.js"></script>
        <script src="/assets/js/countdown.init.js"></script>
        <!-- Icons -->
        <script src="/assets/js/feather.min.js"></script>
        <script src="https://unicons.iconscout.com/release/v2.1.9/script/monochrome/bundle.js"></script>
        <!-- Main Js -->
        <script src="/assets/js/app.js"></script>
    </body>
</html>
