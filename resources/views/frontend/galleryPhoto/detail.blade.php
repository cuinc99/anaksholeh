@extends('frontend.layouts.base')

@section('meta')
@if ($photo->photo->count())
@foreach($photo->photo as $key => $entry)
@if ($loop->first)
@php
    $photoImage = $entry['photo'];
@endphp
@endif
@endforeach
@else
@php
    $photoImage = '/assets/images/1.jpg'
@endphp
@endif

<meta property="og:title" content="{{ $photo->title }}">
<meta property="og:image" content="{{ $photoImage }}">
<meta property="og:description" content="{!! Str::limit(strip_tags($photo->desc), 100) !!}">
<meta property="og:url" content="{{ route('galleryPhotoDetail', [$photo->schoolUnit->slug, $photo->slug]) }}">
@endsection

@section('title', $photo->title)

@section('content')

<!-- Hero Start -->
<section class="bg-half d-table w-100" style="background: url({{ $photoImage }}) center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="text-center col-lg-12">
                <div class="page-next-level">
                    <h4 class="text-white title title-dark"> {{ $photo->title ?? '' }} </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="mb-0 bg-white rounded shadow breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('schoolUnit', $photo->schoolUnit->slug) }}">{{ $photo->schoolUnit->name }}</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('galleryPhotos', [$photo->schoolUnit->slug]) }}">Galeri Foto</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<div class="position-relative">
    <div class="overflow-hidden text-white shape">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->

<section class="section">
    <div class="container">

        <div class="pt-2 mt-4 row projects-wrapper">
            @if ($photo->photo->count())
                @foreach($photo->photo as $key => $entry)
                <div class="px-0 col-lg-3 col-md-6 col-12">
                    <div class="overflow-hidden border-0 card work-container work-modern position-relative d-block rounded-0">
                        <div class="p-0 card-body">
                            <img src="{{ $entry['url'] }}" class="img-fluid" alt="work-image">
                            <div class="overlay-work bg-dark"></div>

                            <div class="text-center icons">
                                <a href="{{ $entry['url'] }}" class="bg-white text-primary work-icon d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm"></i></a>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                @endforeach
            @endif
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- End -->
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-60b24a8c6b265e03"></script>

@endsection
