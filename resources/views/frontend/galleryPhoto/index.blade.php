@extends('frontend.layouts.base')

@section('title', 'Galeri Foto')

@section('content')
    <!-- Hero Start -->
    <section class="bg-half bg-light d-table w-100"
        style="background: url('/img/1043102220.jpg'); background-repeat: repeat; background-size: 10%;">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="text-center col-lg-12">
                    <div class="page-next-level">
                        <h4 class="text-white title title-dark">Galeri Foto {{ $schoolUnit->name }} </h4>

                        <div class="pt-2 mt-4 subcribe-form">
                            <form method="GET" action="{{ url()->current() }}">
                                <div class="mb-0 form-group">
                                    <input type="text" id="help" name="keyword"
                                        value="{{ isset($_GET['keyword']) ? $_GET['keyword'] : '' }}"
                                        class="bg-white border shadow rounded-pill" required
                                        placeholder="Ketikan judul foto disni...">
                                    <button type="submit" class="btn btn-pills btn-primary">Cari</button>
                                </div>
                            </form>
                        </div>

                        <div class="page-next">
                            <nav aria-label="breadcrumb" class="d-inline-block">
                                <ul class="mb-0 bg-white rounded shadow breadcrumb">
                                    <li class="breadcrumb-item"><a
                                            href="{{ route('schoolUnit', $schoolUnit->slug) }}">{{ $schoolUnit->name }}</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Galeri Foto</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->
    <!-- Hero End -->

    <section class="section">
        <div class="container">
            <div class="row" id="counter">
                @foreach ($galleryPhotos as $galleryPhoto)
                    <div class="pt-2 mt-4 col-lg-4 col-md-6 col-12">
                        <div
                            class="overflow-hidden border-0 rounded card work-container work-modern position-relative d-block">
                            <div class="p-0 card-body">
                                @if ($galleryPhoto->photo->count())
                                    @foreach ($galleryPhoto->photo as $key => $entry)
                                        @if ($loop->first)
                                            <img src="{{ $entry['photo'] }}" class="img-fluid" alt="{{ $entry['name'] }}"
                                                title="{{ $entry['name'] }}">
                                        @endif
                                    @endforeach
                                @else
                                    <img src="/assets/images/personal/1.jpg" class="img-fluid" alt="...">
                                @endif
                                <div class="overlay-work bg-dark"></div>
                                <div class="content">
                                    <h5 class="mb-0"><a
                                            href="{{ route('galleryPhotoDetail', [$schoolUnit->slug, $galleryPhoto->slug]) }}"
                                            class="text-white title">{{ Str::limit($galleryPhoto->title, 50) }}</a>
                                    </h5>
                                </div>
                                <div class="text-center icons">
                                    @if ($galleryPhoto->photo->count())
                                        @foreach ($galleryPhoto->photo as $key => $entry)
                                            @if ($loop->first)
                                                <a href="{{ $entry['url'] }}"
                                                    class="bg-white text-primary work-icon d-inline-block rounded-pill mfp-image"><i
                                                        data-feather="camera" class="fea icon-sm"></i></a>
                                            @endif
                                        @endforeach
                                    @else
                                        <a href="/assets/images/personal/1.jpg"
                                            class="bg-white text-primary work-icon d-inline-block rounded-pill mfp-image"><i
                                                data-feather="camera" class="fea icon-sm"></i></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <!-- PAGINATION START -->
                {{ $galleryPhotos->withQueryString()->links() }}
                <!-- PAGINATION END -->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->
    <!-- End -->
@endsection
