@extends('frontend.layouts.base')

@section('title', 'Pendaftaran')

@section('content')

<section class="section">
    <div class="container">
        <h1>{{ $tahunAjaran->name }}</h1>
        <h4>{{ $tahunAjaran->description }}</h4>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong> {{ session()->get('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"> × </span>
            </button>
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Ups Terjadi Kesalahan!</strong>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"> × </span>
            </button>
        </div>
        @endif
        <div class=" justify-content-center login_page" id="counter">
            <form action="{{ route('registerStore') }}" method="POST" class="mt-4 login-form row"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="school_unit_id" value="{{ $schoolUnit->id }}">
                <input type="hidden" name="tahun_ajaran_id" value="{{ $tahunAjaran->id }}">
                <div class="col-md-6">
                    <div class="border-0 rounded shadow card">
                        <div class="card-body">
                            <h4 class="text-center card-title">Data Siswa</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Nama Lengkap <span class="text-danger">*</span></label>
                                        <i data-feather="user" class="fea icon-sm icons"></i>
                                        <input type="text" class="pl-5 form-control" placeholder="Nama Lengkap"
                                            name="nama_lengkap" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Jenis Kelamin <span class="text-danger">*</span></label>
                                        <i data-feather="airplay" class="fea icon-sm icons"></i>
                                        <select name="jenis_kelamin" class="pl-5 form-control" required>
                                            <option value="">=== Pilih Jenis Kelamin ===</option>
                                            @foreach (\App\Models\Pendaftaran::JENIS_KELAMIN_RADIO as $jk)
                                            <option value="{{ $jk }}">{{ $jk }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>NISN (Boleh tidak diisi)</label>
                                        <i data-feather="hash" class="fea icon-sm icons"></i>
                                        <input type="number" class="pl-5 form-control" placeholder="NISN" name="nisn">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Tempat Lahir <span class="text-danger">*</span></label>
                                        <i data-feather="home" class="fea icon-sm icons"></i>
                                        <input type="text" class="pl-5 form-control" placeholder="Tempat Lahir"
                                            name="tempat_lahir" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Tanggal Lahir <span class="text-danger">*</span></label>
                                        <i data-feather="calendar" class="fea icon-sm icons"></i>
                                        <input type="date" class="pl-5 form-control" placeholder="Tanggal Lahir"
                                            name="tanggal_lahir" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Asal Sekolah <span class="text-danger">*</span></label>
                                        <i data-feather="home" class="fea icon-sm icons"></i>
                                        <input type="text" class="pl-5 form-control" placeholder="Asal Sekolah"
                                            name="asal_sekolah" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Alamat Lengkap <span class="text-danger">*</span></label>
                                        <i data-feather="map-pin" class="fea icon-sm icons"></i>
                                        <textarea name="alamat_siswa" class="pl-5 form-control"
                                            placeholder="Alamat Lengkap" required></textarea>
                                    </div>
                                </div>
                                @if (in_array($schoolUnit->id, [1,2]))
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Pilih Kelas <span class="text-danger">*</span></label>
                                        <i data-feather="airplay" class="fea icon-sm icons"></i>
                                        <select name="kelas" class="pl-5 form-control" required>
                                            <option value="">=== Pilih Kelas ===</option>
                                            @foreach (\App\Models\Pendaftaran::KELAS_RADIO as $kelas)
                                            <option value="{{ $kelas }}">{{ $kelas }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if (in_array($schoolUnit->id, [2]))
                    <div class="mt-4 border-0 rounded shadow card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Jalur Masuk yang Pilih <span class="text-danger">*</span></label>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="radio" name="jalur_masuk" value="JALUR TAHFIDZ" class="form-check-input">
                                                JALUR TAHFIDZ (Jumlah juz ditulis diketerangan)
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="radio" name="jalur_masuk" value="JALUR PRESTASI" class="form-check-input">
                                                JALUR PRESTASI (Prestasi yang diraih ditulis diketerangan)
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="radio" name="jalur_masuk" value="JALUR REGULER" class="form-check-input">
                                                JALUR REGULER (Jalur masuk tanpa beasiswa)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Keterangan Jalur Masuk</label>
                                        <i data-feather="map-pin" class="fea icon-sm icons"></i>
                                        <textarea name="ket_jalur_masuk" class="pl-5 form-control" placeholder="Jawaban Anda"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="mt-4 border-0 rounded shadow card">
                        <div class="card-body">
                            <h4 class="text-center card-title">Upload File</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Bukti Pembayaran <span class="text-danger">*</span></label>
                                        <i data-feather="file" class="fea icon-sm icons"></i>
                                        <input type="file" class="pl-5 form-control" name="biaya_administrasi" accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps"
                                            required="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="border-0 rounded shadow card">
                        <div class="card-body">
                            <h4 class="text-center card-title">Data Orang Tua</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Data Ayah</h4>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Nama Lengkap <span class="text-danger">*</span></label>
                                        <i data-feather="user" class="fea icon-sm icons"></i>
                                        <input type="text" class="pl-5 form-control" placeholder="Nama Lengkap"
                                            name="nama_ayah" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Telepon (Nomor HP) <span class="text-danger">*</span></label>
                                        <i data-feather="phone" class="fea icon-sm icons"></i>
                                        <input type="number" class="pl-5 form-control" placeholder="Telepon (Nomor HP)"
                                            name="telepon_ayah" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Pekerjaan <span class="text-danger">*</span></label>
                                        <i data-feather="trello" class="fea icon-sm icons"></i>
                                        <input type="text" class="pl-5 form-control" placeholder="Pekerjaan"
                                            name="pekerjaan_ayah" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Alamat <span class="text-danger">*</span></label>
                                        <i data-feather="map-pin" class="fea icon-sm icons"></i>
                                        <textarea name="alamat_ayah" class="pl-5 form-control" placeholder="Alamat"
                                            required></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h4>Data Ibu</h4>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Nama Lengkap <span class="text-danger">*</span></label>
                                        <i data-feather="user" class="fea icon-sm icons"></i>
                                        <input type="text" class="pl-5 form-control" placeholder="Nama Lengkap"
                                            name="nama_ibu" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Telepon (Nomor HP) <span class="text-danger">*</span></label>
                                        <i data-feather="phone" class="fea icon-sm icons"></i>
                                        <input type="number" class="pl-5 form-control" placeholder="Telepon (Nomor HP)"
                                            name="telepon_ibu" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Pekerjaan <span class="text-danger">*</span></label>
                                        <i data-feather="trello" class="fea icon-sm icons"></i>
                                        <input type="text" class="pl-5 form-control" placeholder="Pekerjaan"
                                            name="pekerjaan_ibu" required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Alamat <span class="text-danger">*</span></label>
                                        <i data-feather="map-pin" class="fea icon-sm icons"></i>
                                        <textarea name="alamat_ibu" class="pl-5 form-control" placeholder="Alamat"
                                            required></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h4>Data Wali (Opsional)</h4>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        <label>Nama Lengkap</label>
                                        <i data-feather="user" class="fea icon-sm icons"></i>
                                        <input type="text" class="pl-5 form-control" placeholder="Nama Lengkap"
                                            name="nama_wali">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        <label>Pekerjaan</label>
                                        <i data-feather="trello" class="fea icon-sm icons"></i>
                                        <input type="text" class="pl-5 form-control" placeholder="Pekerjaan"
                                            name="pekerjaan_wali">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Telepon (Nomor HP)</label>
                                        <i data-feather="phone" class="fea icon-sm icons"></i>
                                        <input type="number" class="pl-5 form-control" placeholder="Telepon (Nomor HP)"
                                            name="telepon_wali">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="mt-4 col-md-12">
                    <button type="submit" class="btn btn-primary btn-block">Daftar</button>
                </div>

            </form>
        </div>
    </div>
</section>

<!-- Hero End -->
@include('frontend.layouts.footer')
@endsection
