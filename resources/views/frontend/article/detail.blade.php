@extends('frontend.layouts.base')

@section('meta')
@if ($article->cover->count())
@foreach($article->cover as $key => $entry)
@php
    $cover = $entry['share_article'];
@endphp
@endforeach
@else
@php
    $cover = '/assets/images/1.jpg'
@endphp
@endif
<meta property="og:title" content="{{ $article->title }}">
<meta property="og:image" content="{{ $cover }}">
<meta property="og:description" content="{!! Str::limit(strip_tags($article->content), 100) !!}">
<meta property="og:url" content="{{ route('articleDetail', [$schoolUnit->slug, $article->category->slug, $article->slug]) }}">
@endsection

@section('title', $article->title)

@section('content')
@if ($article->cover->count())
@foreach($article->cover as $key => $entry)
@php
    $coverA = $entry['url'];
@endphp
@endforeach
@else
@php
    $coverA = ''
@endphp
@endif
<!-- Hero Start -->
<section class="bg-half d-table w-100" style="background: url({{ $coverA }}) center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="text-center col-lg-12">
                <div class="page-next-level">
                    <h4 class="text-white title title-dark"> {{ $article->title ?? '' }} </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="mb-0 bg-white rounded shadow breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('schoolUnit', $article->schoolUnit->slug) }}">{{ $article->schoolUnit->name }}</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('articles', [$article->schoolUnit->slug, Request::segment(2)]) }}">{{ Request::segment(2) }}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<div class="position-relative">
    <div class="overflow-hidden text-white shape">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->

<section class="section">
    <div class="container">
        <div class="row justify-content-center" id="counter">
            <div class="col-lg-10">
                <div class="row">
                    {{-- <div class="col-md-2 d-none d-md-block">
                        <ul class="mb-0 text-center list-unstyled sticky-bar social-icon">
                            <li class="mb-3 h6">Share</li>
                            <li><a href="javascript:void(0)" class="rounded"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                            <li><a href="javascript:void(0)" class="rounded"><i data-feather="message-circle" class="fea icon-sm fea-social"></i></a></li>
                        </ul><!--end icon-->
                    </div> --}}

                    <div class="col-md-12 responsive">
                        @if ($coverA != '')
                        <img src="{{ $coverA }}" class="img-fluid rounded-top" alt="">

                        @endif
                        <ul class="list-unstyled d-flex justify-content-between">
                            <li class="mr-2 list-inline-item user"><a href="javascript:void(0)" class="text-muted"><i class="mdi mdi-account text-dark"></i> {{ $article->schoolUnit->name }}</a></li>
                            <li class="list-inline-item date text-muted"><i class="mdi mdi-calendar-check text-dark"></i> {{ Date::parse($article->published_at)->diffForHumans() }}</li>
                        </ul>

                        <div id="article">
                            {!! $article->content !!}
                        </div>

                        <div class="mt-3 post-meta">
                            <ul class="mb-0 list-unstyled">
                                <li class="mr-2 list-inline-item"><a href="javascript:void(0)" class="text-muted like"><i class="mr-1 mdi mdi-eye-outline"></i>{{ $article->visit ?? '0' }}x dibaca</a></li>
                            </ul>
                        </div>

                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-60b24a8c6b265e03"></script>

                        <h5 class="mt-4">Komentar :</h5>

                        <div id="disqus_thread"></div>
                        <script>
                            /**
                            *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                            *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
                            /*
                            var disqus_config = function () {
                            this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                            this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                            };
                            */
                            (function() { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');
                            s.src = 'https://anaksholeh.disqus.com/embed.js';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                            })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
    <script>
        $('#article img').addClass('img-fluid');
    </script>
@endsection
