@extends('frontend.layouts.base')

@section('title', 'Berita')

@section('content')
  <!-- Hero Start -->
  <section class="bg-half bg-light d-table w-100" style="background: url('https://www.technonatura.sch.id/sites/default/files/2020-08/important%20announcement.jpg') center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="text-center col-lg-12">
                <div class="page-next-level">
                    <h4 class="text-white title title-dark">Berita dan Artikel {{ $schoolUnit->name }} </h4>

                    <div class="pt-2 mt-4 subcribe-form">
                        <form method="GET" action="{{ url()->current() }}">
                            <div class="mb-0 form-group">
                                <input type="text" id="help" name="keyword" value="{{ isset($_GET['keyword']) ? $_GET['keyword'] : '' }}" class="bg-white border shadow rounded-pill" required placeholder="Ketikan judul berita dan artikel disni...">
                                <button type="submit" class="btn btn-pills btn-primary">Cari</button>
                            </div>
                        </form>
                    </div>

                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="mb-0 bg-white rounded shadow breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('schoolUnit', $schoolUnit->slug) }}">{{ $schoolUnit->name }}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Berita dan Artikel</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->


<section class="section">
    <div class="container">
        <div class="row">
            @foreach ($articles as $article)
            <div class="pb-2 mb-4 col-lg-6 col-12">
                <div class="overflow-hidden border-0 rounded shadow card blog">
                    <div class="row align-items-center no-gutters">
                        <div class="col-md-6">
                            @if ($article->cover->count())
                            @foreach($article->cover as $key => $entry)
                            <img src="{{ $entry['article_thumbnail'] }}" class="img-fluid" alt="{{ $entry['name'] }}" title="{{ $entry['name'] }}">
                            @endforeach
                            @else
                            <img src="/assets/images/work/14.jpg" class="img-fluid" alt="">
                            @endif
                            <div class="overlay bg-dark"></div>
                            <div class="author">
                                <small class="text-light user d-block"><i class="mdi mdi-account"></i> Admin {{ $schoolUnit->name ?? '' }}</small>
                                <small class="text-light date"><i class="mdi mdi-calendar-check"></i> {{ Date::parse($article->published_at)->diffForHumans() }}</small>
                            </div>
                        </div><!--end col-->

                        <div class="col-md-6">
                            <div class="card-body content">
                                <h5><a href="javascript:void(0)" class="card-title title text-dark">{{ Str::limit($article->title, 50) }}</a></h5>
                                <p class="mb-0 text-muted">{!! strip_tags(Str::limit($article->content, 50)) !!}</p>
                                <div class="mt-3 post-meta d-flex justify-content-between">
                                    <ul class="mb-0 list-unstyled">
                                        <li class="mb-0 mr-2 list-inline-item"><a href="javascript:void(0)" class="text-muted like"><i class="mr-1 mdi mdi-eye-outline"></i>{{ $article->visit ?? '0' }}</a></li>
                                        {{-- <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="mr-1 mdi mdi-comment-outline"></i>08</a></li> --}}
                                    </ul>
                                    <a href="{{ route('articleDetail', [$schoolUnit->slug, $article->category->slug, $article->slug]) }}" class="text-muted readmore">Selengkapnya <i class="mdi mdi-chevron-right"></i></a>
                                </div>
                            </div>
                        </div><!--end col-->
                    </div> <!--end row-->
                </div><!--end blog post-->
            </div><!--end col-->
            @endforeach

            {{ $articles->withQueryString()->links() }}
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section -->
<!--Blog Lists End-->
@endsection
