@extends('frontend.layouts.base')

@section('meta')
{{-- @if ($event->cover->count())
@foreach($event->cover as $key => $entry)
@php
    $cover = $entry['cover'];
@endphp
@endforeach
@endif --}}
<meta property="og:title" content="{{ $event->title }}">
{{-- <meta property="og:image" content="{{ $cover }}"> --}}
<meta property="og:description" content="{{ Date::parse($event->from_date)->format('d M, Y') }} s.d {{ Date::parse($event->to_date)->format('d M, Y') }} | Lokasi: {{ $event->location ?? '' }}">
<meta property="og:url" content="{{ route('eventDetail', [$event->schoolUnit->slug, $event->slug]) }}">
@endsection

@section('title', $event->title)

@section('content')
<!-- Hero Start -->
<section class="section d-table w-100" style="background: url('/assets/images/digital/home-bg.png') center center; height: 100vh;" id="home">
    <div class="container">
        <div class="mt-4 row align-items-center" id="counter">
            <div class="col-lg-6 col-md-6">
                <div class="mt-4 title-heading">
                    <h1 class="heading">{{ Str::limit($event->title, 50) ?? '' }}</h1>
                    <p class="text-dark">{!! $event->desc ?? '' !!}</p>

                    <div class="mt-3 media contact-detail align-items-center">
                        <div class="icon">
                            <i data-feather="calendar" class="mr-3 fea icon-m-md text-dark"></i>
                        </div>
                        <div class="media-body content">
                            <h4 class="mb-0 title font-weight-bold">Tanggal</h4>
                            <span class="text-muted">{{ Date::parse($event->from_date)->format('d M, Y') }} s.d {{ Date::parse($event->to_date)->format('d M, Y') }}</span>
                        </div>
                    </div>

                    <div class="mt-3 media contact-detail align-items-center">
                        <div class="icon">
                            <i data-feather="map-pin" class="mr-3 fea icon-m-md text-dark"></i>
                        </div>
                        <div class="media-body content">
                            <h4 class="mb-0 title font-weight-bold">Lokasi</h4>
                            <span class="text-muted">{{ $event->location ?? '' }}</span>
                        </div>
                    </div>
                </div>
            </div><!--end col-->

             <!-- Go to www.addthis.com/dashboard to customize your tools -->
             <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-60b24a8c6b265e03"></script>

            <div class="pt-2 mt-4 col-lg-6 col-md-6 mt-sm-0 pt-sm-0">
                <img src="/assets/images/digital/seo.gif" class="img-fluid" alt="">
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- Hero End -->
@endsection
