@extends('frontend.layouts.base')

@section('meta')
@if ($poster->image->count())
@foreach ($poster->image as $key => $entry)
@php
    $cover = $entry['poster'];
@endphp
@endforeach
@else
@php
    $cover = '/assets/images/landing/2.jpg';
@endphp
@endif
<meta property="og:title" content="{{ $poster->title }}">
<meta property="og:image" content="{{ $cover }}">
<meta property="og:description" content="{!! Str::limit(strip_tags($poster->desc), 100) !!}">
<meta property="og:url" content="{{ route('posterDetail', [$poster->schoolUnit->slug, $poster->slug]) }}">
@endsection

@section('title', $poster->title)

@section('content')
    <!-- Hero Start -->
    <section class="section pb-lg-0 pb-md-4 bg-white d-table w-100" style="height: 100vh;">
        {{-- <div class="bg-overlay bg-black" style="opacity: 0.8;"></div> --}}
        <div class="container" style="margin-top: 50px">
            <div class="row position-relative" style="z-index: 1;" id="counter">
                <div class="col-md-7 col-12 mt-4">
                    <div class="title-heading">
                        <h1 class="heading text-dark title-dark mb-4">{{ Str::limit($poster->title, 50) }}</h1>
                        <p class="para-desc text-dark-50">{{ $poster->desc }}</p>
                        <div class="watch-video mt-4 pt-2">
                            @if ($poster->image->count())
                                @foreach ($poster->image as $key => $entry)
                                    <a href="{{ $entry['poster'] }}" class="btn btn-primary mb-2" target="_blank"><i
                                            class="mdi mdi-download"></i> Unduh
                                        Poster</a>
                                @endforeach
                            @else
                                <a href="javascript:void(0)" disabled class="btn btn-primary mb-2"><i
                                        class="mdi mdi-download"></i> Unduh
                                    Poster</a>
                            @endif

                        </div>
                    </div>
                </div>
                <!--end col-->

                <div class="col-md-5 col-12 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <div class="shape-before">
                        <div class="carousel-cell">
                            @if ($poster->image->count())
                                @foreach ($poster->image as $key => $entry)
                                    <img src="{{ $entry['poster'] }}" class="img-fluid rounded-md"
                                        alt="{{ $entry['name'] }}" title="{{ $entry['name'] }}">
                                @endforeach
                            @else
                                <img src="/assets/images/landing/2.jpg" class="img-fluid rounded-md" alt="">
                            @endif
                        </div>
                        <img src="/assets/images/shapes/shape1.png" class="img-fluid shape-img mt-6" alt="">
                         <!-- Go to www.addthis.com/dashboard to customize your tools -->
                         <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-60b24a8c6b265e03"></script>

                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
@endsection
