<!-- javascript -->
<script src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/jquery.easing.min.js"></script>
<script src="/assets/js/scrollspy.min.js"></script>
<!-- Magnific -->
<script src="/assets/js/jquery.magnific-popup.min.js"></script>
<script src="/assets/js/isotope.js"></script>
<script src="/assets/js/portfolio.init.js"></script>
<!-- SLIDER -->
<script src="/assets/js/owl.carousel.min.js "></script>
<script src="/assets/js/owl.init.js "></script>
<!-- Animation -->
<script src="/assets/js/aos.js "></script>
<!-- Counter -->
<script src="/assets/js/counter.init.js "></script>
 <!-- Typed -->
 <script src="/assets/js/typed.js"></script>
 <script src="/assets/js/typed.init.js"></script>
<!-- Icons -->
<script src="/assets/js/feather.min.js"></script>
<script src="https://unicons.iconscout.com/release/v2.1.9/script/monochrome/bundle.js"></script>
<!-- Main Js -->
<script src="/assets/js/app.js"></script>

<!-- Animation Js -->
<script>
    AOS.init({
        easing: 'ease-in-out-sine',
        duration: 1000
    });
</script>
<script src="https://cdn.plyr.io/3.6.8/plyr.js"></script>

@yield('scripts')
