

<!-- Navbar STart -->
<header id="topnav" class="sticky bg-white defaultscroll">
    <div class="container">
        <!-- Logo container-->
        <div>
            <a class="logo" href="/">
                <img src="/img/logo-anaksholeh.png" height="47" alt="logo-anak-sholeh">
            </a>
        </div>
        <!--end login button-->
        <!-- End Logo container-->
        <div class="menu-extras">
            <div class="menu-item">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </div>
        </div>

        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                <li><a href="/">Beranda</a></li>
                <li class="has-submenu">
                    <a href="javascript:void(0)">Yayasan</a><span class="menu-arrow"></span>
                    <ul class="submenu">
                        <li><a href="{{ route('pageDetail', 'sejarah') }}">Sejarah Singkat</a></li>
                        <li><a href="{{ route('pageDetail', 'visi-misi') }}">Visi & Misi</a></li>
                        <li><a href="{{ route('pageDetail', 'organisasi') }}">Organisasi</a></li>
                        <li><a href="{{ route('pageDetail', 'lembaga-pendidikan') }}">Lembaga Pendidikan</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('register') }}">Pendaftaran</a></li>
                <li><a href="{{ route('contactUs') }}">Hubungi Kami</a></li>
                <li><a href="http://lama.anaksholeh.sch.id/">Web Lama</a></li>
            </ul><!--end navigation menu-->
            <!--end login button-->
        </div><!--end navigation-->
    </div><!--end container-->
</header><!--end header-->
<!-- Navbar End -->
<!-- Navbar End -->
