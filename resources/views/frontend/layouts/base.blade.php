<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('meta')
    <title>Anak Sholeh | @yield('title')</title>
    <!-- favicon -->
    <link rel="shortcut icon" href="/img/favicon.png">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-NKRR48PXHY"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-NKRR48PXHY');
    </script>


    @include('frontend.layouts.styles')
    @yield('styles')

</head>

<body>
    <!-- Loader -->
    <!-- <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div> -->
    <!-- Loader -->
    @if (Request::segment(3))
    @include('frontend.layouts.nav3')
    @elseif (Request::segment(2))
    @include('frontend.layouts.nav2')
    @elseif (Request::segment(1))
    @include('frontend.layouts.nav1')
    @else
    @include('frontend.layouts.nav')
    @endif

    @yield('content')

    <footer class="footer footer-bar">
        <div class="container text-center">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="text-sm-left">
                        <p class="mb-0">© {{ date('Y') }} <i class="mdi mdi-heart text-danger"></i> Yayasan Pendidikan Islam Terpadu Ibnu Abbas Mataram</a>.
                        </p>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </footer>

    <!-- Back to top -->
    <a href="#" class="btn btn-icon btn-soft-primary back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
    <!-- Back to top -->

    @include('frontend.layouts.scripts')
    @yield('scripts')
</body>

</html>
