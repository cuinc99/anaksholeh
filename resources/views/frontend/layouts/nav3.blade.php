

<!-- Navbar STart -->
<header id="topnav" class="sticky bg-white defaultscroll">
    <div class="container">
        <!-- Logo container-->
        <div>
            <a class="logo" href="/">
                <img src="/img/logo-anaksholeh.png" height="47" alt="logo-anak-sholeh">
            </a>
        </div>
         @if ($schoolUnit->id != 0)
         <div class="buy-button d-none d-sm-block">
            <a href="{{ route('schoolUnit', $schoolUnit->slug) }}" class="btn btn-primary">Kembali</a>
        </div>
        @endif

        <!--end login button-->
        <!-- End Logo container-->
        <div class="menu-extras">
            <div class="menu-item">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </div>
        </div>

        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                <li><a href="/">Beranda</a></li>
                {{-- @if ($schoolUnit->id != 0)
                <li><a href="{{ route('schoolUnit', $schoolUnit->slug) }}">Kembali</a></li>
                @endif --}}
                <li><a href="{{ route('registerForm', $schoolUnit->slug) }}">Pendaftaran</a></li>
                <li><a href="{{ route('contactUs') }}">Hubungi Kami</a></li>
                <li><a href="http://lama.anaksholeh.sch.id/">Web Lama</a></li>
            </ul><!--end navigation menu-->
            {{-- <div class="buy-menu-btn d-none">
                <a href="https://1.envato.market/4n73n" target="_blank" class="btn btn-primary">Buy Now</a>
            </div> --}}
            <!--end login button-->
        </div><!--end navigation-->
    </div><!--end container-->
</header><!--end header-->
<!-- Navbar End -->
<!-- Navbar End -->
