<!-- Bootstrap -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- Icons -->
<link href="/assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://unicons.iconscout.com/release/v2.1.9/css/unicons.css">
<!-- Slider -->
<link rel="stylesheet" href="/assets/css/owl.carousel.min.css"/>
<link rel="stylesheet" href="/assets/css/owl.theme.default.min.css"/>
<!-- Animation -->
<link href="/assets/css/animate.css" rel="stylesheet" />
<link href="/assets/css/animations-delay.css" rel="stylesheet" />
<!-- Animation -->
<link href="/assets/css/aos.css" rel="stylesheet" type="text/css" />
<!-- Magnific -->
<link href="/assets/css/magnific-popup.css" rel="stylesheet" type="text/css" />
<!-- Main Css -->
<link href="/assets/css/style.css" rel="stylesheet" type="text/css" id="theme-opt" />
<link href="/assets/css/custom.css" rel="stylesheet" type="text/css" id="theme-opt" />
<link href="/assets/css/colors/default.css" rel="stylesheet" id="color-opt">

@yield('styles')
