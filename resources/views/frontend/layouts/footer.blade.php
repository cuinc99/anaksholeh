<!-- Footer Start -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                <a href="#" class="logo-footer">
                    <img src="/img/logo-anaksholeh2.png" height="50" alt="">
                </a>
                <p class="mt-4">Yayasan Pendidikan Islam Terpadu Ibnu Abbas yang selanjutnya disebut YPIT Ibnu Abbas didirikan berdasarkan Akte Notaris Abdullah SH, no 3 Tanggal 1 April Tahun 1997.</p>
                <ul class="list-unstyled social-icon social mb-0 mt-4">
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                </ul>
                <!--end icon-->
            </div>
            <!--end col-->

            {{-- <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 class="text-light footer-head">Lembaga Pendidikan</h4>
                <ul class="list-unstyled footer-list mt-4">
                    @foreach ($schoolUnits as $schoolUnit)
                    <li><a href="#" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i>{{ $schoolUnit->name }}</a></li>
                    @endforeach
                </ul>
            </div> --}}
            <!--end col-->

            <div class="col-lg-3 col-md-8 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 class="text-light footer-head">Link Terkait</h4>
                <ul class="list-unstyled footer-list mt-4">
                    @foreach ($relatedLinks as $relatedLink)
                    <li><a href="{{ $relatedLink->url }}" class="text-foot"><i class="mdi mdi-chevron-right mr-1"></i> {{ $relatedLink->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <!--end col-->

            <div class="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 class="text-light footer-head">Buletin</h4>
                <p class="mt-4">Ayok daftar! kami akan mengirimkan berita dan pengumuman terbaru melalui email.</p>
                <form>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="foot-subscribe form-group position-relative">
                                <label>Tulis email anda <span class="text-danger">*</span></label>
                                <i data-feather="mail" class="fea icon-sm icons"></i>
                                <input type="email" name="email" id="emailsubscribe"
                                    class="form-control pl-5 rounded" placeholder="Your email : " required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <input type="submit" id="submitsubscribe" name="send"
                                class="btn btn-soft-primary btn-block" value="Submit">
                        </div>
                    </div>
                </form>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</footer>
<!--end footer-->

<!--end footer-->
<!-- Footer End -->
