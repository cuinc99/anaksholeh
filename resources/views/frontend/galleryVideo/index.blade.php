@extends('frontend.layouts.base')

@section('title', 'Galeri Video')

@section('content')
  <!-- Hero Start -->
  <section class="bg-half bg-light d-table w-100" style="background: url('/img/1043102220.jpg'); background-repeat: repeat; background-size: 10%;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title text-white title-dark">Galeri Video {{ $schoolUnit->name }} </h4>

                    <div class="subcribe-form mt-4 pt-2">
                        <form method="GET" action="{{ url()->current() }}">
                            <div class="form-group mb-0">
                                <input type="text" id="help" name="keyword" value="{{ isset($_GET['keyword']) ? $_GET['keyword'] : '' }}" class="border bg-white rounded-pill shadow" required placeholder="Ketikan judul video disni...">
                                <button type="submit" class="btn btn-pills btn-primary">Cari</button>
                            </div>
                        </form>
                    </div>

                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('schoolUnit', $schoolUnit->slug) }}">{{ $schoolUnit->name }}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Galeri Video</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<section class="section">
    <div class="container">
        <div class="row projects-wrapper" id="counter">
            @foreach ($galleryVideos as $galleryVideo)

            <div class="col-lg-4 col-md-6 col-12 mb-4 pb-2 branding">
                <div class="card border-0 work-container work-grid position-relative d-block overflow-hidden rounded-0">
                    <div class="card-body p-0">
                        <iframe width="100%" height="195px" src="https://www.youtube.com/embed/{{ substr($galleryVideo->link, -11) }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                        <div class="content bg-white p-3">
                            <h5 class="mb-0"><a href="{{ route('galleryVideoDetail', [$schoolUnit->slug, $galleryVideo->slug]) }}" class="text-dark title">{{ Str::limit($galleryVideo->title, 50) }}</a></h5>
                        </div>
                    </div>
                </div>
            </div><!--end col-->

            @endforeach

            <!-- PAGINATION START -->
            {{ $galleryVideos->withQueryString()->links() }}
            <!-- PAGINATION END -->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- End -->
@endsection
