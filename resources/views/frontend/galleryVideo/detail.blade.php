@extends('frontend.layouts.base')

@section('meta')
{{-- <meta property="og:title" content="{{ $video->title }}">
<meta property="og:url" content="https://www.youtube.com/watch?v=NJrFH6BIiwk">
<meta property="og:description" content="{!! Str::limit(strip_tags($video->desc), 100) !!}">
<meta property="og:url" content="{{ route('galleryVideoDetail', [$video->schoolUnit->slug, $video->slug]) }}"> --}}
<meta property="fb:app_id" content="87741124305">
<meta property="og:url" content="{{ $video->link }}">
<meta property="og:title" content="{{ $video->title }}">
<meta property="og:description" content="{!! Str::limit(strip_tags($video->desc), 100) !!}">
<meta property="og:type" content="video">
<meta property="og:video" content="{{ $video->link }}?version=3&autohide=1">
<meta property="og:video:type" content="application/x-shockwave-flash">
<meta property="og:video:width" content="398">
<meta property="og:video:height" content="264">
@endsection

@section('title', $video->title)

@section('content')

<!-- Hero Start -->
<section class="bg-half d-table w-100" style="background: url('/assets/images/digital/home-bg.png') center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title text-white title-dark"> {{ $video->title ?? '' }} </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('schoolUnit', $video->schoolUnit->slug) }}">{{ $video->schoolUnit->name }}</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('galleryVideos', [$video->schoolUnit->slug]) }}">Galeri Video</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->

<section class="section">
    <div class="container">

        <div class="row projects-wrapper mt-4 pt-2">
            <p>{{ $video->desc ?? '' }}</p>
            <iframe width="100%" height="700px" src="https://www.youtube.com/embed/{{ substr($video->link, -11) }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- End -->
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-60b24a8c6b265e03"></script>

@endsection
