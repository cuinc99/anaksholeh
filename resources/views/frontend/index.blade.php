@extends('frontend.layouts.base')

@section('title', 'Beranda')

@section('content')
<!-- Hero Start -->
<section class="home-slider position-relative">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach ($sliders as $slider)
            @if ($slider->image->count())
            @foreach($slider->image as $key => $entry)
            @php
                $img = $entry['slider'];
            @endphp
            @endforeach
            @else
            @php
                $img = '/assets/images/coworking/bg01.jpg'
            @endphp
            @endif
            <div class="carousel-item align-items-center {{ $loop->first ? 'active' : '' }}"
                style="background-image:url({{ $img }});">
                <div class="bg-overlay"></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="text-center col-lg-12">
                            <div class="mt-4 title-heading">
                                <h1 class="mb-3 text-white heading title-dark animated fadeInUpBig animation-delay-3">{{ $slider->title }}</h1>
                                <p class="mx-auto para-desc text-light para-dark animated fadeInUpBig animation-delay-7">{{ $slider->desc ?? '' }}</p>
                                @if ($slider->link)
                                <div class="pt-2 mt-4 animated fadeInUpBig animation-delay-11">
                                    <a href="{{ $slider->link }}" class="mt-2 btn btn-primary">Lihat Selengkapnya</a>
                                </div>
                                @endif
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end carousel item-->
            @endforeach
        </div>
        <!--end carousel inner-->
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!--end carousel-->
</section>
<!--end section-->
<!--end section-->
<!-- Hero End -->
<!-- Hero Start -->
<section class="shadow-lg section border-bottom">
    <div class="container">
        <div class="row justify-content-center">
            <div class="text-center col-12">
                <div class="pb-2 mb-4 section-title">
                    <h4 class="mb-4 title" data-aos="fade-up" data-aos-duration="1400">Lembaga Pendidikan</h4>
                    <p class="mx-auto mb-0 text-muted para-desc" data-aos="fade-up" data-aos-duration="1800">Berikut lembaga pendidikan formal yang didirikan oleh Yayasan Pendidikan Islam Terpadu <span class="text-primary font-weight-bold">Ibnu Abbas</span> Mataram.</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row align-items-center">
            @foreach ($schoolUnits as $schoolUnit)
            @if ($schoolUnit->logo->count())
            @foreach($schoolUnit->logo as $key => $entry)
            @php
                $background = $entry['background'];
            @endphp
            @endforeach
            @else
            @php
                $background = '/img/banner.png'
            @endphp
            @endif
            <div class="pt-2 mt-4 col-md-2 col-6" data-aos="fade-up" data-aos-duration="1{{ $loop->iteration + 3 }}00">
                <div class="text-center border-0 rounded shadow card pricing-rates business-rate bg-light">
                    <div class="card-body" style="padding: 0">
                        {{-- @foreach($schoolUnit->logo as $key => $entry) --}}
                        {{-- <img src="/assets/images/statistic/school.svg" class="mb-4" height="50" alt="{{ $schoolUnit->name }}" title="{{ $schoolUnit->name }}"> --}}
                        <img src="{{ $background }}" class="mb-4" width="100%" alt="{{ $schoolUnit->name }}" title="{{ $schoolUnit->name }}" style="padding: 0">
                        {{-- @endforeach --}}
                        <h2 class="title text-uppercase">{{ $schoolUnit->name ?? '-' }}</h2>

                        <a href="{{ route('schoolUnit', $schoolUnit->slug) }}" class="mt-4 btn btn-outline-primary btn-sm" style="margin-bottom: 30px">Kunjungi</a>
                    </div>
                </div>
            </div><!--end col-->
            @endforeach
        </div><!--end row-->
    </div><!--end container-->
</section>
<!--end section-->
<!-- Hero End -->
{{-- berita --}}
<section class="section bg-light">
    <div class="container">
        <div class="pb-4 mb-4 row align-items-center">
            <div class="col-md-8">
                <div class="text-center section-title text-md-left">
                    <h4 class="mb-4">Berita dan Artikel Terbaru</h4>
                    <p class="mb-0 text-muted para-desc">Berikut berita dan artikel terbaru Yayasan Pendidikan Islam Terpadu <span class="text-primary font-weight-bold">Ibnu Abbas</span> Mataram.</p>
                </div>
            </div>
            <!--end col-->

            <div class="mt-4 col-md-4 mt-sm-0">
                <div class="text-center text-md-right">
                    <a href="{{ route('blog', [$su->slug]) }}" class="btn btn-soft-primary">Lihat Semua <i data-feather="arrow-right"
                            class="fea icon-sm"></i></a>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->

        <div class="row">
            @foreach ($theNews as $news)
            <div class="pt-2 mt-4 col-lg-4 col-md-6">
                <div class="overflow-hidden border-0 rounded shadow card blog">
                    <div class="position-relative">
                        @if ($news->cover->count())
                        @foreach($news->cover as $key => $entry)
                        <img src="{{ $entry['article_thumbnail'] }}" class="card-img-top" alt="{{ $entry['name'] }}" title="{{ $entry['name'] }}">
                        @endforeach
                        @else
                        <img src="/assets/images/blog/04.jpg" class="card-img-top" alt="...">
                        @endif
                        <div class="overlay rounded-top bg-dark"></div>
                    </div>
                    <div class="card-body content">
                        <h5><a href="{{ route('articleDetail', [$su->slug, $news->category->slug, $news->slug]) }}" class="card-title title text-dark">{{ Str::limit($news->title, 50) }}</a></h5>
                        <div class="mt-3 post-meta d-flex justify-content-between">
                            <ul class="mb-0 list-unstyled">
                                {{-- <li class="mb-0 mr-2 list-inline-item"><a href="javascript:void(0)"
                                        class="text-muted like"><i class="mr-1 mdi mdi-heart-outline"></i>33</a></li> --}}
                                <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i
                                            class="mr-1 mdi mdi-eye-outline"></i>{{ $news->visit ?? '0' }}</a></li>
                            </ul>
                            <a href="{{ route('articleDetail', [$su->slug, $news->category->slug, $news->slug]) }}" class="text-muted readmore">Selengkapnya <i
                                    class="mdi mdi-chevron-right"></i></a>
                        </div>
                    </div>
                    <div class="author">
                        <small class="text-light user d-block"><i class="mdi mdi-account"></i> {{ $su->name ?? '' }}</small>
                        <small class="text-light date"><i class="mdi mdi-calendar-check"></i> {{ Date::parse($news->published_at)->diffForHumans() }}</small>
                    </div>
                </div>
            </div>
            <!--end col-->
            @endforeach
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
<!-- How It Work Start -->
<section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="text-center col-12">
                <div class="pb-2 mb-4 section-title">
                    <h4 class="mb-4 title">Statistik</h4>
                    <p class="mx-auto mb-0 text-muted para-desc">Berikut statistik terkini Yayasan Pendidikan Islam Terpadu <span class="text-primary font-weight-bold">Ibnu Abbas</span> Mataram.</p>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->

        <div class="row" id="counter">
            <div class="pt-2 mt-4 col-md-3 col-6">
                <div class="text-center counter-box">
                    <img src="/assets/images/statistic/school.svg" class="avatar avatar-small" alt="">
                    <h2 class="mt-4 mb-0"><span class="counter-value" data-count="{{ $statistic->school_unit }}">0</span></h2>
                    <h6 class="counter-head text-muted">Lembaga Pendidikan</h6>
                </div>
                <!--end counter box-->
            </div>

            <div class="pt-2 mt-4 col-md-3 col-6">
                <div class="text-center counter-box">
                    <img src="/assets/images/statistic/teacher.svg" class="avatar avatar-small" alt="">
                    <h2 class="mt-4 mb-0"><span class="counter-value" data-count="{{ $statistic->teacher }}">0</span>+</h2>
                    <h6 class="counter-head text-muted">Tenaga Pengajar</h6>
                </div>
                <!--end counter box-->
            </div>

            <div class="pt-2 mt-4 col-md-3 col-6">
                <div class="text-center counter-box">
                    <img src="/assets/images/statistic/employee.svg" class="avatar avatar-small" alt="">
                    <h2 class="mt-4 mb-0"><span class="counter-value" data-count="{{ $statistic->employee }}">0</span>+</h2>
                    <h6 class="counter-head text-muted">Karyawan</h6>
                </div>
                <!--end counter box-->
            </div>

            <div class="pt-2 mt-4 col-md-3 col-6">
                <div class="text-center counter-box">
                    <img src="/assets/images/statistic/student.svg" class="avatar avatar-small" alt="">
                    <h2 class="mt-4 mb-0"><span class="counter-value" data-count="{{ $statistic->student }}">0</span>+</h2>
                    <h6 class="counter-head text-muted">Siswa</h6>
                </div>
                <!--end counter box-->
            </div>
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
<!-- How It Work End -->

<!-- Testi Start -->

<!--end section-->
<!-- Testi End -->

<!--end section-->
<section class="pb-0 section bg-light border-top">
    <div class="container">
        <div class="row justify-content-center">
            <div class="text-center col-12">
                <div class="pb-2 mb-4 section-title">
                    <h4 class="mb-4 title">Testimonial</h4>
                    <p class="mx-auto mb-0 text-muted para-desc">Apa kata mereka tentang Yayasan Pendidikan Islam Terpadu <span class="text-primary font-weight-bold">Ibnu Abbas</span> Mataram.</p>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->

        <div class="row">
            <div class="pt-2 mt-4 col-12" style="margin-bottom: 150px">
                <div id="customer-testi" class="owl-carousel owl-theme">
                    @foreach ($testimonials as $testimonial)

                    <div class="m-2 media customer-testi">
                        @if ($testimonial->cover->count())
                        @foreach($testimonial->cover as $key => $entry)
                        <img src="{{ $entry['preview_thumbnail'] }}" class="mr-3 rounded shadow avatar avatar-small" alt="{{ $entry['name'] }}" title="{{ $entry['name'] }}">
                        @endforeach
                        @else
                        <img src="/assets/images/client/01.jpg" class="mr-3 rounded shadow avatar avatar-small" alt="">
                        @endif
                        <div class="p-3 bg-white rounded shadow media-body content position-relative">
                            {{-- <ul class="mb-0 list-unstyled">
                                <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                            </ul> --}}
                            {{-- <p class="mt-2 text-muted"
                                x-data="{ isCollapsed: false, maxLength: 20, originalContent: '', content: '' }"
                                x-init="originalContent = $el.firstElementChild.textContent.trim(); content = originalContent.slice(0, maxLength)"
                            >"
                                <span x-text="isCollapsed ? originalContent : content">{{ $testimonial->message}}</span>
                            "
                            <button class="btn btn-info"
                            @click="isCollapsed = !isCollapsed"
                            x-show="originalContent.length > maxLength"
                            x-text="isCollapsed ? 'Show less' : 'Show more'"
                          ></button>
                            </p> --}}
                            <p class="mt-2 text-muted">
                            @if(strlen($testimonial->message) > 100)
                            {{substr($testimonial->message,0,100)}}
                            <span class="read-more-show hide_content">More<i class="fa fa-angle-down"></i></span>
                            <span class="read-more-content"> {{substr($testimonial->message,100,strlen($testimonial->message))}}
                            <span class="read-more-hide hide_content">Less <i class="fa fa-angle-up"></i></span> </span>
                            @else
                            {{$testimonial->message}}
                            @endif
                            </p>

                            <h6 class="text-primary">- {{ $testimonial->name }}</h6>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
{{-- <div class="position-relative">
    <div class="overflow-hidden shape text-footer">
        <svg viewBox="0 0 2880 250" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M720 125L2160 0H2880V250H0V125H720Z" fill="currentColor"></path>
        </svg>
    </div>
</div> --}}
<div class="position-relative">
    <div class="overflow-hidden shape text-footer">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!-- Partners End -->

@include('frontend.layouts.footer')


@endsection

@section('styles')
<style>
    [x-cloak] { display: none; }
</style>
<style type="text/css">
    .read-more-show{
      cursor:pointer;
      color: #2326ed;
    }
    .read-more-hide{
      cursor:pointer;
      color: #ed8323;
    }

    .hide_content{
      display: none;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript">
    // Hide the extra content initially, using JS so that if JS is disabled, no problemo:
    $('.read-more-content').addClass('hide_content')
    $('.read-more-show, .read-more-hide').removeClass('hide_content')

    // Set up the toggle effect:
    $('.read-more-show').on('click', function(e) {
        $(this).next('.read-more-content').removeClass('hide_content');
        $(this).addClass('hide_content');
        e.preventDefault();
    });

    // Changes contributed by @diego-rzg
    $('.read-more-hide').on('click', function(e) {
        var p = $(this).parent('.read-more-content');
        p.addClass('hide_content');
        p.prev('.read-more-show').removeClass('hide_content'); // Hide only the preceding "Read More"
        e.preventDefault();
    });
</script>
@endsection
