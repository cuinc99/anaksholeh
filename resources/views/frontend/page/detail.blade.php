@extends('frontend.layouts.base')

@section('meta')
<meta property="og:title" content="{{ $page->title }}">
<meta property="og:image" content="{{ asset('/img/full-bloom.png') }}">
<meta property="og:description" content="{!! Str::limit(strip_tags($page->content), 100) !!}">
<meta property="og:url" content="{{ route('pageDetail', [$page->slug]) }}">
@endsection

@section('title', $page->title)

@section('content')
<!-- Hero Start -->
<section class="bg-half d-table w-100" style="background: url('/img/full-bloom.png'); background-repeat: repeat;">
    {{-- <div class="bg-overlay"></div> --}}
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level title-heading text-center">
                    <span class="element" data-elements="Yayasan Pendidikan Islam Terpadu ( IBNU Abbas ), Anak Sholeh Mataram"></span><br>
                    <h1 class="heading text-dark text-shadow-title"> {{ $page->title ?? '' }} </h1>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <li class="breadcrumb-item"><a href="/">Kembali</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
{{-- <div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div> --}}
<!-- Hero End -->

<section class="section">
    <div class="container">
        <div class="row justify-content-center" id="counter">
            <div class="col-lg-10">
                <div class="row">

                    <div class="col-md-12">
                        <div id="page">
                            {!! $page->content !!}
                        </div>
                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-60b24a8c6b265e03"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
    <script>
        $('#page img').addClass('img-fluid');
    </script>
@endsection
