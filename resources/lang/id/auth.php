<?php

return [
    'failed'   => 'nama pengguna atau kata sandi anda salah, silahkan coba kembali!',
    'password' => 'Kata sandi yang dimasukkan salah.',
    'throttle' => 'Terlalu banyak percobaan, silahkan coba kembali dalam   :seconds detik',
];
