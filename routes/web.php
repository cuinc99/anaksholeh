<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\EventController;
use App\Http\Controllers\Admin\PosterController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ContactUsController;
use App\Http\Controllers\Admin\StatisticController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\SchoolUnitController;
use App\Http\Controllers\Admin\PendaftaranController;
use App\Http\Controllers\Admin\RelatedLinkController;
use App\Http\Controllers\Admin\TahunAjaranController;
use App\Http\Controllers\Admin\TestimonialController;
use App\Http\Controllers\Admin\GalleryPhotoController;
use App\Http\Controllers\Admin\GalleryVideoController;

Route::get('/', [FrontendController::class, 'index'])->name('index');
Route::get('hubungi-kami.html', [FrontendController::class, 'contactUs'])->name('contactUs');
Route::post('hubungi-kami.html', [FrontendController::class, 'contactUsStore'])->name('contactUs.store');
Route::get('pendaftaran.html', [FrontendController::class, 'register'])->name('register');
Route::get('{slug}.html', [FrontendController::class, 'pageDetail'])->name('pageDetail');
Route::get('/profil/{slug}.html', [FrontendController::class, 'profile'])->name('profile');
Route::get('/lembaga-pendidikan/{slug}.html', [FrontendController::class, 'schoolUnit'])->name('schoolUnit');
Route::get('{lp}/agenda/{slug}.html', [FrontendController::class, 'eventDetail'])->name('eventDetail');
Route::get('{lp}/galeri-foto.html', [FrontendController::class, 'galleryPhotos'])->name('galleryPhotos');
Route::get('{lp}/galeri-foto/{slug}.html', [FrontendController::class, 'galleryPhotoDetail'])->name('galleryPhotoDetail');
Route::get('{lp}/galeri-video.html', [FrontendController::class, 'galleryVideos'])->name('galleryVideos');
Route::get('{lp}/galeri-video/{slug}.html', [FrontendController::class, 'galleryVideoDetail'])->name('galleryVideoDetail');
Route::get('{lp}/poster.html', [FrontendController::class, 'posters'])->name('posters');
Route::get('{lp}/poster/{slug}.html', [FrontendController::class, 'posterDetail'])->name('posterDetail');
Route::get('{lp}/blog.html', [FrontendController::class, 'blog'])->name('blog');
Route::get('{lp}/pendaftaran.html', [FrontendController::class, 'registerForm'])->name('registerForm');
Route::post('pendaftaran/store', [FrontendController::class, 'registerStore'])->name('registerStore');
Route::get('{lp}/{category}.html', [FrontendController::class, 'articles'])->name('articles');
Route::get('{lp}/{category}/{slug}.html', [FrontendController::class, 'articleDetail'])->name('articleDetail');

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth']], function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');

    // Permissions
    Route::resource('permissions', PermissionController::class, ['except' => ['store', 'update', 'destroy']]);

    // Roles
    Route::resource('roles', RoleController::class, ['except' => ['store', 'update', 'destroy']]);

    // Users
    Route::resource('users', UserController::class, ['except' => ['store', 'update', 'destroy']]);

    // School Unit
    Route::post('school-units/media', [SchoolUnitController::class, 'storeMedia'])->name('school-units.storeMedia');
    Route::resource('school-units', SchoolUnitController::class, ['except' => ['store', 'update', 'destroy']]);

    // Category
    Route::resource('categories', CategoryController::class, ['except' => ['store', 'update', 'destroy']]);

    // Article
    Route::post('articles/media', [ArticleController::class, 'storeMedia'])->name('articles.storeMedia');
    Route::resource('articles', ArticleController::class, ['except' => ['store', 'update', 'destroy']]);

    // Event
    Route::post('events/media', [EventController::class, 'storeMedia'])->name('events.storeMedia');
    Route::resource('events', EventController::class, ['except' => ['store', 'update', 'destroy']]);

    // Pages
    Route::resource('pages', PageController::class, ['except' => ['store', 'update', 'destroy']]);

    // Gallery Photo
    Route::post('gallery-photos/media', [GalleryPhotoController::class, 'storeMedia'])->name('gallery-photos.storeMedia');
    Route::resource('gallery-photos', GalleryPhotoController::class, ['except' => ['store', 'update', 'destroy']]);

    // Gallery Video
    Route::resource('gallery-videos', GalleryVideoController::class, ['except' => ['store', 'update', 'destroy']]);

    // Poster
    Route::post('posters/media', [PosterController::class, 'storeMedia'])->name('posters.storeMedia');
    Route::resource('posters', PosterController::class, ['except' => ['store', 'update', 'destroy']]);

    // Slider
    Route::post('sliders/media', [SliderController::class, 'storeMedia'])->name('sliders.storeMedia');
    Route::resource('sliders', SliderController::class, ['except' => ['store', 'update', 'destroy']]);

    // Statistic
    Route::resource('statistics', StatisticController::class, ['except' => ['store', 'update', 'destroy']]);

    // Related Link
    Route::post('related-links/media', [RelatedLinkController::class, 'storeMedia'])->name('related-links.storeMedia');
    Route::resource('related-links', RelatedLinkController::class, ['except' => ['store', 'update', 'destroy']]);

    // Testimonial
    Route::post('testimonials/media', [TestimonialController::class, 'storeMedia'])->name('testimonials.storeMedia');
    Route::resource('testimonials', TestimonialController::class, ['except' => ['store', 'update', 'destroy']]);

    // Contact Us
    Route::resource('contactuses', ContactUsController::class, ['except' => ['store', 'update', 'destroy']]);

    // Pendaftaran
    Route::resource('pendaftarans', PendaftaranController::class, ['except' => ['store', 'update', 'destroy']]);

    // Tahun Ajaran
    Route::resource('tahun-ajarans', TahunAjaranController::class, ['except' => ['store', 'update', 'destroy']]);
    Route::get('tahun-ajarans/{tahunAjaran}/export', [TahunAjaranController::class, 'export'])->name('tahun-ajarans.export');
});
