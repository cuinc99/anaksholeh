<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTahunAjaransTable extends Migration
{
    public function up()
    {
        Schema::table('tahun_ajarans', function (Blueprint $table) {
            $table->unsignedBigInteger('school_unit_id')->nullable();
            $table->foreign('school_unit_id', 'school_unit_fk_4855510')->references('id')->on('school_units');
        });
    }
}
