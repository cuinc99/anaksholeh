<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendaftaransTable extends Migration
{
    public function up()
    {
        Schema::create('pendaftarans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_lengkap');
            $table->string('nama_pangilan')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('nisn')->nullable();
            $table->string('golongan_darah')->nullable();
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir')->nullable();
            $table->string('nik')->nullable();
            $table->string('kewarganegaraan')->nullable();
            $table->integer('anak_ke')->nullable();
            $table->integer('jumlah_saudara')->nullable();
            $table->string('bahasa')->nullable();
            $table->string('asal_sekolah')->nullable();
            $table->longText('alamat_siswa');
            $table->string('nama_ayah');
            $table->integer('tahun_lahir_ayah')->nullable();
            $table->string('pendidikan_ayah')->nullable();
            $table->string('telepon_ayah')->nullable();
            $table->string('pekerjaan_ayah');
            $table->string('penghasilan_ayah')->nullable();
            $table->string('nama_ibu');
            $table->integer('tahun_lahir_ibu')->nullable();
            $table->string('pendidikan_ibu')->nullable();
            $table->string('telepon_ibu')->nullable();
            $table->string('pekerjaan_ibu');
            $table->string('penghasilan_ibu')->nullable();
            $table->longText('alamat_ayah');
            $table->longText('alamat_ibu');
            $table->string('pilihan_kelas')->nullable();
            $table->string('nama_wali')->nullable();
            $table->string('pekerjaan_wali')->nullable();
            $table->integer('jarak_rumah')->nullable();
            $table->integer('waktu_tempuh')->nullable();
            $table->string('biaya_administrasi')->nullable();
            $table->string('kartu_keluarga')->nullable();
            $table->string('akta_kelahiran')->nullable();
            $table->string('pas_foto')->nullable();
            $table->bigInteger('school_unit_id');
            $table->bigInteger('tahun_ajaran_id');
            $table->timestamps();
        });
    }
}
