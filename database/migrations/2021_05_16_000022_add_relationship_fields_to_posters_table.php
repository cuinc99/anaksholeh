<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPostersTable extends Migration
{
    public function up()
    {
        Schema::table('posters', function (Blueprint $table) {
            $table->unsignedBigInteger('school_unit_id')->nullable();
            $table->foreign('school_unit_id', 'school_unit_fk_3913765')->references('id')->on('school_units');
        });
    }
}
