<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToEventsTable extends Migration
{
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->unsignedBigInteger('school_unit_id')->nullable();
            $table->foreign('school_unit_id', 'school_unit_fk_3846091')->references('id')->on('school_units');
        });
    }
}
