<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolUnitsTable extends Migration
{
    public function up()
    {
        Schema::create('school_units', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug')->nullable();
            $table->string('name')->nullable();
            $table->longText('profile')->nullable();
            $table->longText('vision_mission')->nullable();
            $table->string('location')->nullable();
            $table->string('fb')->nullable();
            $table->string('ig')->nullable();
            $table->string('tw')->nullable();
            $table->string('yt')->nullable();
            $table->string('wa')->nullable();
            $table->timestamps();
        });
    }
}
