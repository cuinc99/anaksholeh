<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug')->nullable();
            $table->string('title')->nullable();
            $table->longText('content')->nullable();
            $table->datetime('published_at')->nullable();
            $table->boolean('is_slider')->default(0)->nullable();
            $table->integer('visit')->default(0)->nullable();
            $table->timestamps();
        });
    }
}
