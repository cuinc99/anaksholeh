<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToGalleryPhotosTable extends Migration
{
    public function up()
    {
        Schema::table('gallery_photos', function (Blueprint $table) {
            $table->unsignedBigInteger('school_unit_id')->nullable();
            $table->foreign('school_unit_id', 'school_unit_fk_3846129')->references('id')->on('school_units');
        });
    }
}
