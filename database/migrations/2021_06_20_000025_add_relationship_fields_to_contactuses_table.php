<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToContactusesTable extends Migration
{
    public function up()
    {
        Schema::table('contactuses', function (Blueprint $table) {
            $table->unsignedBigInteger('school_unit_id');
            $table->foreign('school_unit_id', 'school_unit_fk_4198956')->references('id')->on('school_units');
        });
    }
}
