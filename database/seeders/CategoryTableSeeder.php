<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id'             => 1,
                'slug'           => 'berita',
                'name'          => 'Berita',
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 2,
                'slug'           => 'pengumuman',
                'name'          => 'Pengumuman',
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 3,
                'slug'           => 'artikel',
                'name'          => 'Artikel',
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
        ];

        Category::insert($data);
    }
}
