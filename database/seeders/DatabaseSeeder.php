<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\PageSeeder;
use Database\Seeders\EventSeeder;
use Database\Seeders\PosterSeeder;
use Database\Seeders\SliderSeeder;
use Database\Seeders\ArticleSeeder;
use Database\Seeders\StatisticSeeder;
use Database\Seeders\SchoolUnitSeeder;
use Database\Seeders\RelatedLinkSeeder;
use Database\Seeders\TestimonialSeeder;
use Database\Seeders\GalleryPhotoSeeder;
use Database\Seeders\GalleryVideoSeeder;
use Database\Seeders\CategoryTableSeeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
            SchoolUnitSeeder::class,
            CategoryTableSeeder::class,
            ArticleSeeder::class,
            PageSeeder::class,
            EventSeeder::class,
            GalleryVideoSeeder::class,
            GalleryPhotoSeeder::class,
            PosterSeeder::class,
            RelatedLinkSeeder::class,
            StatisticSeeder::class,
            TestimonialSeeder::class,
            SliderSeeder::class,
        ]);
    }
}
