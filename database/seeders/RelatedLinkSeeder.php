<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use App\Models\Poster;
use App\Models\Article;
use App\Models\RelatedLink;
use App\Models\Testimonial;
use Illuminate\Support\Str;
use App\Models\GalleryVideo;
use Illuminate\Database\Seeder;

class RelatedLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i=0; $i < 10; $i++) {

            $data = [
                'name'           => $faker->name,
                'url'        => $faker->domainName,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ];

            RelatedLink::create($data);
        }


    }
}
