<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use App\Models\Poster;
use App\Models\Article;
use Illuminate\Support\Str;
use App\Models\GalleryVideo;
use Illuminate\Database\Seeder;

class PosterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i=0; $i < 10; $i++) {
            $title = Str::limit($faker->text, 100);

            $data = [
                'slug'           => Str::slug($title),
                'title'           => $title,
                'desc'        => $faker->text,
                'school_unit_id'       => rand(1, 6),
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ];

            Poster::create($data);
        }


    }
}
