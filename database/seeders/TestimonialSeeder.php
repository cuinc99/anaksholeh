<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use App\Models\Poster;
use App\Models\Article;
use App\Models\Testimonial;
use Illuminate\Support\Str;
use App\Models\GalleryVideo;
use Illuminate\Database\Seeder;

class TestimonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i=0; $i < 10; $i++) {

            $data = [
                'name'           => $faker->name,
                'message'        => $faker->text,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ];

            Testimonial::create($data);
        }


    }
}
