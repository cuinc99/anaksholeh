<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use App\Models\Article;
use Illuminate\Support\Str;
use App\Models\GalleryVideo;
use Illuminate\Database\Seeder;

class GalleryVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i=0; $i < 10; $i++) {
            $title = Str::limit($faker->text, 100);

            $data = [
                'slug'           => Str::slug($title),
                'title'           => $title,
                'desc'        => $faker->text,
                'link'        => 'https://www.youtube.com/watch?v=sc9z_ad4F74',
                'school_unit_id'       => rand(1, 6),
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ];

            GalleryVideo::create($data);
        }


    }
}
