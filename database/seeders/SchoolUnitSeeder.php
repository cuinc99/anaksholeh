<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use App\Models\SchoolUnit;
use Illuminate\Database\Seeder;

class SchoolUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $data = [
            [
                'id'             => 7,
                'slug'           => 'yayasan',
                'name'           => 'Yayasan',
                'profile'        => $faker->text,
                'vision_mission' => $faker->text,
                'location'       => $faker->address,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 1,
                'slug'           => 'smait',
                'name'           => 'SMAIT',
                'profile'        => $faker->text,
                'vision_mission' => $faker->text,
                'location'       => $faker->address,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 2,
                'slug'           => 'smpit',
                'name'           => 'SMPIT',
                'profile'        => $faker->text,
                'vision_mission' => $faker->text,
                'location'       => $faker->address,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 3,
                'slug'           => 'sdit-1',
                'name'           => 'SDIT-1',
                'profile'        => $faker->text,
                'vision_mission' => $faker->text,
                'location'       => $faker->address,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 4,
                'slug'           => 'sdit-2',
                'name'           => 'SDIT-2',
                'profile'        => $faker->text,
                'vision_mission' => $faker->text,
                'location'       => $faker->address,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 5,
                'slug'           => 'tkit',
                'name'           => 'TKIT',
                'profile'        => $faker->text,
                'vision_mission' => $faker->text,
                'location'       => $faker->address,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 6,
                'slug'           => 'kbit',
                'name'           => 'KBIT',
                'profile'        => $faker->text,
                'vision_mission' => $faker->text,
                'location'       => $faker->address,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
        ];

        SchoolUnit::insert($data);
    }
}
