<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use App\Models\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    public function run()
    {
        $faker = Factory::create();

        $data = [
            [
                'id'             => 1,
                'slug'           => 'sejarah',
                'title'          => 'Sejarah Singkat',
                'content'       => $faker->text,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 2,
                'slug'           => 'visi-misi',
                'title'          => 'Visi & Misi',
                'content'       => $faker->text,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 3,
                'slug'           => 'organisasi',
                'title'          => 'Organisasi',
                'content'       => $faker->text,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 4,
                'slug'           => 'lembaga-pendidikan',
                'title'          => 'Lembaga Pendidikan',
                'content'       => $faker->text,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
        ];

        Page::insert($data);
    }
}
