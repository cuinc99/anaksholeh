<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use App\Models\Event;
use App\Models\Article;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i=0; $i < 10; $i++) {
            $title = Str::limit($faker->text, 100);

            $data = [
                'slug'           => Str::slug($title),
                'title'           => $title,
                'from_date'        => Carbon::now()->subDays(\rand(6, 10))->format('Y-m-d'),
                'to_date' => Carbon::now()->subDays(\rand(1, 5))->format('Y-m-d'),
                'location'       => $faker->address,
                'desc'       => $faker->text,
                'school_unit_id'       => rand(1, 6),
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ];

            Event::create($data);
        }


    }
}
