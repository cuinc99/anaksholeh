<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use App\Models\Article;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i=0; $i < 50; $i++) {
            $title = Str::limit($faker->text, 100);

            $data = [
                'slug'           => Str::slug($title),
                'title'           => $title,
                'content'        => $faker->text,
                'published_at' => Carbon::now(),
                'is_slider'       => 0,
                'school_unit_id'       => rand(1, 6),
                'category_id'       => \rand(1, 3),
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ];

            Article::create($data);
        }


    }
}
