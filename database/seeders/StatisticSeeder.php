<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use App\Models\Poster;
use App\Models\Article;
use App\Models\Statistic;
use Illuminate\Support\Str;
use App\Models\GalleryVideo;
use Illuminate\Database\Seeder;

class StatisticSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


            $data = [
                'school_unit' => 6,
                'teacher' => 100,
                'employee' => 50,
                'student' => 1000,
            ];

            Statistic::create($data);


    }
}
