<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'school_unit_create',
            ],
            [
                'id'    => 18,
                'title' => 'school_unit_edit',
            ],
            [
                'id'    => 19,
                'title' => 'school_unit_show',
            ],
            [
                'id'    => 20,
                'title' => 'school_unit_delete',
            ],
            [
                'id'    => 21,
                'title' => 'school_unit_access',
            ],
            [
                'id'    => 22,
                'title' => 'category_create',
            ],
            [
                'id'    => 23,
                'title' => 'category_edit',
            ],
            [
                'id'    => 24,
                'title' => 'category_show',
            ],
            [
                'id'    => 25,
                'title' => 'category_delete',
            ],
            [
                'id'    => 26,
                'title' => 'category_access',
            ],
            [
                'id'    => 27,
                'title' => 'article_create',
            ],
            [
                'id'    => 28,
                'title' => 'article_edit',
            ],
            [
                'id'    => 29,
                'title' => 'article_show',
            ],
            [
                'id'    => 30,
                'title' => 'article_delete',
            ],
            [
                'id'    => 31,
                'title' => 'article_access',
            ],
            [
                'id'    => 32,
                'title' => 'event_create',
            ],
            [
                'id'    => 33,
                'title' => 'event_edit',
            ],
            [
                'id'    => 34,
                'title' => 'event_show',
            ],
            [
                'id'    => 35,
                'title' => 'event_delete',
            ],
            [
                'id'    => 36,
                'title' => 'event_access',
            ],
            [
                'id'    => 37,
                'title' => 'page_create',
            ],
            [
                'id'    => 38,
                'title' => 'page_edit',
            ],
            [
                'id'    => 39,
                'title' => 'page_show',
            ],
            [
                'id'    => 40,
                'title' => 'page_delete',
            ],
            [
                'id'    => 41,
                'title' => 'page_access',
            ],
            [
                'id'    => 42,
                'title' => 'gallery_photo_create',
            ],
            [
                'id'    => 43,
                'title' => 'gallery_photo_edit',
            ],
            [
                'id'    => 44,
                'title' => 'gallery_photo_show',
            ],
            [
                'id'    => 45,
                'title' => 'gallery_photo_delete',
            ],
            [
                'id'    => 46,
                'title' => 'gallery_photo_access',
            ],
            [
                'id'    => 47,
                'title' => 'gallery_video_create',
            ],
            [
                'id'    => 48,
                'title' => 'gallery_video_edit',
            ],
            [
                'id'    => 49,
                'title' => 'gallery_video_show',
            ],
            [
                'id'    => 50,
                'title' => 'gallery_video_delete',
            ],
            [
                'id'    => 51,
                'title' => 'gallery_video_access',
            ],
            [
                'id'    => 52,
                'title' => 'poster_create',
            ],
            [
                'id'    => 53,
                'title' => 'poster_edit',
            ],
            [
                'id'    => 54,
                'title' => 'poster_show',
            ],
            [
                'id'    => 55,
                'title' => 'poster_delete',
            ],
            [
                'id'    => 56,
                'title' => 'poster_access',
            ],
            [
                'id'    => 57,
                'title' => 'slider_create',
            ],
            [
                'id'    => 58,
                'title' => 'slider_edit',
            ],
            [
                'id'    => 59,
                'title' => 'slider_show',
            ],
            [
                'id'    => 60,
                'title' => 'slider_delete',
            ],
            [
                'id'    => 61,
                'title' => 'slider_access',
            ],
            [
                'id'    => 62,
                'title' => 'setting_access',
            ],
            [
                'id'    => 63,
                'title' => 'statistic_create',
            ],
            [
                'id'    => 64,
                'title' => 'statistic_edit',
            ],
            [
                'id'    => 65,
                'title' => 'statistic_show',
            ],
            [
                'id'    => 66,
                'title' => 'statistic_delete',
            ],
            [
                'id'    => 67,
                'title' => 'statistic_access',
            ],
            [
                'id'    => 68,
                'title' => 'related_link_create',
            ],
            [
                'id'    => 69,
                'title' => 'related_link_edit',
            ],
            [
                'id'    => 70,
                'title' => 'related_link_show',
            ],
            [
                'id'    => 71,
                'title' => 'related_link_delete',
            ],
            [
                'id'    => 72,
                'title' => 'related_link_access',
            ],
            [
                'id'    => 73,
                'title' => 'testimonial_create',
            ],
            [
                'id'    => 74,
                'title' => 'testimonial_edit',
            ],
            [
                'id'    => 75,
                'title' => 'testimonial_show',
            ],
            [
                'id'    => 76,
                'title' => 'testimonial_delete',
            ],
            [
                'id'    => 77,
                'title' => 'testimonial_access',
            ],
            [
                'id'    => 78,
                'title' => 'contact_us_create',
            ],
            [
                'id'    => 79,
                'title' => 'contact_us_edit',
            ],
            [
                'id'    => 80,
                'title' => 'contact_us_show',
            ],
            [
                'id'    => 81,
                'title' => 'contact_us_delete',
            ],
            [
                'id'    => 82,
                'title' => 'contact_us_access',
            ],
            [
                'id'    => 83,
                'title' => 'profile_password_edit',
            ],
            [
                'id'    => 84,
                'title' => 'pendaftaran_create',
            ],
            [
                'id'    => 85,
                'title' => 'pendaftaran_edit',
            ],
            [
                'id'    => 86,
                'title' => 'pendaftaran_show',
            ],
            [
                'id'    => 87,
                'title' => 'pendaftaran_delete',
            ],
            [
                'id'    => 88,
                'title' => 'pendaftaran_access',
            ],
            [
                'id'    => 89,
                'title' => 'tahun_ajaran_create',
            ],
            [
                'id'    => 90,
                'title' => 'tahun_ajaran_edit',
            ],
            [
                'id'    => 91,
                'title' => 'tahun_ajaran_show',
            ],
            [
                'id'    => 92,
                'title' => 'tahun_ajaran_delete',
            ],
            [
                'id'    => 93,
                'title' => 'tahun_ajaran_access',
            ],
        ];

        Permission::insert($permissions);
    }
}
