<?php

namespace App\Http\Livewire\SchoolUnit;

use App\Models\SchoolUnit;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Edit extends Component
{
    public SchoolUnit $schoolUnit;

    public array $mediaToRemove = [];

    public array $mediaCollections = [];

    public function mount(SchoolUnit $schoolUnit)
    {
        $this->schoolUnit       = $schoolUnit;
        $this->mediaCollections = [
            'school_unit_logo' => $schoolUnit->logo,
        ];
    }

    public function render()
    {
        return view('livewire.school-unit.edit');
    }

    public function submit()
    {
        $this->validate();

        $this->schoolUnit->save();
        $this->syncMedia();

        return redirect()->route('admin.school-units.index');
    }

    public function addMedia($media): void
    {
        $this->mediaCollections[$media['collection_name']][] = $media;
    }

    public function removeMedia($media): void
    {
        $collection = collect($this->mediaCollections[$media['collection_name']]);

        $this->mediaCollections[$media['collection_name']] = $collection->reject(fn ($item) => $item['uuid'] === $media['uuid'])->toArray();

        $this->mediaToRemove[] = $media['uuid'];
    }

    public function getMediaCollection($name)
    {
        return $this->mediaCollections[$name];
    }

    protected function rules(): array
    {
        return [
            'schoolUnit.slug' => [
                'string',
                'nullable',
            ],
            'schoolUnit.name' => [
                'string',
                'nullable',
            ],
            'mediaCollections.school_unit_logo' => [
                'array',
                'nullable',
            ],
            'mediaCollections.school_unit_logo.*.id' => [
                'integer',
                'exists:media,id',
            ],
            'schoolUnit.profile' => [
                'string',
                'nullable',
            ],
            'schoolUnit.vision_mission' => [
                'string',
                'nullable',
            ],
            'schoolUnit.location' => [
                'string',
                'nullable',
            ],
            'schoolUnit.fb' => [
                'string',
                'nullable',
            ],
            'schoolUnit.ig' => [
                'string',
                'nullable',
            ],
            'schoolUnit.tw' => [
                'string',
                'nullable',
            ],
            'schoolUnit.yt' => [
                'string',
                'nullable',
            ],
            'schoolUnit.wa' => [
                'string',
                'nullable',
            ],
        ];
    }

    protected function syncMedia(): void
    {
        collect($this->mediaCollections)->flatten(1)
            ->each(fn ($item) => Media::where('uuid', $item['uuid'])
            ->update(['model_id' => $this->schoolUnit->id]));

        Media::whereIn('uuid', $this->mediaToRemove)->delete();
    }
}
