<?php

namespace App\Http\Livewire\GalleryVideo;

use App\Http\Livewire\WithConfirmation;
use App\Http\Livewire\WithSorting;
use App\Models\GalleryVideo;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    use WithSorting;
    use WithConfirmation;

    public int $perPage;

    public array $orderable;

    public string $search = '';

    public array $selected = [];

    public array $paginationOptions;

    protected $queryString = [
        'search' => [
            'except' => '',
        ],
        'sortBy' => [
            'except' => 'id',
        ],
        'sortDirection' => [
            'except' => 'asc',
        ],
    ];

    public function getSelectedCountProperty()
    {
        return count($this->selected);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingPerPage()
    {
        $this->resetPage();
    }

    public function resetSelected()
    {
        $this->selected = [];
    }

    public function mount()
    {
        $this->sortBy            = 'id';
        $this->sortDirection     = 'asc';
        $this->perPage           = 25;
        $this->paginationOptions = config('project.pagination.options');
        $this->orderable         = (new GalleryVideo())->orderable;
    }

    public function render()
    {
        $query = GalleryVideo::with(['schoolUnit'])->latest()->advancedFilter([
            's'               => $this->search ?: null,
            'order_column'    => $this->sortBy,
            'order_direction' => $this->sortDirection,
        ]);

        if (auth()->user()->school_unit_id != 0) {
            $query->where('school_unit_id', auth()->user()->school_unit_id);
        }

        $galleryVideos = $query->paginate($this->perPage);

        return view('livewire.gallery-video.index', compact('query', 'galleryVideos', 'galleryVideos'));
    }

    public function deleteSelected()
    {
        abort_if(Gate::denies('gallery_video_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        GalleryVideo::whereIn('id', $this->selected)->delete();

        $this->resetSelected();
    }

    public function delete(GalleryVideo $galleryVideo)
    {
        abort_if(Gate::denies('gallery_video_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $galleryVideo->delete();
    }
}
