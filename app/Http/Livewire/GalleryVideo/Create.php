<?php

namespace App\Http\Livewire\GalleryVideo;

use App\Models\GalleryVideo;
use App\Models\SchoolUnit;
use Livewire\Component;

class Create extends Component
{
    public array $listsForFields = [];

    public GalleryVideo $galleryVideo;

    public function mount(GalleryVideo $galleryVideo)
    {
        $this->galleryVideo = $galleryVideo;
        $this->initListsForFields();
    }

    public function render()
    {
        return view('livewire.gallery-video.create');
    }

    public function submit()
    {
        $this->validate();
        if (auth()->user()->school_unit_id != 0) {
            $this->galleryVideo->school_unit_id = auth()->user()->school_unit_id;
        }
        $this->galleryVideo->save();

        return redirect()->route('admin.gallery-videos.index');
    }

    protected function rules(): array
    {
        return [
            'galleryVideo.slug' => [
                'string',
                'nullable',
            ],
            'galleryVideo.title' => [
                'string',
                'nullable',
            ],
            'galleryVideo.desc' => [
                'string',
                'nullable',
            ],
            'galleryVideo.link' => [
                'string',
                'nullable',
            ],
            'galleryVideo.school_unit_id' => [
                'integer',
                'exists:school_units,id',
                'nullable',
            ],
        ];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['school_unit'] = SchoolUnit::pluck('name', 'id');
    }
}
