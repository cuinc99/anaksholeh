<?php

namespace App\Http\Livewire\Event;

use App\Models\Event;
use App\Models\SchoolUnit;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Create extends Component
{
    public Event $event;

    public array $mediaToRemove = [];

    public array $listsForFields = [];

    public array $mediaCollections = [];

    public function mount(Event $event)
    {
        $this->event = $event;
        $this->initListsForFields();
    }

    public function render()
    {
        return view('livewire.event.create');
    }

    public function submit()
    {
        $this->validate();
        if (auth()->user()->school_unit_id != 0) {
            $this->event->school_unit_id = auth()->user()->school_unit_id;
        }
        $this->event->save();
        $this->syncMedia();

        return redirect()->route('admin.events.index');
    }

    public function addMedia($media): void
    {
        $this->mediaCollections[$media['collection_name']][] = $media;
    }

    public function removeMedia($media): void
    {
        $collection = collect($this->mediaCollections[$media['collection_name']]);

        $this->mediaCollections[$media['collection_name']] = $collection->reject(fn ($item) => $item['uuid'] === $media['uuid'])->toArray();

        $this->mediaToRemove[] = $media['uuid'];
    }

    protected function rules(): array
    {
        return [
            'event.slug' => [
                'string',
                'nullable',
            ],
            'event.title' => [
                'string',
                'nullable',
            ],
            'mediaCollections.event_cover' => [
                'array',
                'nullable',
            ],
            'mediaCollections.event_cover.*.id' => [
                'integer',
                'exists:media,id',
            ],
            'event.from_date' => [
                'nullable',
                'date_format:' . config('project.date_format'),
            ],
            'event.to_date' => [
                'nullable',
                'date_format:' . config('project.date_format'),
            ],
            'event.location' => [
                'string',
                'nullable',
            ],
            'event.desc' => [
                'string',
                'nullable',
            ],
            'event.school_unit_id' => [
                'integer',
                'exists:school_units,id',
                'nullable',
            ],
        ];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['school_unit'] = SchoolUnit::pluck('name', 'id');
    }

    protected function syncMedia(): void
    {
        collect($this->mediaCollections)->flatten(1)
            ->each(fn ($item) => Media::where('uuid', $item['uuid'])
            ->update(['model_id' => $this->event->id]));

        Media::whereIn('uuid', $this->mediaToRemove)->delete();
    }
}
