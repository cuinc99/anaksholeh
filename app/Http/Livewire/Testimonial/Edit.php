<?php

namespace App\Http\Livewire\Testimonial;

use App\Models\Testimonial;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Edit extends Component
{
    public Testimonial $testimonial;

    public array $mediaToRemove = [];

    public array $listsForFields = [];

    public array $mediaCollections = [];

    public function mount(Testimonial $testimonial)
    {
        $this->testimonial = $testimonial;
        $this->mediaCollections = [
            'testimonial_cover' => $testimonial->cover,
        ];
    }

    public function render()
    {
        return view('livewire.testimonial.edit');
    }

    public function submit()
    {
        $this->validate();

        $this->testimonial->save();
        $this->syncMedia();

        return redirect()->route('admin.testimonials.index');
    }

    public function addMedia($media): void
    {
        $this->mediaCollections[$media['collection_name']][] = $media;
    }

    public function removeMedia($media): void
    {
        $collection = collect($this->mediaCollections[$media['collection_name']]);

        $this->mediaCollections[$media['collection_name']] = $collection->reject(fn ($item) => $item['uuid'] === $media['uuid'])->toArray();

        $this->mediaToRemove[] = $media['uuid'];
    }

    public function getMediaCollection($name)
    {
        return $this->mediaCollections[$name];
    }

    protected function rules(): array
    {
        return [
            'testimonial.name' => [
                'string',
                'nullable',
            ],
            'testimonial.message' => [
                'string',
                'nullable',
            ],
            'mediaCollections.testimonial_cover' => [
                'array',
                'nullable',
            ],
            'mediaCollections.testimonial_cover.*.id' => [
                'integer',
                'exists:media,id',
            ],
        ];
    }

    protected function syncMedia(): void
    {
        collect($this->mediaCollections)->flatten(1)
            ->each(fn ($item) => Media::where('uuid', $item['uuid'])
            ->update(['model_id' => $this->testimonial->id]));

        Media::whereIn('uuid', $this->mediaToRemove)->delete();
    }
}
