<?php

namespace App\Http\Livewire\Poster;

use App\Models\Poster;
use App\Models\SchoolUnit;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Edit extends Component
{
    public Poster $poster;

    public array $mediaToRemove = [];

    public array $listsForFields = [];

    public array $mediaCollections = [];

    public function mount(Poster $poster)
    {
        $this->poster = $poster;
        $this->initListsForFields();
        $this->mediaCollections = [
            'poster_image' => $poster->image,
        ];
    }

    public function render()
    {
        return view('livewire.poster.edit');
    }

    public function submit()
    {
        $this->validate();

        $this->poster->save();
        $this->syncMedia();

        return redirect()->route('admin.posters.index');
    }

    public function addMedia($media): void
    {
        $this->mediaCollections[$media['collection_name']][] = $media;
    }

    public function removeMedia($media): void
    {
        $collection = collect($this->mediaCollections[$media['collection_name']]);

        $this->mediaCollections[$media['collection_name']] = $collection->reject(fn ($item) => $item['uuid'] === $media['uuid'])->toArray();

        $this->mediaToRemove[] = $media['uuid'];
    }

    public function getMediaCollection($name)
    {
        return $this->mediaCollections[$name];
    }

    protected function rules(): array
    {
        return [
            'poster.slug' => [
                'string',
                'nullable',
            ],
            'poster.title' => [
                'string',
                'nullable',
            ],
            'poster.desc' => [
                'string',
                'nullable',
            ],
            'mediaCollections.poster_image' => [
                'array',
                'nullable',
            ],
            'mediaCollections.poster_image.*.id' => [
                'integer',
                'exists:media,id',
            ],
            'poster.school_unit_id' => [
                'integer',
                'exists:school_units,id',
                'nullable',
            ],
        ];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['school_unit'] = SchoolUnit::pluck('name', 'id')->toArray();
    }

    protected function syncMedia(): void
    {
        collect($this->mediaCollections)->flatten(1)
            ->each(fn ($item) => Media::where('uuid', $item['uuid'])
            ->update(['model_id' => $this->poster->id]));

        Media::whereIn('uuid', $this->mediaToRemove)->delete();
    }
}
