<?php

namespace App\Http\Livewire\Article;

use App\Models\Article;
use App\Models\Category;
use App\Models\SchoolUnit;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Create extends Component
{
    public Article $article;

    public array $mediaToRemove = [];

    public array $listsForFields = [];

    public array $mediaCollections = [];

    public function mount(Article $article)
    {
        $this->article            = $article;
        $this->article->is_slider = '1';
        $this->initListsForFields();
    }

    public function render()
    {
        return view('livewire.article.create');
    }

    public function submit()
    {
        $this->validate();
        if (auth()->user()->school_unit_id != 0) {
            $this->article->school_unit_id = auth()->user()->school_unit_id;
        }
        $this->article->save();
        $this->syncMedia();

        return redirect()->route('admin.articles.index');
    }

    public function addMedia($media): void
    {
        $this->mediaCollections[$media['collection_name']][] = $media;
    }

    public function removeMedia($media): void
    {
        $collection = collect($this->mediaCollections[$media['collection_name']]);

        $this->mediaCollections[$media['collection_name']] = $collection->reject(fn ($item) => $item['uuid'] === $media['uuid'])->toArray();

        $this->mediaToRemove[] = $media['uuid'];
    }

    protected function rules(): array
    {
        return [
            'article.slug' => [
                'string',
                'nullable',
            ],
            'article.title' => [
                'string',
                'nullable',
            ],
            'article.content' => [
                'string',
                'nullable',
            ],
            'mediaCollections.article_cover' => [
                'array',
                'nullable',
            ],
            'mediaCollections.article_cover.*.id' => [
                'integer',
                'exists:media,id',
            ],
            'article.school_unit_id' => [
                'integer',
                'exists:school_units,id',
                'nullable',
            ],
            'article.category_id' => [
                'integer',
                'exists:categories,id',
                'nullable',
            ],
            'article.published_at' => [
                'nullable',
                'date_format:' . config('project.datetime_format'),
            ],
            'article.is_slider' => [
                'nullable',
                'in:' . implode(',', array_keys($this->listsForFields['is_slider'])),
            ],
        ];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['school_unit'] = SchoolUnit::pluck('name', 'id');
        $this->listsForFields['category']     = Category::pluck('name', 'id');
        $this->listsForFields['is_slider']    = $this->article::IS_SLIDER_RADIO;
    }

    protected function syncMedia(): void
    {
        collect($this->mediaCollections)->flatten(1)
            ->each(fn ($item) => Media::where('uuid', $item['uuid'])
            ->update(['model_id' => $this->article->id]));

        Media::whereIn('uuid', $this->mediaToRemove)->delete();
    }
}
