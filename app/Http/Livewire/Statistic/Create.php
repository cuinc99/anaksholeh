<?php

namespace App\Http\Livewire\Statistic;

use App\Models\Statistic;
use Livewire\Component;

class Create extends Component
{
    public Statistic $statistic;

    public function mount(Statistic $statistic)
    {
        $this->statistic = $statistic;
    }

    public function render()
    {
        return view('livewire.statistic.create');
    }

    public function submit()
    {
        $this->validate();

        $this->statistic->save();

        return redirect()->route('admin.statistics.index');
    }

    protected function rules(): array
    {
        return [
            'statistic.school_unit' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'nullable',
            ],
            'statistic.teacher' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'nullable',
            ],
            'statistic.employee' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'nullable',
            ],
            'statistic.student' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'nullable',
            ],
        ];
    }
}
