<?php

namespace App\Http\Livewire\GalleryPhoto;

use App\Models\GalleryPhoto;
use App\Models\SchoolUnit;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Create extends Component
{
    public array $mediaToRemove = [];

    public array $listsForFields = [];

    public GalleryPhoto $galleryPhoto;

    public array $mediaCollections = [];

    public function mount(GalleryPhoto $galleryPhoto)
    {
        $this->galleryPhoto = $galleryPhoto;
        $this->initListsForFields();
    }

    public function render()
    {
        return view('livewire.gallery-photo.create');
    }

    public function submit()
    {
        $this->validate();
        if (auth()->user()->school_unit_id != 0) {
            $this->galleryPhoto->school_unit_id = auth()->user()->school_unit_id;
        }
        $this->galleryPhoto->save();
        $this->syncMedia();

        return redirect()->route('admin.gallery-photos.index');
    }

    public function addMedia($media): void
    {
        $this->mediaCollections[$media['collection_name']][] = $media;
    }

    public function removeMedia($media): void
    {
        $collection = collect($this->mediaCollections[$media['collection_name']]);

        $this->mediaCollections[$media['collection_name']] = $collection->reject(fn ($item) => $item['uuid'] === $media['uuid'])->toArray();

        $this->mediaToRemove[] = $media['uuid'];
    }

    protected function rules(): array
    {
        return [
            'galleryPhoto.slug' => [
                'string',
                'nullable',
            ],
            'galleryPhoto.title' => [
                'string',
                'nullable',
            ],
            'galleryPhoto.desc' => [
                'string',
                'nullable',
            ],
            'mediaCollections.gallery_photo_photo' => [
                'array',
                'nullable',
            ],
            'mediaCollections.gallery_photo_photo.*.id' => [
                'integer',
                'exists:media,id',
            ],
            'galleryPhoto.school_unit_id' => [
                'integer',
                'exists:school_units,id',
                'nullable',
            ],
        ];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['school_unit'] = SchoolUnit::pluck('name', 'id');
    }

    protected function syncMedia(): void
    {
        collect($this->mediaCollections)->flatten(1)
            ->each(fn ($item) => Media::where('uuid', $item['uuid'])
            ->update(['model_id' => $this->galleryPhoto->id]));

        Media::whereIn('uuid', $this->mediaToRemove)->delete();
    }
}
