<?php

namespace App\Http\Livewire\Page;

use App\Models\Page;
use Livewire\Component;

class Create extends Component
{
    public Page $page;

    public function mount(Page $page)
    {
        $this->page = $page;
    }

    public function render()
    {
        return view('livewire.page.create');
    }

    public function submit()
    {
        $this->validate();

        $this->page->save();

        return redirect()->route('admin.pages.index');
    }

    protected function rules(): array
    {
        return [
            'page.slug' => [
                'string',
                'nullable',
            ],
            'page.title' => [
                'string',
                'nullable',
            ],
            'page.content' => [
                'string',
                'nullable',
            ],
        ];
    }
}
