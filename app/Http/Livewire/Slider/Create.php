<?php

namespace App\Http\Livewire\Slider;

use App\Models\Slider;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Create extends Component
{
    public Slider $slider;

    public array $mediaToRemove = [];

    public array $mediaCollections = [];

    public function mount(Slider $slider)
    {
        $this->slider = $slider;
    }

    public function render()
    {
        return view('livewire.slider.create');
    }

    public function submit()
    {
        $this->validate();

        $this->slider->save();
        $this->syncMedia();

        return redirect()->route('admin.sliders.index');
    }

    public function addMedia($media): void
    {
        $this->mediaCollections[$media['collection_name']][] = $media;
    }

    public function removeMedia($media): void
    {
        $collection = collect($this->mediaCollections[$media['collection_name']]);

        $this->mediaCollections[$media['collection_name']] = $collection->reject(fn ($item) => $item['uuid'] === $media['uuid'])->toArray();

        $this->mediaToRemove[] = $media['uuid'];
    }

    protected function rules(): array
    {
        return [
            'slider.title' => [
                'string',
                'nullable',
            ],
            'slider.desc' => [
                'string',
                'nullable',
            ],
            'mediaCollections.slider_image' => [
                'array',
                'nullable',
            ],
            'mediaCollections.slider_image.*.id' => [
                'integer',
                'exists:media,id',
            ],
            'slider.link' => [
                'string',
                'nullable',
            ],
        ];
    }

    protected function syncMedia(): void
    {
        collect($this->mediaCollections)->flatten(1)
            ->each(fn ($item) => Media::where('uuid', $item['uuid'])
            ->update(['model_id' => $this->slider->id]));

        Media::whereIn('uuid', $this->mediaToRemove)->delete();
    }
}
