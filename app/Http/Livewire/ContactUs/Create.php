<?php

namespace App\Http\Livewire\ContactUs;

use App\Models\ContactUs;
use App\Models\SchoolUnit;
use Livewire\Component;

class Create extends Component
{
    public ContactUs $contactUs;

    public array $listsForFields = [];

    public function mount(ContactUs $contactUs)
    {
        $this->contactUs = $contactUs;
        $this->initListsForFields();
    }

    public function render()
    {
        return view('livewire.contact-us.create');
    }

    public function submit()
    {
        $this->validate();

        $this->contactUs->save();

        return redirect()->route('admin.contactuses.index');
    }

    protected function rules(): array
    {
        return [
            'contactUs.name' => [
                'string',
                'required',
            ],
            'contactUs.number' => [
                'string',
                'required',
            ],
            'contactUs.message' => [
                'string',
                'required',
            ],
            'contactUs.school_unit_id' => [
                'integer',
                'exists:school_units,id',
                'required',
            ],
        ];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['school_unit'] = SchoolUnit::pluck('name', 'id')->toArray();
    }
}
