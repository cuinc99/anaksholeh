<?php

namespace App\Http\Livewire\Pendaftaran;

use App\Models\Pendaftaran;
use Livewire\Component;

class Create extends Component
{
    public Pendaftaran $pendaftaran;

    public array $listsForFields = [];

    public function mount(Pendaftaran $pendaftaran)
    {
        $this->pendaftaran = $pendaftaran;
        $this->initListsForFields();
    }

    public function render()
    {
        return view('livewire.pendaftaran.create');
    }

    public function submit()
    {
        $this->validate();

        $this->pendaftaran->save();

        return redirect()->route('admin.pendaftarans.index');
    }

    protected function rules(): array
    {
        return [
            'pendaftaran.nama_lengkap' => [
                'string',
                'required',
            ],
            'pendaftaran.nama_pangilan' => [
                'string',
                'nullable',
            ],
            'pendaftaran.jenis_kelamin' => [
                'nullable',
                'in:' . implode(',', array_keys($this->listsForFields['jenis_kelamin'])),
            ],
            'pendaftaran.nisn' => [
                'string',
                'nullable',
            ],
            'pendaftaran.golongan_darah' => [
                'nullable',
                'in:' . implode(',', array_keys($this->listsForFields['golongan_darah'])),
            ],
            'pendaftaran.tempat_lahir' => [
                'string',
                'required',
            ],
            'pendaftaran.tanggal_lahir' => [
                'nullable',
                'date_format:' . config('project.date_format'),
            ],
            'pendaftaran.nik' => [
                'string',
                'nullable',
            ],
            'pendaftaran.kewarganegaraan' => [
                'string',
                'nullable',
            ],
            'pendaftaran.anak_ke' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'nullable',
            ],
            'pendaftaran.jumlah_saudara' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'nullable',
            ],
            'pendaftaran.bahasa' => [
                'string',
                'nullable',
            ],
            'pendaftaran.asal_sekolah' => [
                'string',
                'nullable',
            ],
            'pendaftaran.alamat_siswa' => [
                'string',
                'required',
            ],
            'pendaftaran.nama_ayah' => [
                'string',
                'required',
            ],
            'pendaftaran.tahun_lahir_ayah' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'nullable',
            ],
            'pendaftaran.pendidikan_ayah' => [
                'string',
                'nullable',
            ],
            'pendaftaran.telepon_ayah' => [
                'string',
                'nullable',
            ],
            'pendaftaran.pekerjaan_ayah' => [
                'string',
                'required',
            ],
            'pendaftaran.penghasilan_ayah' => [
                'nullable',
                'in:' . implode(',', array_keys($this->listsForFields['penghasilan_ayah'])),
            ],
            'pendaftaran.nama_ibu' => [
                'string',
                'required',
            ],
            'pendaftaran.tahun_lahir_ibu' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'nullable',
            ],
            'pendaftaran.pendidikan_ibu' => [
                'string',
                'nullable',
            ],
            'pendaftaran.telepon_ibu' => [
                'string',
                'nullable',
            ],
            'pendaftaran.pekerjaan_ibu' => [
                'string',
                'required',
            ],
            'pendaftaran.penghasilan_ibu' => [
                'nullable',
                'in:' . implode(',', array_keys($this->listsForFields['penghasilan_ibu'])),
            ],
            'pendaftaran.alamat_ortu' => [
                'string',
                'required',
            ],
            'pendaftaran.pilihan_kelas' => [
                'nullable',
                'in:' . implode(',', array_keys($this->listsForFields['pilihan_kelas'])),
            ],
            'pendaftaran.nama_wali' => [
                'string',
                'nullable',
            ],
            'pendaftaran.pekerjaan_wali' => [
                'string',
                'nullable',
            ],
            'pendaftaran.telepon_wali' => [
                'string',
                'nullable',
            ],
            'pendaftaran.jarak_rumah' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'nullable',
            ],
            'pendaftaran.waktu_tempuh' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
                'nullable',
            ],
            'pendaftaran.biaya_administrasi' => [
                'string',
                'nullable',
            ],
            'pendaftaran.kartu_keluarga' => [
                'string',
                'nullable',
            ],
            'pendaftaran.akta_kelahiran' => [
                'string',
                'nullable',
            ],
            'pendaftaran.pas_foto' => [
                'string',
                'nullable',
            ],
        ];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['jenis_kelamin']    = $this->pendaftaran::JENIS_KELAMIN_RADIO;
        $this->listsForFields['golongan_darah']   = $this->pendaftaran::GOLONGAN_DARAH_RADIO;
        $this->listsForFields['penghasilan_ayah'] = $this->pendaftaran::PENGHASILAN_AYAH_RADIO;
        $this->listsForFields['penghasilan_ibu']  = $this->pendaftaran::PENGHASILAN_IBU_RADIO;
        $this->listsForFields['pilihan_kelas']    = $this->pendaftaran::PILIHAN_KELAS_SELECT;
    }
}
