<?php

namespace App\Http\Livewire\TahunAjaran;

use App\Models\SchoolUnit;
use App\Models\TahunAjaran;
use Livewire\Component;

class Edit extends Component
{
    public TahunAjaran $tahunAjaran;

    public array $listsForFields = [];

    public function mount(TahunAjaran $tahunAjaran)
    {
        $this->tahunAjaran = $tahunAjaran;
        $this->initListsForFields();
    }

    public function render()
    {
        return view('livewire.tahun-ajaran.edit');
    }

    public function submit()
    {
        $this->validate();

        $this->tahunAjaran->save();

        return redirect()->route('admin.tahun-ajarans.index');
    }

    protected function rules(): array
    {
        return [
            'tahunAjaran.name' => [
                'string',
                'required',
            ],
            'tahunAjaran.description' => [
                'string',
                'nullable',
            ],
            'tahunAjaran.school_unit_id' => [
                'integer',
                'exists:school_units,id',
                'nullable',
            ],
        ];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['school_unit'] = SchoolUnit::pluck('name', 'id')->toArray();
    }
}
