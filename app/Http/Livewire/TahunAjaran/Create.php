<?php

namespace App\Http\Livewire\TahunAjaran;

use App\Models\SchoolUnit;
use App\Models\TahunAjaran;
use Livewire\Component;

class Create extends Component
{
    public TahunAjaran $tahunAjaran;

    public array $listsForFields = [];

    public function mount(TahunAjaran $tahunAjaran)
    {
        $this->tahunAjaran = $tahunAjaran;
        $this->initListsForFields();
    }

    public function render()
    {
        return view('livewire.tahun-ajaran.create');
    }

    public function submit()
    {
        $this->validate();
        if (auth()->user()->school_unit_id != 0) {
            $this->tahunAjaran->school_unit_id = auth()->user()->school_unit_id;
        }
        $this->tahunAjaran->save();

        return redirect()->route('admin.tahun-ajarans.index');
    }

    protected function rules(): array
    {
        return [
            'tahunAjaran.name' => [
                'string',
                'required',
            ],
            'tahunAjaran.description' => [
                'string',
                'nullable',
            ],
            'tahunAjaran.school_unit_id' => [
                'integer',
                'exists:school_units,id',
                'nullable',
            ],
        ];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['school_unit'] = SchoolUnit::where('id', '!=', 0)->pluck('name', 'id')->toArray();
    }
}
