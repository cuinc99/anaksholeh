<?php

namespace App\Http\Livewire\RelatedLink;

use App\Models\RelatedLink;
use Livewire\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Create extends Component
{
    public RelatedLink $relatedLink;

    public array $mediaToRemove = [];

    public array $mediaCollections = [];

    public function mount(RelatedLink $relatedLink)
    {
        $this->relatedLink = $relatedLink;
    }

    public function render()
    {
        return view('livewire.related-link.create');
    }

    public function submit()
    {
        $this->validate();

        $this->relatedLink->save();
        $this->syncMedia();

        return redirect()->route('admin.related-links.index');
    }

    public function addMedia($media): void
    {
        $this->mediaCollections[$media['collection_name']][] = $media;
    }

    public function removeMedia($media): void
    {
        $collection = collect($this->mediaCollections[$media['collection_name']]);

        $this->mediaCollections[$media['collection_name']] = $collection->reject(fn ($item) => $item['uuid'] === $media['uuid'])->toArray();

        $this->mediaToRemove[] = $media['uuid'];
    }

    protected function rules(): array
    {
        return [
            'relatedLink.name' => [
                'string',
                'nullable',
            ],
            'relatedLink.url' => [
                'string',
                'nullable',
            ],
            'mediaCollections.related_link_logo' => [
                'array',
                'nullable',
            ],
            'mediaCollections.related_link_logo.*.id' => [
                'integer',
                'exists:media,id',
            ],
        ];
    }

    protected function syncMedia(): void
    {
        collect($this->mediaCollections)->flatten(1)
            ->each(fn ($item) => Media::where('uuid', $item['uuid'])
            ->update(['model_id' => $this->relatedLink->id]));

        Media::whereIn('uuid', $this->mediaToRemove)->delete();
    }
}
