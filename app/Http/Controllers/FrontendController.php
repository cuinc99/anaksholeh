<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Event;
use App\Models\Poster;
use App\Models\Slider;
use App\Models\Article;
use App\Models\Category;
use App\Models\ContactUs;
use App\Models\Statistic;
use App\Models\SchoolUnit;
use App\Models\RelatedLink;
use App\Models\Testimonial;
use App\Models\GalleryPhoto;
use App\Models\GalleryVideo;
use App\Models\Pendaftaran;
use App\Models\TahunAjaran;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        $sliders = Slider::latest()->limit(5)->get();
        $statistic = Statistic::first();
        $testimonials = Testimonial::latest()->get();
        $su = SchoolUnit::whereId(0)->firstOrFail();
        $theNews = Article::latest()
            ->where('school_unit_id', 0)
            ->whereIn('category_id', [1, 3])
            ->limit(3)
            ->get();
        return view('frontend.index', compact('sliders', 'statistic', 'testimonials','su','theNews'));
    }

    public function schoolUnit($slug)
    {
        $schoolUnit = SchoolUnit::whereSlug($slug)->firstOrFail();
        $announcements = Article::latest()
            ->where('school_unit_id', $schoolUnit->id)
            ->where('category_id', 2)
            ->limit(5)
            ->get();

        $theNews = Article::latest()
            ->where('school_unit_id', $schoolUnit->id)
            ->whereIn('category_id', [1, 3])
            ->limit(3)
            ->get();

        $events = Event::latest()
            ->where('school_unit_id', $schoolUnit->id)
            ->limit(2)
            ->get();

        $photos = GalleryPhoto::latest()
            ->where('school_unit_id', $schoolUnit->id)
            ->limit(3)
            ->get();

        $videos = GalleryVideo::latest()
            ->where('school_unit_id', $schoolUnit->id)
            ->limit(3)
            ->get();

        $posters = Poster::latest()
            ->where('school_unit_id', $schoolUnit->id)
            ->limit(6)
            ->get();


        return view('frontend.school-unit.index', compact('schoolUnit', 'announcements', 'theNews', 'events', 'photos', 'videos', 'posters'));
    }

    public function blog($lp, Request $request)
    {
        $schoolUnit = SchoolUnit::query()
            ->whereSlug($lp)
            ->firstOrFail();

        $articles = Article::query()
            ->where('school_unit_id', $schoolUnit->id)
            ->whereIn('category_id', [1, 3])
            ->when($request->keyword, function ($query) use ($request) {
                $query->where('title', 'like', "%{$request->keyword}%");
            })
            ->latest()
            ->paginate(6);

        $articles->appends($request->only('keyword'));

        return view('frontend.article.blog', compact('articles', 'schoolUnit'));
    }

    public function articles($lp, $category, Request $request)
    {
        $schoolUnit = SchoolUnit::query()
            ->whereSlug($lp)
            ->firstOrFail();

        $category = Category::query()
            ->whereSlug($category)
            ->firstOrFail();

        $articles = Article::query()
            ->where('school_unit_id', $schoolUnit->id)
            ->where('category_id', $category->id)
            ->when($request->keyword, function ($query) use ($request) {
                $query->where('title', 'like', "%{$request->keyword}%");
            })
            ->paginate(6);

        $articles->appends($request->only('keyword'));

        return view('frontend.article.index', compact('articles', 'schoolUnit', 'category'));
    }

    public function articleDetail($lp, $category, $slug)
    {
        $article = Article::whereSlug($slug)->firstOrFail();
        $article->increment('visit');
        $schoolUnit = $article->schoolUnit;
        return view('frontend.article.detail', compact('article', 'schoolUnit'));
    }

    public function eventDetail($lp, $slug)
    {
        $event = Event::whereSlug($slug)->firstOrFail();
        $schoolUnit = $event->schoolUnit;
        return view('frontend.event.detail', compact('event', 'schoolUnit'));
    }

    public function galleryPhotos($lp, Request $request)
    {
        $schoolUnit = SchoolUnit::query()
            ->whereSlug($lp)
            ->firstOrFail();

        $galleryPhotos = GalleryPhoto::query()
            ->where('school_unit_id', $schoolUnit->id)
            ->when($request->keyword, function ($query) use ($request) {
                $query->where('title', 'like', "%{$request->keyword}%");
            })
            ->paginate(6);

        $galleryPhotos->appends($request->only('keyword'));

        return view('frontend.galleryPhoto.index', compact('galleryPhotos', 'schoolUnit'));
    }

    public function galleryPhotoDetail($lp, $slug)
    {
        $photo = GalleryPhoto::whereSlug($slug)->firstOrFail();
        $schoolUnit = $photo->schoolUnit;
        return view('frontend.galleryPhoto.detail', compact('photo', 'schoolUnit'));
    }

    public function galleryVideos($lp, Request $request)
    {
        $schoolUnit = SchoolUnit::query()
            ->whereSlug($lp)
            ->firstOrFail();

        $galleryVideos = GalleryVideo::query()
            ->where('school_unit_id', $schoolUnit->id)
            ->when($request->keyword, function ($query) use ($request) {
                $query->where('title', 'like', "%{$request->keyword}%");
            })
            ->paginate(6);

        $galleryVideos->appends($request->only('keyword'));

        return view('frontend.galleryVideo.index', compact('galleryVideos', 'schoolUnit'));
    }

    public function galleryVideoDetail($lp, $slug)
    {
        $video = GalleryVideo::whereSlug($slug)->firstOrFail();
        $schoolUnit = $video->schoolUnit;
        return view('frontend.galleryVideo.detail', compact('video', 'schoolUnit'));
    }

    public function posters()
    {
        return view();
    }

    public function posterDetail($lp, $slug)
    {
        $poster = Poster::whereSlug($slug)->firstOrFail();
        $schoolUnit = $poster->schoolUnit;
        return view('frontend.poster.detail', compact('poster', 'schoolUnit'));
    }

    public function contactUs()
    {
        $schoolUnits = SchoolUnit::pluck('name', 'id');
        return view('frontend.contactUs', compact('schoolUnits'));
    }

    public function pageDetail($slug)
    {
        $page = Page::whereSlug($slug)->firstOrFail();
        return view('frontend.page.detail', compact('page'));
    }

    public function register()
    {
        $schoolUnits = SchoolUnit::where('id', '!=', 0)->get();
        return view('frontend.register', compact('schoolUnits'));
    }

    public function registerForm($slug)
    {
        $schoolUnit = SchoolUnit::where('slug', $slug)->firstOrFail();
        $tahunAjaran = TahunAjaran::where('school_unit_id', $schoolUnit->id)->latest()->first();
        if (! $tahunAjaran) {
            return view('frontend.coming-soon');
        }

        if(in_array($schoolUnit->id, [1,2,3,4])) {
            return view('frontend.register1', compact('schoolUnit','tahunAjaran'));
        } elseif(in_array($schoolUnit->id, [5,6])) {
            return view('frontend.register2', compact('schoolUnit','tahunAjaran'));
        } else {
            return view('frontend.coming-soon');
        }
    }

    public function registerStore(Request $request)
    {
        $rr = $request->all();

        if ($request->hasFile('biaya_administrasi')) {
            $request->validate(['biaya_administrasi' => 'required|mimes:ppt,pptx,doc,docx,pdf,xls,xlsx,jpg,jpeg,png|max:500'], ['biaya_administrasi.mimes' => 'File Bukti Pembayaran harus berupa dokumen atau gambar.', 'biaya_administrasi.max' => 'File Bukti Pembayaran maksimal 500 kilobytes.']);
            $file = $request->file('biaya_administrasi');
            $path = $file->storeAs('dokumen-persyaratan/biaya_administrasi', $file->getClientOriginalName());
            $rr['biaya_administrasi'] = $path;
        }

        if ($request->hasFile('kartu_keluarga')) {
            $request->validate(['kartu_keluarga' => 'required|mimes:ppt,pptx,doc,docx,pdf,xls,xlsx,jpg,jpeg,png|max:500'], ['kartu_keluarga.mimes' => 'File biaya adminitrasi harus berupa dokumen atau gambar.']);
            $file = $request->file('kartu_keluarga');
            $path = $file->storeAs('dokumen-persyaratan/kartu_keluarga', $file->getClientOriginalName());
            $rr['kartu_keluarga'] = $path;
        }

        if ($request->hasFile('akta_kelahiran')) {
            $request->validate(['akta_kelahiran' => 'required|mimes:ppt,pptx,doc,docx,pdf,xls,xlsx,jpg,jpeg,png|max:500'], ['akta_kelahiran.mimes' => 'File biaya adminitrasi harus berupa dokumen atau gambar.']);
            $file = $request->file('akta_kelahiran');
            $path = $file->storeAs('dokumen-persyaratan/akta_kelahiran', $file->getClientOriginalName());
            $rr['akta_kelahiran'] = $path;
        }

        if ($request->hasFile('pas_foto')) {
            $request->validate(['pas_foto' => 'required|mimes:ppt,pptx,doc,docx,pdf,xls,xlsx,jpg,jpeg,png|max:500'], ['pas_foto.mimes' => 'File biaya adminitrasi harus berupa dokumen atau gambar.']);
            $file = $request->file('pas_foto');
            $path = $file->storeAs('dokumen-persyaratan/pas_foto', $file->getClientOriginalName());
            $rr['pas_foto'] = $path;
        }

        session()->flash('success', 'Anda telah berhasil terdaftar! terima kasih.');
        Pendaftaran::create($rr);
        return back();
    }

    public function profile($slug)
    {
        $schoolUnit = SchoolUnit::query()
            ->whereSlug($slug)
            ->firstOrFail();

        return view('frontend.school-unit.detail', compact('schoolUnit'));
    }

    public function contactUsStore(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'number' => 'required',
            'message' => 'required',
            'school_unit_id' => 'required',
            'captcha' => 'required|captcha'
        ],[
            'name.required' => 'Nama tidak boleh kosong',
            'number.required' => 'Nomor HP tidak boleh kosong',
            'message.required' => 'Pesan tidak boleh kosong',
            'school_unit_id.required' => 'Lembaga pendidikan harus dipilih',
            'captcha.required' => 'Captcha harus diisi',
            'captcha.captcha' => 'Captcha salah',
        ]);

        ContactUs::create($request->all());

        session()->flash('success', 'Pesan anda berhasil terkirim!');
        return back();
    }
}
