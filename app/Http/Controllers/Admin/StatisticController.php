<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Statistic;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StatisticController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('statistic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.statistic.index');
    }

    public function create()
    {
        abort_if(Gate::denies('statistic_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.statistic.create');
    }

    public function edit(Statistic $statistic)
    {
        abort_if(Gate::denies('statistic_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.statistic.edit', compact('statistic'));
    }

    public function show(Statistic $statistic)
    {
        abort_if(Gate::denies('statistic_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.statistic.show', compact('statistic'));
    }
}
