<?php

namespace App\Http\Controllers\Admin;

use Gate;
use App\Models\Pendaftaran;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PendaftaranController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('pendaftaran_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pendaftaran.index');
    }

    public function create()
    {
        abort_if(Gate::denies('pendaftaran_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pendaftaran.create');
    }

    public function edit(Pendaftaran $pendaftaran)
    {
        abort_if(Gate::denies('pendaftaran_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pendaftaran.edit', compact('pendaftaran'));
    }

    public function show(Pendaftaran $pendaftaran)
    {
        abort_if(Gate::denies('pendaftaran_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pendaftaran.show', compact('pendaftaran'));
    }
}
