<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GalleryVideo;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GalleryVideoController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('gallery_video_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.gallery-video.index');
    }

    public function create()
    {
        abort_if(Gate::denies('gallery_video_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.gallery-video.create');
    }

    public function edit(GalleryVideo $galleryVideo)
    {
        abort_if(Gate::denies('gallery_video_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.gallery-video.edit', compact('galleryVideo'));
    }

    public function show(GalleryVideo $galleryVideo)
    {
        abort_if(Gate::denies('gallery_video_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $galleryVideo->load('schoolUnit');

        return view('admin.gallery-video.show', compact('galleryVideo'));
    }
}
