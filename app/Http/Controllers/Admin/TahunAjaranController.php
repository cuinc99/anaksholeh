<?php

namespace App\Http\Controllers\Admin;

use Gate;
use Excel;
use App\Models\TahunAjaran;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Exports\PendaftaranExport;
use App\Http\Controllers\Controller;

class TahunAjaranController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('tahun_ajaran_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tahun-ajaran.index');
    }

    public function create()
    {
        abort_if(Gate::denies('tahun_ajaran_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tahun-ajaran.create');
    }

    public function edit(TahunAjaran $tahunAjaran)
    {
        abort_if(Gate::denies('tahun_ajaran_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tahun-ajaran.edit', compact('tahunAjaran'));
    }

    public function show(TahunAjaran $tahunAjaran)
    {
        abort_if(Gate::denies('tahun_ajaran_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tahunAjaran->load('schoolUnit');

        return view('admin.tahun-ajaran.show', compact('tahunAjaran'));
    }

    public function export(TahunAjaran $tahunAjaran)
    {
        $name = Str::slug($tahunAjaran->name);
        return Excel::download(new PendaftaranExport($tahunAjaran->id), 'pendaftaran-'. $name .'.xlsx');
    }
}
