<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\RelatedLink;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RelatedLinkController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('related_link_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.related-link.index');
    }

    public function create()
    {
        abort_if(Gate::denies('related_link_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.related-link.create');
    }

    public function edit(RelatedLink $relatedLink)
    {
        abort_if(Gate::denies('related_link_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.related-link.edit', compact('relatedLink'));
    }

    public function show(RelatedLink $relatedLink)
    {
        abort_if(Gate::denies('related_link_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.related-link.show', compact('relatedLink'));
    }

    public function storeMedia(Request $request)
    {
        abort_if(Gate::none(['related_link_create', 'related_link_edit']), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->has('size')) {
            $this->validate($request, [
                'file' => 'max:' . $request->input('size') * 1024,
            ]);
        }
        if (request()->has('max_width') || request()->has('max_height')) {
            $this->validate(request(), [
                'file' => sprintf(
                'image|dimensions:max_width=%s,max_height=%s',
                request()->input('max_width', 100000),
                request()->input('max_height', 100000)
                ),
            ]);
        }

        $model                     = new RelatedLink();
        $model->id                 = $request->input('model_id', 0);
        $model->exists             = true;
        $media                     = $model->addMediaFromRequest('file')->toMediaCollection($request->input('collection_name'));
        $media->wasRecentlyCreated = true;

        return response()->json(compact('media'), Response::HTTP_CREATED);
    }
}
