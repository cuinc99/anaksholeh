<?php

namespace App\Providers;

use Jenssegers\Date\Date;
use App\Models\SchoolUnit;
use App\Models\RelatedLink;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Paginator::defaultView('vendor.pagination.bootstrap-4');
        Date::setLocale('id');

        View::share('schoolUnits', SchoolUnit::where('id', '!=', 0)->get());
        View::share('relatedLinks', RelatedLink::latest()->get());
    }
}
