<?php

namespace App\Models;

use Carbon\Carbon;
use \DateTimeInterface;
use App\Models\SchoolUnit;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia;
use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Article extends Model implements HasMedia
{
    use HasFactory;
    use HasAdvancedFilter;
    use InteractsWithMedia;
    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public const IS_SLIDER_RADIO = [
        1 => 'Ya',
        0 => 'Tidak',
    ];

    public $table = 'articles';

    public $orderable = [
        'id',
        'slug',
        'title',
        'content',
        'school_unit.name',
        'category.name',
        'published_at',
        'is_slider',
        'visit',
    ];

    public $filterable = [
        'id',
        'slug',
        'title',
        'content',
        'school_unit.name',
        'category.name',
        'published_at',
        'is_slider',
    ];

    protected $appends = [
        'cover',
    ];

    protected $dates = [
        'published_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'slug',
        'title',
        'content',
        'school_unit_id',
        'category_id',
        'published_at',
        'is_slider',
        'visit',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $thumbnailWidth  = 50;
        $thumbnailHeight = 50;

        $thumbnailPreviewWidth  = 120;
        $thumbnailPreviewHeight = 120;

        $thumbnailArticleiewWidth  = 800;
        $thumbnailArticleiewHeight = 600;

        $coverArticleiewWidth  = 2000;
        $coverArticleiewHeight = 1333;

        $shareWidth  = 400;
        $shareHeight = 400;

        $this->addMediaConversion('cover_article')
            ->width($coverArticleiewWidth)
            ->height($coverArticleiewHeight)
            ->fit('crop', $coverArticleiewWidth, $coverArticleiewHeight);

        $this->addMediaConversion('share_article')
            ->width($shareWidth)
            ->height($shareHeight)
            ->fit('crop', $shareWidth, $shareHeight);

        $this->addMediaConversion('thumbnail')
            ->width($thumbnailWidth)
            ->height($thumbnailHeight)
            ->fit('crop', $thumbnailWidth, $thumbnailHeight);
        $this->addMediaConversion('preview_thumbnail')
            ->width($thumbnailPreviewWidth)
            ->height($thumbnailPreviewHeight)
            ->fit('crop', $thumbnailPreviewWidth, $thumbnailPreviewHeight);
        $this->addMediaConversion('article_thumbnail')
            ->width($thumbnailArticleiewWidth)
            ->height($thumbnailArticleiewHeight)
            ->fit('crop', $thumbnailArticleiewWidth, $thumbnailArticleiewHeight);
    }

    public function getCoverAttribute()
    {
        return $this->getMedia('article_cover')->map(function ($item) {
            $media = $item->toArray();
            $media['url'] = $item->getUrl();
            $media['thumbnail'] = $item->getUrl('thumbnail');
            $media['preview_thumbnail'] = $item->getUrl('preview_thumbnail');
            $media['article_thumbnail'] = $item->getUrl('article_thumbnail');
            $media['cover_article'] = $item->getUrl('cover_article');
            $media['share_article'] = $item->getUrl('share_article');

            return $media;
        });
    }

    public function schoolUnit()
    {
        return $this->belongsTo(SchoolUnit::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getPublishedAtAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('project.datetime_format')) : null;
    }

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ? Carbon::createFromFormat(config('project.datetime_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function getIsSliderLabelAttribute($value)
    {
        return static::IS_SLIDER_RADIO[$this->is_slider] ?? null;
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
