<?php

namespace App\Models;

use \DateTimeInterface;
use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TahunAjaran extends Model
{
    use HasFactory;
    use HasAdvancedFilter;

    public $table = 'tahun_ajarans';

    public $orderable = [
        'id',
        'name',
        'description',
        'school_unit.name',
    ];

    public $filterable = [
        'id',
        'name',
        'description',
        'school_unit.name',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'description',
        'school_unit_id',
    ];

    public function schoolUnit()
    {
        return $this->belongsTo(SchoolUnit::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function pendaftarans()
    {
        return $this->hasMany(Pendaftaran::class, 'tahun_ajaran_id');
    }
}
