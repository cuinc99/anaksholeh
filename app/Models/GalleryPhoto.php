<?php

namespace App\Models;

use \DateTimeInterface;
use Spatie\MediaLibrary\HasMedia;
use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class GalleryPhoto extends Model implements HasMedia
{
    use HasFactory;
    use HasAdvancedFilter;
    use InteractsWithMedia;
    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public $table = 'gallery_photos';

    public $orderable = [
        'id',
        'slug',
        'title',
        'desc',
        'school_unit.name',
    ];

    public $filterable = [
        'id',
        'slug',
        'title',
        'desc',
        'school_unit.name',
    ];

    protected $appends = [
        'photo',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'slug',
        'title',
        'desc',
        'school_unit_id',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $thumbnailWidth  = 50;
        $thumbnailHeight = 50;

        $thumbnailPreviewWidth  = 120;
        $thumbnailPreviewHeight = 120;

        $photoWidth  = 800;
        $photoHeight = 600;

        $this->addMediaConversion('thumbnail')
            ->width($thumbnailWidth)
            ->height($thumbnailHeight)
            ->fit('crop', $thumbnailWidth, $thumbnailHeight);
        $this->addMediaConversion('preview_thumbnail')
            ->width($thumbnailPreviewWidth)
            ->height($thumbnailPreviewHeight)
            ->fit('crop', $thumbnailPreviewWidth, $thumbnailPreviewHeight);
        $this->addMediaConversion('photo')
            ->width($photoWidth)
            ->height($photoHeight)
            ->fit('crop', $photoWidth, $photoHeight);
    }

    public function getPhotoAttribute()
    {
        return $this->getMedia('gallery_photo_photo')->map(function ($item) {
            $media = $item->toArray();
            $media['url'] = $item->getUrl();
            $media['thumbnail'] = $item->getUrl('thumbnail');
            $media['preview_thumbnail'] = $item->getUrl('preview_thumbnail');
            $media['photo'] = $item->getUrl('photo');

            return $media;
        });
    }

    public function schoolUnit()
    {
        return $this->belongsTo(SchoolUnit::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
