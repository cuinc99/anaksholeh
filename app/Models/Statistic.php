<?php

namespace App\Models;

use \DateTimeInterface;
use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    use HasFactory;
    use HasAdvancedFilter;

    public $table = 'statistics';

    public $orderable = [
        'id',
        'school_unit',
        'teacher',
        'employee',
        'student',
    ];

    public $filterable = [
        'id',
        'school_unit',
        'teacher',
        'employee',
        'student',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'school_unit',
        'teacher',
        'employee',
        'student',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
