<?php

namespace App\Models;

use \DateTimeInterface;
use App\Support\HasAdvancedFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    use HasFactory;
    use HasAdvancedFilter;

    public const TRANSPORTASI_RADIO = [
        'Mobil' => 'Mobil',
        'Sepeda Motor' => 'Sepeda Motor',
        'Sepeda' => 'Sepeda',
        'Angkutan Umum' => 'Angkutan Umum',
        'Jalan Kaki' => 'Jalan Kaki',
        'Lainnya' => 'Lainnya',
    ];

    public const JENIS_KELAMIN_RADIO = [
        'Laki-Laki' => 'Laki-Laki',
        'Perempuan' => 'Perempuan',
    ];

    public const KELAS_RADIO = [
        'Reguler' => 'Reguler',
        'Tahfidz' => 'Tahfidz',
    ];

    public const PILIHAN_KELAS_SELECT = [
        'Kelompok A' => 'Kelompok A',
        'Kelompok B' => 'Kelompok B',
    ];

    public const GOLONGAN_DARAH_RADIO = [
        'A'          => 'A',
        'B'          => 'B',
        'AB'         => 'AB',
        'O'          => 'O',
        'Tidak Tahu' => 'Tidak Tahu',
    ];

    public const PENGHASILAN_IBU_RADIO = [
        '< Rp. 500.000'                 => '< Rp. 500.000',
        'Rp. 500.000 - Rp. 999.999'     => 'Rp. 500.000 - Rp. 999.999',
        'Rp. 1.000.000 - Rp. 1.999.999' => 'Rp. 1.000.000 - Rp. 1.999.999',
        'Rp. 2.000.000 - Rp. 5.000.000' => 'Rp. 2.000.000 - Rp. 5.000.000',
        '> Rp. 5.000.000'               => '> Rp. 5.000.000',
    ];

    public const PENGHASILAN_AYAH_RADIO = [
        '< Rp. 500.000'                 => '< Rp. 500.000',
        'Rp. 500.000 - Rp. 999.999'     => 'Rp. 500.000 - Rp. 999.999',
        'Rp. 1.000.000 - Rp. 1.999.999' => 'Rp. 1.000.000 - Rp. 1.999.999',
        'Rp. 2.000.000 - Rp. 5.000.000' => 'Rp. 2.000.000 - Rp. 5.000.000',
        '> Rp. 5.000.000'               => '> Rp. 5.000.000',
    ];

    public $table = 'pendaftarans';

    public $orderable = [
        'id',
        'nama_lengkap',
        'nama_pangilan',
        'jenis_kelamin',
        'nisn',
        'golongan_darah',
        'tempat_lahir',
        'tanggal_lahir',
        'nik',
        'kewarganegaraan',
        'anak_ke',
        'jumlah_saudara',
        'bahasa',
        'asal_sekolah',
        'alamat_siswa',
        'nama_ayah',
        'tahun_lahir_ayah',
        'pendidikan_ayah',
        'telepon_ayah',
        'pekerjaan_ayah',
        'penghasilan_ayah',
        'nama_ibu',
        'tahun_lahir_ibu',
        'pendidikan_ibu',
        'telepon_ibu',
        'pekerjaan_ibu',
        'penghasilan_ibu',
        'alamat_ayah',
        'alamat_ibu',
        'pilihan_kelas',
        'nama_wali',
        'pekerjaan_wali',
        'telepon_wali',
        'jarak_rumah',
        'waktu_tempuh',
        'biaya_administrasi',
        'kartu_keluarga',
        'akta_kelahiran',
        'pas_foto',
        'school_unit_id',
        'tahun_ajaran_id',
        'jalur_masuk',
        'ket_jalur_masuk',
    ];

    public $filterable = [
        'id',
        'nama_lengkap',
        'nama_pangilan',
        'jenis_kelamin',
        'nisn',
        'golongan_darah',
        'tempat_lahir',
        'tanggal_lahir',
        'nik',
        'kewarganegaraan',
        'anak_ke',
        'jumlah_saudara',
        'bahasa',
        'asal_sekolah',
        'alamat_siswa',
        'nama_ayah',
        'tahun_lahir_ayah',
        'pendidikan_ayah',
        'telepon_ayah',
        'pekerjaan_ayah',
        'penghasilan_ayah',
        'nama_ibu',
        'tahun_lahir_ibu',
        'pendidikan_ibu',
        'telepon_ibu',
        'pekerjaan_ibu',
        'penghasilan_ibu',
        'alamat_ayah',
        'alamat_ibu',
        'pilihan_kelas',
        'nama_wali',
        'pekerjaan_wali',
        'telepon_wali',
        'jarak_rumah',
        'waktu_tempuh',
        'biaya_administrasi',
        'kartu_keluarga',
        'akta_kelahiran',
        'pas_foto',
        'school_unit_id',
        'tahun_ajaran_id',
        'jalur_masuk',
        'ket_jalur_masuk',
    ];

    protected $dates = [
        'tanggal_lahir',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'nama_lengkap',
        'nama_pangilan',
        'jenis_kelamin',
        'nisn',
        'golongan_darah',
        'tempat_lahir',
        'tanggal_lahir',
        'nik',
        'kewarganegaraan',
        'anak_ke',
        'jumlah_saudara',
        'bahasa',
        'asal_sekolah',
        'alamat_siswa',
        'kelas',
        'nama_ayah',
        'tahun_lahir_ayah',
        'pendidikan_ayah',
        'telepon_ayah',
        'pekerjaan_ayah',
        'penghasilan_ayah',
        'nama_ibu',
        'tahun_lahir_ibu',
        'pendidikan_ibu',
        'telepon_ibu',
        'pekerjaan_ibu',
        'penghasilan_ibu',
        'alamat_ayah',
        'alamat_ibu',
        'pilihan_kelas',
        'nama_wali',
        'pekerjaan_wali',
        'telepon_wali',
        'jarak_rumah',
        'waktu_tempuh',
        'biaya_administrasi',
        'kartu_keluarga',
        'akta_kelahiran',
        'pas_foto',
        'school_unit_id',
        'tahun_ajaran_id',
        'jalur_masuk',
        'ket_jalur_masuk',
    ];

    public function getJenisKelaminLabelAttribute($value)
    {
        return static::JENIS_KELAMIN_RADIO[$this->jenis_kelamin] ?? null;
    }

    public function getGolonganDarahLabelAttribute($value)
    {
        return static::GOLONGAN_DARAH_RADIO[$this->golongan_darah] ?? null;
    }

    public function getTanggalLahirAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('project.date_format')) : null;
    }

    public function setTanggalLahirAttribute($value)
    {
        $this->attributes['tanggal_lahir'] = $value ? Carbon::createFromFormat(config('project.date_format'), $value)->format('Y-m-d') : null;
    }

    public function getPenghasilanAyahLabelAttribute($value)
    {
        return static::PENGHASILAN_AYAH_RADIO[$this->penghasilan_ayah] ?? null;
    }

    public function getPenghasilanIbuLabelAttribute($value)
    {
        return static::PENGHASILAN_IBU_RADIO[$this->penghasilan_ibu] ?? null;
    }

    public function getPilihanKelasLabelAttribute($value)
    {
        return static::PILIHAN_KELAS_SELECT[$this->pilihan_kelas] ?? null;
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
