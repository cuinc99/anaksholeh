<?php

namespace App\Services;

class ModelService
{
    public static function generate($title = '')
    {
        return Str::slug($title);
    }
}
