<?php

namespace App\Exports;

use App\Models\Pendaftaran;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PendaftaranExport implements FromView, ShouldAutoSize
{
    protected $taId;

    public function __construct($taId)
    {
        $this->taId = $taId;
    }

    public function view(): View
    {
        return view('exports.pendaftarans', [
            'pendaftarans' => Pendaftaran::where('tahun_ajaran_id', $this->taId)->get()
        ]);
    }
}
